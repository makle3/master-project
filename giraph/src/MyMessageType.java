import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by Martin on 16-03-2016.
 */
public class MyMessageType implements WritableComparable {
    private final Long source;
    private double value;

    public MyMessageType(double i, Long sourceId) {
        this.value = i;
        this.source = sourceId;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeDouble(value);
    }

    public double get() {
        return this.value;
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.value = dataInput.readInt();
    }

    @Override
    public int compareTo(Object o) {
        MyMessageType other = (MyMessageType)o;
        return this.value < other.value?-1:(this.value == other.value?0:1);
    }

    public Long getSource() {
        return source;
    }
}
