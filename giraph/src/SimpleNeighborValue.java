import org.apache.giraph.GiraphRunner;
import org.apache.giraph.edge.Edge;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.io.formats.JsonLongDoubleFloatDoubleVertexInputFormat;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * Created by Martin on 16-03-2016.
 */
public class SimpleNeighborValue extends BasicComputation<LongWritable, DoubleWritable, FloatWritable, MyMessageType> {
    @Override
    public void compute(Vertex<LongWritable, DoubleWritable, FloatWritable> vertex, Iterable<MyMessageType> messages) throws IOException {

        if (getSuperstep() == 0) {
            vertex.setValue(new DoubleWritable(Double.MAX_VALUE));
            for (Edge<LongWritable, FloatWritable> edge : vertex.getEdges()) {
                sendMessage(edge.getTargetVertexId(), new MyMessageType(-1, vertex.getId().get()));
            }
        }
        else {
            vertex.voteToHalt();
        }

        double d = -1d;
        for(MyMessageType message : messages){
            if(message.get() == -1){
                sendMessage(new LongWritable(message.getSource()),new MyMessageType(vertex.getValue().get(), vertex.getId().get()));
            }
            else{
                if(d == -1d){
                    d = 0d;
                }
                d += message.get();
            }
        }
        if(d != -1d){
            vertex.setValue(new DoubleWritable(d));
        }
    }

    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new GiraphRunner(), args));
    }
}
