package smucparser;

import org.apache.commons.lang3.time.StopWatch;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.complex.MST.MSTDataType;
import smucparser.datatypes.simple.IntDataType;
import smucparser.graph.Import.DOT;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by Martin on 06-05-2016.
 */
public class MSTTest {
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;

    @Before
    public void setUp() throws Exception {


        MyVertexFactory factory = new MyVertexFactory();

        MyVertex V1 = factory.createVertex();
        MyVertex V2 = factory.createVertex();
        MyVertex V3 = factory.createVertex();
        MyVertex V4 = factory.createVertex();
        MyVertex V5 = factory.createVertex();
        MyVertex V6 = factory.createVertex();
        MyVertex V7 = factory.createVertex();
        MyVertex V8 = factory.createVertex();
        MyVertex V9 = factory.createVertex();
        MyVertex V10 = factory.createVertex();

        // add the vertices
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);
        g.addVertex(V5);
        g.addVertex(V6);
        g.addVertex(V7);
        g.addVertex(V8);
        g.addVertex(V9);
        g.addVertex(V10);

        AbstractDataType dataType = AbstractDataType.getInstance(IntDataType.class);
        String capability = "a(x)(s,t):=AddEdgeWeight(x,asMST(test))";
        g.addEdge(V1, V2) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 1)).addEdgeCapability(capability);
        g.addEdge(V2, V3) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 2)).addEdgeCapability(capability);
        g.addEdge(V3, V4) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 3)).addEdgeCapability(capability);
        g.addEdge(V4, V1) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 4)).addEdgeCapability(capability);
        g.addEdge(V8, V7) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 5)).addEdgeCapability(capability);
        g.addEdge(V7, V4) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 6)).addEdgeCapability(capability);
        g.addEdge(V6, V3) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 7)).addEdgeCapability(capability);
        g.addEdge(V5, V6) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 8)).addEdgeCapability(capability);
        g.addEdge(V5, V2) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 9)).addEdgeCapability(capability);
        g.addEdge(V8, V9) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 1)).addEdgeCapability(capability);
        g.addEdge(V9, V10).addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 2)).addEdgeCapability(capability);
        g.addEdge(V9, V1) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 3)).addEdgeCapability(capability);

        this.graph = g;
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void asd() throws Exception {
        SMuC.performQuery(MSTDataType.class,this.graph,
                "lfp z. connect(" +
                        "gfp y. shareSmallestPotentialInGroup( " +
                        "lfp x. findSmallest(z+1, asSet[a]z+1)," +
                        "asSet[]x), " +
                        "asSet[]y)",
                10000L);

        //SMuC.performQuery(MSTDataType.class,this.graph,
        //        "lfp z. connect(" +
        //                "shareSmallestPotentialInGroup( " +
        //                "findSmallest(z+1, asSet[a]z+1)," +
        //                "asSet[]findSmallest(z+1, asSet[a]z+1)), " +
        //                "asSet[]shareSmallestPotentialInGroup(" +
        //                "findSmallest(z+1, asSet[a]z+1)," +
        //                "asSet[]findSmallest(z+1, asSet[a]z+1)))",
        //        40L);
        DOT.printGraph(this.graph, "Mst.pdf");
    }

    @Test
    public void dsa() throws Exception{


        MyVertexFactory factory = new MyVertexFactory();

        MyVertex V1 = factory.createVertex();
        MyVertex V2 = factory.createVertex();
        MyVertex V3 = factory.createVertex();
        MyVertex V4 = factory.createVertex();
        MyVertex V5 = factory.createVertex();

        // add the vertices
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);
        g.addVertex(V5);

        AbstractDataType dataType = AbstractDataType.getInstance(IntDataType.class);
        String capability = "a(x)(s,t):=AddEdgeWeight(x,asMST(test))";
        Random rand = new Random();
        g.addEdge(V1, V2) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 0)).addEdgeCapability(capability);
        g.addEdge(V1, V3) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 0)).addEdgeCapability(capability);
        g.addEdge(V2, V3) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 0)).addEdgeCapability(capability);
        g.addEdge(V3, V4) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 0)).addEdgeCapability(capability);
        g.addEdge(V1, V5) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 0)).addEdgeCapability(capability);
        g.addEdge(V2, V1) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 0)).addEdgeCapability(capability);
        g.addEdge(V3, V1) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 0)).addEdgeCapability(capability);
        g.addEdge(V3, V2) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 0)).addEdgeCapability(capability);
        g.addEdge(V4, V3) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 0)).addEdgeCapability(capability);
        g.addEdge(V5, V1) .addAttribute("test",  AbstractDataType.newInstance(dataType.getClass(), 0)).addEdgeCapability(capability);
        //DOT.printGraph(g,"Mst2.pdf");
        //SMuC.performQuery(MSTDataType.class,g,"lfp x. findSmallest(x+1, asSet[a]x+1)", 100L);
        //DOT.printGraph(g,"Mst2.pdf");
        //SMuC.performQuery(MSTDataType.class,g,"lfp z. shareSmallestPotentialInGroup(x, asSet[]x)", 100L);
        //DOT.printGraph(g,"Mst2.pdf");
        //SMuC.performQuery(MSTDataType.class,g,"gfp y. connect(x, asSet[]x)");
        //SMuC.performQuery(MSTDataType.class,g,"gfp z. connect(y, asSet[]y)");

        //SMuC.performQuery(MSTDataType.class,g,
        //            "lfp z. connect(" +
        //                "lfp y. shareSmallestPotentialInGroup( " +
        //                    "lfp x. findSmallest(z+1, asSet[a]z+1)," +
        //                    "asSet[]x), " +
        //                "asSet[]y)",
        //        10000L);


        SMuC.performQuery(MSTDataType.class,g,
                "gfp z. connect(" +
                        "shareSmallestPotentialInGroup( " +
                        "findSmallest(z+1, asSet[a]z+1)," +
                        "asSet[]findSmallest(z+1, asSet[a]z+1)), " +
                        "asSet[]shareSmallestPotentialInGroup(" +
                        "findSmallest(z+1, asSet[a]z+1)," +
                        "asSet[]findSmallest(z+1, asSet[a]z+1)))",
                50L);

        SMuC.performQuery(MSTDataType.class,g,
                "lfp adjList. asNodeSet(z)",
                10000L);
        SMuC.performQuery(MSTDataType.class,g,
                "lfp root. getRoot(z)",
                10000L);
        //Main.writeFile(JSON.serialize( g), "MST.json");
        //SMuC.performQuery(MSTDataType.class,g,"lfp y. connect(lfp x. findSmallest(y+1, asSet[a]y+1), asSet[]x)");

        //SMuC.performQuery(MSTDataType.class,g,"lfp y. same(x,min_edge_value[a]addNeighbor(x,1))");
        //DOT.printGraph(g,"Mst2.pdf");
        //SMuC.performQuery(MSTDataType.class,g,"gfp z. add_adjacent_nodes(min_edge_value(smallest_connected[]add_adjacent_nodes(z,y), smallest_unconnected[a]y),y)");

        //Find lowest edge given an edge id which is not in the same group
        //SMuC.performQuery(MSTDataType.class,g,"
    }



    @Test
    public void dsa2() throws Exception{


        MyVertexFactory factory = new MyVertexFactory();

        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);

        AbstractDataType dataType = AbstractDataType.getInstance(IntDataType.class);

        int maxNodes = 100;
        MyVertex prev = null;
        for (int i = 0; i < maxNodes; i++) {
            MyVertex vert = factory.createVertex();
            g.addVertex(vert);
            if(prev != null){
                MyEdge e = g.addEdge(prev, vert);
                e.addAttribute("test", AbstractDataType.newInstance(dataType.getClass(), 0));
                e.addEdgeCapability("a(x)(s,t):=AddEdgeWeight(x,asMST(test))");
            }

            prev = vert;
        }

        StopWatch sw = new StopWatch();
        sw.start();
        SMuC.performQuery(MSTDataType.class,g,
                "gfp z. connect(" +
                        "shareSmallestPotentialInGroup( " +
                        "findSmallest(z+1, asSet[a]z+1)," +
                        "asSet[]findSmallest(z+1, asSet[a]z+1)), " +
                        "asSet[]shareSmallestPotentialInGroup(" +
                        "findSmallest(z+1, asSet[a]z+1)," +
                        "asSet[]findSmallest(z+1, asSet[a]z+1)))");

        sw.stop();
        System.out.println(sw.toString());
        //Main.writeFile(JSON.serialize( g), "MST.json");
        //SMuC.performQuery(MSTDataType.class,g,"lfp y. connect(lfp x. findSmallest(y+1, asSet[a]y+1), asSet[]x)");

        //SMuC.performQuery(MSTDataType.class,g,"lfp y. same(x,min_edge_value[a]addNeighbor(x,1))");
        //DOT.printGraph(g,"Mst2.pdf");
    }
}
