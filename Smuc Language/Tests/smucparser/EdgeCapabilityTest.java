package smucparser;

import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.simple.IntDataType;
import smucparser.datatypes.complex.NodeSetDataType;
import smucparser.graph.Import.CSV;
import smucparser.graph.Import.DOT;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by Martin on 28-02-2016.
 */
public class EdgeCapabilityTest {
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;
    MyVertexFactory factory;
    private MyVertex V1;
    private MyVertex V2;
    private MyVertex V3;
    private MyVertex V4;
    private Class<? extends AbstractDataType> dataType = IntDataType.class;

    @Before
    public void setUp() throws Exception {
        graph = new DefaultDirectedGraph<>(MyEdge.class);
        DefaultDirectedGraph<MyVertex, MyEdge> g = graph;
        factory = new MyVertexFactory(IntDataType.class);

        //String v1 = "1 goal = 0;
        HashMap<String, AbstractDataType> v1 = new HashMap<>();
        v1.put("goal",AbstractDataType.newInstance(IntDataType.class, 0));
        V1 = factory.createVertex(v1);


        HashMap<String, AbstractDataType> v2 = new HashMap<>();
        v2.put("goal",AbstractDataType.newInstance(IntDataType.class, Integer.MAX_VALUE));


        V2 = factory.createVertex(v2);
        V3 = factory.createVertex(v2);
        V4 = factory.createVertex(v2);

        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);

        String capability = "f(x,s,t):= x+1";
        MyEdge e = g.addEdge(V1, V2);
        e.addEdgeCapability(capability);
        e = g.addEdge(V2, V3);
        e.addEdgeCapability(capability);
        e = g.addEdge(V3, V4);
        e.addEdgeCapability(capability);
        e = g.addEdge(V4, V1);
        e.addEdgeCapability(capability);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testMissingCapability() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        g.addEdge(V2,V4);
        String s = "gfp z. min(goal,min<f>z)";
        try {
            SMuC.performQuery(dataType, g, s);
            assertTrue(true);
        }
        catch (ParseCancellationException ex){
            assertTrue(false);
        }
    }


    @Test
    public void testMissingCapability2() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        String s = "gfp z. min(goal,min<z>z)";
        try {
            SMuC.performQuery(dataType, g, s);
            assertTrue(false);
        }
        catch (ParseCancellationException ex){
            assertTrue(true);
        }
    }

    @Test
    public void testMissingVarnameCollision() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        String s = "gfp f. min(goal,min<f>f)";
        try {
            SMuC.performQuery(dataType, g, s);
            assertTrue(false);
        }
        catch (ParseCancellationException ex){
            assertTrue(true);
        }
    }

    @Test
    public void testNotUsingCapabilityWithMissingCapability() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        g.addEdge(V2,V4);
        String s = "gfp z. min(goal,min<>z)";
        try {
            SMuC.performQuery(dataType, g, s);
            assertTrue(true);
        }
        catch (ParseCancellationException ex){
            ex.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    public void testCapabilityPlus() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        String s = "gfp z. min(goal,min<f>z)";
        try {
            SMuC.performQuery(dataType, g, s);
            assertTrue(true);
        }
        catch (ParseCancellationException ex){
            assertTrue(false);
        }
        MyVertex[] vertices =  g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        assertEquals(0, (int) vertices[0].getFormulaVariable("z").getValue());
        assertEquals(3, (int) vertices[1].getFormulaVariable("z").getValue());
        assertEquals(2, (int) vertices[2].getFormulaVariable("z").getValue());
        assertEquals(1, (int) vertices[3].getFormulaVariable("z").getValue());
    }

    @Test
    public void testCapabilityUndirected() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        String s = "gfp z. min(goal,min[f]z)";
        try {
            SMuC.performQuery(dataType, g, s);
            assertTrue(true);
        }
        catch (ParseCancellationException ex){
            assertTrue(false);
        }
        MyVertex[] vertices =  g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        assertEquals(0, (int) vertices[0].getFormulaVariable("z").getValue());
        assertEquals(1, (int) vertices[1].getFormulaVariable("z").getValue());
        assertEquals(2, (int) vertices[2].getFormulaVariable("z").getValue());
        assertEquals(1, (int) vertices[3].getFormulaVariable("z").getValue());
    }


    @Test
    public void testCapabilityIngoing() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        String s = "gfp z. min(goal,min>f<z)";
        try {
            SMuC.performQuery(dataType, g, s);
            assertTrue(true);
        }
        catch (ParseCancellationException ex){
            ex.printStackTrace();
            assertTrue(false);
        }
        MyVertex[] vertices =  g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        assertEquals(0, (int) vertices[0].getFormulaVariable("z").getValue());
        assertEquals(1, (int) vertices[1].getFormulaVariable("z").getValue());
        assertEquals(2, (int) vertices[2].getFormulaVariable("z").getValue());
        assertEquals(3, (int) vertices[3].getFormulaVariable("z").getValue());
    }

    @Test
    public void testCapability1() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();

        HashMap<String, AbstractDataType> v1 = new HashMap<>();
        v1.put("goal",AbstractDataType.newInstance(IntDataType.class, Integer.MAX_VALUE));

        MyVertex VNew1 = factory.createVertex(v1);
        g.addVertex(VNew1);
        MyEdge e = g.addEdge(VNew1,V2);
        e.addEdgeCapability("f(a,s,t):=23");

        MyVertex VNew2 = factory.createVertex(v1);
        g.addVertex(VNew2);
        MyEdge e2 = g.addEdge(VNew2,V2);
        e2.addEdgeCapability("f(a,s,t):=a");

        String s = "gfp z. min(goal,min<f>z)";
        try {
            SMuC.performQuery(dataType, g, s);
            assertTrue(true);
        }
        catch (ParseCancellationException ex){
            assertTrue(false);
        }
        MyVertex[] vertices =  g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        assertEquals(0, (int) vertices[0].getFormulaVariable("z").getValue());
        assertEquals(3, (int) vertices[1].getFormulaVariable("z").getValue());
        assertEquals(2, (int) vertices[2].getFormulaVariable("z").getValue());
        assertEquals(1, (int) vertices[3].getFormulaVariable("z").getValue());
        assertEquals(23, (int) vertices[4].getFormulaVariable("z").getValue());
        assertEquals(3, (int) vertices[5].getFormulaVariable("z").getValue());
    }

    @Test
    public void asd(){

        //DefaultDirectedGraph<Object, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        //factory = new MyVertexFactory(IntDataType.class);
//
        ////String v1 = "1 goal = 0;
        //HashMap<String, AbstractDataType> v1 = new HashMap<>();
        //v1.put("goal",AbstractDataType.newInstance(IntDataType.class, 0));
        //V1 = factory.createVertex(v1);
//
//
        //HashMap<String, AbstractDataType> v2 = new HashMap<>();
        //v2.put("goal",AbstractDataType.newInstance(IntDataType.class, Integer.MAX_VALUE));
//
        //V2 = factory.createVertex(v2);
//
        //g.addVertex(V1);
        //g.addVertex(V2);
//
        //MyEdge e = g.addEdge(V1, V2);
        //e.addEdgeCapability("f1(x,s,t):= x+1+s.1");
        //e.addEdgeCapability("f2(x)(s,t):= x+1+s.1");
        //try {
        //    e.addEdgeCapability("f3(x,s,t):= x+1+x.1");
        //    assertFalse(true);
        //}
        //catch (ParseCancellationException ex){
        //    assertTrue(true);
        //    ex.printStackTrace();
        //}
        //try {
        //    e.addEdgeCapability("f4(x)(s,t):= x+1+x.1");
        //    assertFalse(true);
        //}
        //catch (ParseCancellationException ex){
        //    assertTrue(true);
        //    ex.printStackTrace();
        //}
        //try {
        //    e.addEdgeCapability("f5(x,s,t):= s.(1+2)");
        //    assertFalse(true);
        //}
        //catch (ParseCancellationException ex){
        //    assertTrue(true);
        //    ex.printStackTrace();
        //}
    }

    @Test
    public void testFilter() throws Exception {
        String input = "#Vertexes (SourceID, Name (, item)*)\n" +
                "1,,me:1\n" +
                "2,,me:1\n" +
                "3,,me:1\n" +
                "4,,me:1\n" +
                "5,,me:1\n" +
                "6,,me:1\n" +
                "7,,me:1\n" +
                "\n" +
                "#Edges (SourceID, TargetID, (Capability|Item)*)\n" +
                "1,2,\"f(x,s,t):=x\"\n" +
                "1,1,\"f(x,s,t):=x\"\n" +
                "1,3,\"f(x,s,t):=x\"\n" +
                "2,4,\"f(x,s,t):=x\"\n" +
                "3,4,\"f(x,s,t):=x\"\n" +
                "4,5,\"f(x,s,t):=x*(asNodeSet(s)+asNodeSet(t))\"\n" +
                "5,6,\"f(x,s,t):=x\"\n" +
                "6,7,\"f(x,s,t):=x\"\n" +
                "7,5,\"f(x,s,t):=x\"";
        DefaultDirectedGraph<MyVertex, MyEdge> g = null;
        try {
            g = CSV.deserialize(NodeSetDataType.class, input);
            assertTrue(true);
        }
        catch (NumberFormatException e){
            assertTrue(false);
        }

        SMuC.performQuery(NodeSetDataType.class, g, "lfp R . me + (+>f<R) + (+<f>R) ");

        MyVertex[] vertices =  g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        //DOT.printGraph(g, "filterTest.pdf");

        assertEquals("[1, 2, 3, 4, 5]",vertices[0].getFormulaVariable("R").getValue().toString());
        assertEquals("[1, 2, 3, 4, 5]",vertices[1].getFormulaVariable("R").getValue().toString());
        assertEquals("[1, 2, 3, 4, 5]",vertices[2].getFormulaVariable("R").getValue().toString());
        assertEquals("[1, 2, 3, 4, 5]",vertices[3].getFormulaVariable("R").getValue().toString());
        assertEquals("[4, 5, 6, 7]",vertices[4].getFormulaVariable("R").getValue().toString());
        assertEquals("[4, 5, 6, 7]",vertices[5].getFormulaVariable("R").getValue().toString());
        assertEquals("[4, 5, 6, 7]",vertices[6].getFormulaVariable("R").getValue().toString());
    }


    @Test
    public void testEdgeCapVariable() throws Exception {
        String input =
                "#Types\n" +
                "hi, smucparser.datatypes.simple.IntDataType\n"+
                "#Vertexes (SourceID, Name (, item)*)\n" +
                "1,,me:1\n" +
                "2,,me:2\n" +
                "3,,me:3\n" +
                "4,,me:4\n" +
                "5,,me:5\n" +
                "6,,me:6\n" +
                "7,,me:7\n" +
                "\n" +
                "#Edges (SourceID, TargetID, (Capability|Item)*)\n" +
                "1,2,\"f(x,s,t):=x\"\n" +
                "#1,1,\"f(x,s,t):=x\"\n" +
                "1,3,\"f(x,s,t):=x\"\n" +
                "2,4,\"f(x)(s,t):=x\"\n" +
                "3,4,\"f(x,s,t):=x\"\n" +
                "4,5,hi:10,\"f(x,s,t):=x+hi*asInt(t)+asInt(s)\"\n" +
                "5,6,\"f(x,s,t):=x\"\n" +
                "7,6,\"f(x,s,t):=x\"\n" +
                "7,5,\"f(x,s,t):=x\"\n";
        DefaultDirectedGraph<MyVertex, MyEdge> g = null;
        try {
            g = CSV.deserialize(IntDataType.class, input);
            assertTrue(true);
        }
        catch (NumberFormatException e){
            assertTrue(false);
        }

        SMuC.performQuery(IntDataType.class, g, "lfp R.me + (+>f<R)");

        MyVertex[] vertices =  g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        //DOT.printGraph(g, "filterTest2.pdf");

        assertEquals("1",vertices[0].getFormulaVariable("R").getValue().toString());
        assertEquals("3",vertices[1].getFormulaVariable("R").getValue().toString());
        assertEquals("4",vertices[2].getFormulaVariable("R").getValue().toString());
        assertEquals("11",vertices[3].getFormulaVariable("R").getValue().toString());
        assertEquals("77",vertices[4].getFormulaVariable("R").getValue().toString());
        assertEquals("90",vertices[5].getFormulaVariable("R").getValue().toString());
        assertEquals("7",vertices[6].getFormulaVariable("R").getValue().toString());
    }

    @Test
    public void testMissingEdgeCapabilitiesAsIdentity() throws Exception {
        String input = "#Vertexes (SourceID, Name (, item)*)\n" +
                "1,,me:1\n" +
                "2,,me:1\n" +
                "3,,me:1\n" +
                "4,,me:1\n" +
                "5,,me:1\n" +
                "6,,me:1\n" +
                "7,,me:1\n" +
                "\n" +
                "#Edges (SourceID, TargetID, (Capability|Item)*)\n" +
                "1,2\n" +
                "1,1\n" +
                "1,3\n" +
                "2,4\n" +
                "3,4\n" +
                "4,5,\"f(x,s,t):=x*(asNodeSet(s)+asNodeSet(t))\"\n" +
                "5,6\n" +
                "6,7\n" +
                "7,5";
        DefaultDirectedGraph<MyVertex, MyEdge> g = null;
        try {
            g = CSV.deserialize(NodeSetDataType.class, input);
            assertTrue(true);
        }
        catch (NumberFormatException e){
            assertTrue(false);
        }

        SMuC.performQuery(NodeSetDataType.class, g, "lfp R . me + (+>f<R) + (+<f>R) ");

        MyVertex[] vertices =  g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        //DOT.printGraph(g, "filterTest.pdf");

        assertEquals("[1, 2, 3, 4, 5]",vertices[0].getFormulaVariable("R").getValue().toString());
        assertEquals("[1, 2, 3, 4, 5]",vertices[1].getFormulaVariable("R").getValue().toString());
        assertEquals("[1, 2, 3, 4, 5]",vertices[2].getFormulaVariable("R").getValue().toString());
        assertEquals("[1, 2, 3, 4, 5]",vertices[3].getFormulaVariable("R").getValue().toString());
        assertEquals("[4, 5, 6, 7]",vertices[4].getFormulaVariable("R").getValue().toString());
        assertEquals("[4, 5, 6, 7]",vertices[5].getFormulaVariable("R").getValue().toString());
        assertEquals("[4, 5, 6, 7]",vertices[6].getFormulaVariable("R").getValue().toString());
    }
}
