package smucparser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.simple.IntDataType;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.util.HashMap;

import static java.lang.Integer.MAX_VALUE;
import static org.junit.Assert.*;

/**
 * Created by Martin on 08-02-2016.
 */
public class MyVertexTest {

    private MyVertex testVertex1;
    private MyVertex testVertex2;
    private MyVertex testVertex3;

    @Before
    public void setUp() throws Exception {
        HashMap<String, AbstractDataType> v4 = new HashMap<String, AbstractDataType>()
        {{
            put("goal",AbstractDataType.newInstance(IntDataType.class, MAX_VALUE));
            put("j",   AbstractDataType.newInstance(IntDataType.class, 1));
        }};

        MyVertexFactory a = new MyVertexFactory(IntDataType.class);

        // Used for getId and getAttributeValue
        this.testVertex1 = a.createVertex(v4);
        this.testVertex2 = a.createVertex(v4);

        //GetFormulaVariable
        this.testVertex3 = a.createVertex(v4);
        this.testVertex3.addFormulaVariableIteration("z", 0, AbstractDataType.newInstance(IntDataType.class, 11));
        this.testVertex3.addFormulaVariableIteration("z", 1, AbstractDataType.newInstance(IntDataType.class, 12));
        this.testVertex3.addFormulaVariableIteration("z", 2, AbstractDataType.newInstance(IntDataType.class, 15));

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetId() throws Exception {
        assertTrue(this.testVertex1.getId() < this.testVertex2.getId());
    }

    @Test
    public void testGetItem() throws Exception {
        assertEquals(MAX_VALUE , (int)this.testVertex1.getAttributeValue("goal"));
        assertEquals(1 , (int)this.testVertex1.getAttributeValue("j"));
        assertEquals(MAX_VALUE , (int)this.testVertex2.getAttributeValue("goal"));
        assertEquals(1 , (int)this.testVertex2.getAttributeValue("j"));
    }

    @Test
    public void testAddFormulaVariableIteration() throws Exception {
        this.testVertex1.addFormulaVariableIteration("j", 0, AbstractDataType.newInstance(IntDataType.class, 5));
        this.testVertex1.addFormulaVariableIteration("j", 0, AbstractDataType.newInstance(IntDataType.class, 3));
        this.testVertex1.addFormulaVariableIteration("j", 1, AbstractDataType.newInstance(IntDataType.class, 7));
        //todo
        //assertEquals( 3, (int)this.testVertex1.getFormulaVariable("j", 0).getValue());
        //assertEquals( 7, (int)this.testVertex1.getFormulaVariable("j", 1).getValue());
    }

    @Test
    public void testGetFormulaVariable() throws Exception {
        //todo
        //assertEquals( 11, (int)this.testVertex3.getFormulaVariable("z", 0).getValue());
        //assertEquals( 12, (int)this.testVertex3.getFormulaVariable("z", 1).getValue());
        //assertEquals( 12, (int)this.testVertex3.getFormulaVariable("z", 1).getValue());
        //assertEquals( 15, (int)this.testVertex3.getFormulaVariable("z", 2).getValue());
//
        //assertEquals(null, this.testVertex3.getFormulaVariable("z",3));
        //assertEquals(null, this.testVertex3.getFormulaVariable("a",0));
    }

    @Test
    public void testHasChanged() throws Exception {
        //todo
        //this.testVertex1.addFormulaVariableIteration("j", 0, new AbstractDataType(5,new IntDataType()));
        //assertTrue(this.testVertex1.hasChanged("j"));
        //this.testVertex1.addFormulaVariableIteration("j", 1, new AbstractDataType(5,new IntDataType()));
        //assertFalse(this.testVertex1.hasChanged("j"));
//
        //this.testVertex1.addFormulaVariableIteration("k", 0, new AbstractDataType(5,new IntDataType()));
        //assertTrue(this.testVertex1.hasChanged("k"));
//
//
        //this.testVertex1.addFormulaVariableIteration("k", 1, new AbstractDataType(1,new IntDataType()));
        //assertTrue(this.testVertex1.hasChanged("k"));
//
//
        //this.testVertex1.addFormulaVariableIteration("j", 2, new AbstractDataType(1,new IntDataType()));
        //this.testVertex1.addFormulaVariableIteration("k", 2, new AbstractDataType(1,new IntDataType()));
        //assertTrue(this.testVertex1.hasChanged("j"));
        //assertFalse(this.testVertex1.hasChanged("k"));


    }
}