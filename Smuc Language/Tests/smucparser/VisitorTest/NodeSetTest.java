package smucparser.VisitorTest;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.Before;
import org.junit.Test;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.complex.NodeSetDataType;
import smucparser.datatypes.complex.PathSetDataType;
import smucparser.graph.Import.DOT;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Martin on 17-03-2016.
 */
public class NodeSetTest {

    private HashSet<MyVertex> vertices = new HashSet<>();
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;

    @Before
    public void setUp() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        MyVertexFactory factory = new MyVertexFactory();

        //String v1 = "1 goal = 0;
        HashMap<String, AbstractDataType> v1 = new HashMap<>();
        NodeSetDataType a = (NodeSetDataType) AbstractDataType.getInstance(NodeSetDataType.class);
        v1.put("goal", AbstractDataType.newInstance(a.getClass(), new HashSet<>()));
        MyVertex V1 = factory.createVertex(v1);


        HashMap<String, AbstractDataType> v2 = new HashMap<>();
        v2.put("goal", AbstractDataType.newInstance(NodeSetDataType.class, a.getTop()));


        MyVertex V2 = factory.createVertex(v2);
        MyVertex V3 = factory.createVertex(v2);
        MyVertex V4 = factory.createVertex(v2);
        MyVertex V5 = factory.createVertex(v2);
        MyVertex V6 = factory.createVertex(v2);
        MyVertex V7 = factory.createVertex(v2);
        MyVertex V8 = factory.createVertex(v2);
        MyVertex V9 = factory.createVertex(v2);
        MyVertex V10 = factory.createVertex(v1);
        MyVertex V11 = factory.createVertex(v2);
        MyVertex V12 = factory.createVertex(v2);

        vertices.add(V1);
        vertices.add(V2);
        vertices.add(V3);
        vertices.add(V4);
        vertices.add(V5);
        vertices.add(V6);
        vertices.add(V7);
        vertices.add(V8);
        vertices.add(V9);
        vertices.add(V10);;
        vertices.add(V11);
        vertices.add(V12);


        // add the vertices
        for(MyVertex v : vertices){
            g.addVertex(v);
        }

        g.addEdge(V1, V2);
        g.addEdge(V2, V3);
        g.addEdge(V3, V4);
        g.addEdge(V4, V1);
        g.addEdge(V8, V7);
        g.addEdge(V7, V4);
        g.addEdge(V6, V3);
        g.addEdge(V5, V6);
        g.addEdge(V8, V9);
        g.addEdge(V9, V10);
        g.addEdge(V10, V9);
        g.addEdge(V6, V8);
        g.addEdge(V11, V11);
        g.addEdge(V11, V12);
        this.graph = g;
    }

    @Test
    public void asd() throws Exception{
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        SMuC.performQuery(NodeSetDataType.class, g, "lfp Reachable_goals. union(Reachable_goals, union<>Reachable_goals, complements(1,goal))");    // Set of goals
        SMuC.performQuery(NodeSetDataType.class, g, "lfp reachable. union<> union(reachable, 1)");    // Results in a set of reachable nodes
////////
        SMuC.performQuery(NodeSetDataType.class, g, "lfp Can_reach_goal. containsOne(reachable,Reachable_goals)");    // Set of goals
        SMuC.performQuery(NodeSetDataType.class, g, "lfp z. intersection(Can_reach_goal, union<>union(z,1)) "); // Nodes that can reach one of the goals
        SMuC.performQuery(NodeSetDataType.class, g, "lfp nextToGoal. intersection(goal, z, union<>1) ");       //Set of nodes which lead to a goal node


        SMuC.performQuery(PathSetDataType.class, g, "lfp path_goal. asPath(goal)");
        SMuC.performQuery(PathSetDataType.class, g, "lfp shortest_paths. 1+min(path_goal, 1+min<> shortest_paths)");
        //DOT.printGraph(g, "nodeSet.pdf");
    }

}
