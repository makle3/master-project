package smucparser.VisitorTest;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.Before;
import org.junit.Test;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.complex.HoareShortestPath;
import smucparser.datatypes.complex.Path;
import smucparser.datatypes.simple.IntDataType;
import smucparser.datatypes.complex.PathSetDataType;
import smucparser.graph.Import.CSV;
import smucparser.graph.Import.DOT;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Martin on 21-02-2016.
 */
public class ShortestPathTest {
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;
    private ArrayList<MyVertex> vertices = new ArrayList<>();


    @Before
    public void setUp() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        MyVertexFactory factory = new MyVertexFactory();

        //String v1 = "1 goal = 0;
        HashMap<String, AbstractDataType> v1 = new HashMap<>();
        AbstractDataType b = new PathSetDataType();
        b.setValue(AbstractDataType.getInstance(PathSetDataType.class).getBottom());
        v1.put("goal",b);
        MyVertex V1 = factory.createVertex(v1);
        V1.setId(0L);

        AbstractDataType a = new PathSetDataType();
        a.setValue(AbstractDataType.getInstance(PathSetDataType.class).getTop());
        HashMap<String, AbstractDataType> v2 = new HashMap<>();
        v2.put("goal",  a);


        MyVertex V2 = factory.createVertex(v2);
        V2.setId(1L);
        MyVertex V3 = factory.createVertex(v2);
        V3.setId(2L);
        MyVertex V4 = factory.createVertex(v2);
        V4.setId(3L);
        MyVertex V5 = factory.createVertex(v2);
        V5.setId(4L);
        MyVertex V6 = factory.createVertex(v2);
        V6.setId(5L);
        MyVertex V7 = factory.createVertex(v2);
        V7.setId(6L);
        MyVertex V8 = factory.createVertex(v2);
        V8.setId(7L);
        MyVertex V9 = factory.createVertex(v2);
        V9.setId(8L);
        MyVertex V10 = factory.createVertex(v1);
        V10.setId(9L);
        MyVertex V11 = factory.createVertex(v2);
        V10.setId(10L);
        MyVertex V12 = factory.createVertex(v2);
        V10.setId(11L);
        vertices.add(V1);
        vertices.add(V2);
        vertices.add(V3);
        vertices.add(V4);
        vertices.add(V5);
        vertices.add(V6);
        vertices.add(V7);
        vertices.add(V8);
        vertices.add(V9);
        vertices.add(V10);
        vertices.add(V11);
        vertices.add(V12);


        // add the vertices
        for(MyVertex v : vertices){
            g.addVertex(v);
        }

        g.addEdge(V1, V2);
        g.addEdge(V2, V3);
        g.addEdge(V3, V4);
        g.addEdge(V4, V1);
        g.addEdge(V8, V7);
        g.addEdge(V7, V4);
        g.addEdge(V6, V3);
        g.addEdge(V5, V6);
        g.addEdge(V8, V9);
        g.addEdge(V9, V10);
        g.addEdge(V10, V9);
        g.addEdge(V6, V8);

        g.addEdge(V5, V11);
        g.addEdge(V11, V12);
        g.addEdge(V12, V5);
        g.addEdge(V11, V5);
        g.addEdge(V12, V11);
        g.addEdge(V5, V12);
        this.graph = g;

    }

    @Test
    public void testParse() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        String s = "gfp z. min(goal, min<> addNode(z,1))";
        SMuC.performQuery(PathSetDataType.class, g,s);
        DOT.printGraph(DOT.serialize(g), "shortestPath.pdf");


        MyVertex[] paths = g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);


        Set<Path> node0 = (Set<Path>) paths[0].getFormulaVariable("z").getValue();
        assertEquals(0,node0.size());

        Set<Path> node1 = (Set<Path>) paths[1].getFormulaVariable("z").getValue();
        assertEquals(1,node1.size());
        Path path1 = node1.iterator().next();
        assertEquals(3,(int)path1.size());
        assertTrue(path1.contains(this.vertices.get(0).getId()));
        assertTrue(path1.contains(this.vertices.get(2).getId()));
        assertTrue(path1.contains(this.vertices.get(3).getId()));
        assertFalse(path1.contains(this.vertices.get(4).getId()));

        Set<Path> node2 = (Set<Path>) paths[2].getFormulaVariable("z").getValue();
        assertEquals(1,node2.size());
        Path path2 = node2.iterator().next();
        assertEquals(2,(int)path2.size());
        assertTrue(path2.contains(this.vertices.get(0).getId()));
        assertTrue(path2.contains(this.vertices.get(3).getId()));

        Set<Path> node3 = (Set<Path>) paths[3].getFormulaVariable("z").getValue();
        assertEquals(1,node3.size());
        Path path3 = node3.iterator().next();
        assertEquals(1,(int)path3.size());
        assertTrue(path3.contains(this.vertices.get(0).getId()));

        Set<Path> node4 = (Set<Path>) paths[4].getFormulaVariable("z").getValue();
        assertEquals(2,node4.size());
        Iterator<Path> iterator = node4.iterator();
        Path path4_1 = iterator.next();
        assertEquals(4,(int)path4_1.size());
        assertTrue(path4_1.contains(this.vertices.get(5).getId()));
        assertTrue(path4_1.contains(this.vertices.get(3).getId()));
        assertTrue(path4_1.contains(this.vertices.get(5).getId()));
        assertTrue(path4_1.contains(this.vertices.get(0).getId()));

        Path path4_2 = iterator.next();
        assertEquals(4,(int)path4_2.size());
        assertTrue(path4_2.contains(this.vertices.get(5).getId()));
        assertTrue(path4_2.contains(this.vertices.get(7).getId()));
        assertTrue(path4_2.contains(this.vertices.get(8).getId()));
        assertTrue(path4_2.contains(this.vertices.get(9).getId()));

    }

    @Test
    public void testCsv() throws Exception{
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        String s = CSV.serialize(g);

        DefaultDirectedGraph<MyVertex, MyEdge> g2 = CSV.deserialize(IntDataType.class,  s);
        int i = 0;

    }


    @Test
    public void testTop() throws Exception{
        String s = "";
        try (BufferedReader br = new BufferedReader(new FileReader("./example-graphs/MultiType.csv"))) {
            while (br.ready()){
                s += br.readLine()+"\n";
            }
        }

        DefaultDirectedGraph<MyVertex, MyEdge> g = CSV.deserialize(PathSetDataType.class, s);

        SMuC.performQuery(PathSetDataType.class, g, "gfp z. 1+min(goal, min<> z+1)");

        //DOT.printGraph(g, "sh.pdf");
        int i = 0;

    }

}
