package smucparser.VisitorTest;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.Before;
import org.junit.Test;
import smucparser.SMuC;
import smucparser.datatypes.simple.specialized.PageRankDataType;
import smucparser.graph.Import.DOT;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.text.MessageFormat;

import static org.junit.Assert.assertEquals;

/**
 * Created by Martin on 19-02-2016.
 */
public class PageRankTest {
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;

    @Before
    public void setUp() throws Exception {

        MyVertexFactory factory = new MyVertexFactory();


        MyVertex A = factory.createVertex();
        MyVertex B = factory.createVertex();
        MyVertex C = factory.createVertex();
        MyVertex D = factory.createVertex();

        // add the vertices
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        g.addVertex(A);
        g.addVertex(B);
        g.addVertex(C);
        g.addVertex(D);

        g.addEdge(A, B);
        g.addEdge(B, C);
        g.addEdge(C, A);
        g.addEdge(A, C);
        g.addEdge(D, C);

        this.graph = g;

    }

    @Test
    public void testParse() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        Double dampingFactor = 0.85;

        //PR(A) = (1-d) + d (PR(T1)/C(T1) + ... + PR(Tn)/C(Tn))
        String s = MessageFormat.format("lfp rank. (1-{1}) + {1} * +><  (rank / +<>1)", g.vertexSet().size(), String.valueOf(dampingFactor));
        SMuC.performQuery(PageRankDataType.class, g,s);
        DOT.printGraph(g, "pagerank.pdf");
        MyVertex[] vertices = g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        assertEquals(1.49, (Double)vertices[0].getFormulaVariable("rank").getValue(), 0.01);
        assertEquals(0.78, (Double)vertices[1].getFormulaVariable("rank").getValue(), 0.01);
        assertEquals(1.58, (Double)vertices[2].getFormulaVariable("rank").getValue(), 0.01);
        assertEquals(0.15, (Double)vertices[3].getFormulaVariable("rank").getValue(), 0.01);

    }
}
