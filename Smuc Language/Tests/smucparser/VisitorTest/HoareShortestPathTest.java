package smucparser.VisitorTest;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.Before;
import org.junit.Test;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.complex.HoareShortestPath;
import smucparser.datatypes.complex.Path;
import smucparser.datatypes.complex.HoareShortestPath;
import smucparser.datatypes.simple.IntDataType;
import smucparser.graph.Import.CSV;
import smucparser.graph.Import.DOT;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Martin on 21-02-2016.
 */
public class HoareShortestPathTest {
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;
    private ArrayList<MyVertex> vertices = new ArrayList<>();


    @Before
    public void setUp() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        MyVertexFactory factory = new MyVertexFactory();

        //String v1 = "1 goal = 0;
        HashMap<String, AbstractDataType> v1 = new HashMap<>();
        AbstractDataType b = new HoareShortestPath();
        b.setValue(AbstractDataType.getInstance(HoareShortestPath.class).getBottom());
        v1.put("goal",b);
        MyVertex V1 = factory.createVertex(v1);
        V1.setId(0L);

        AbstractDataType a = new HoareShortestPath();
        a.setValue(AbstractDataType.getInstance(HoareShortestPath.class).getTop());
        HashMap<String, AbstractDataType> v2 = new HashMap<>();
        v2.put("goal",  a);


        MyVertex V2 = factory.createVertex(v2);
        V2.setId(1L);
        MyVertex V3 = factory.createVertex(v2);
        V3.setId(2L);
        MyVertex V4 = factory.createVertex(v2);
        V4.setId(3L);
        MyVertex V5 = factory.createVertex(v2);
        V5.setId(4L);
        MyVertex V6 = factory.createVertex(v2);
        V6.setId(5L);
        MyVertex V7 = factory.createVertex(v2);
        V7.setId(6L);
        MyVertex V8 = factory.createVertex(v2);
        V8.setId(7L);
        MyVertex V9 = factory.createVertex(v2);
        V9.setId(8L);
        MyVertex V10 = factory.createVertex(v1);
        V10.setId(9L);
        MyVertex V11 = factory.createVertex(v2);
        V10.setId(10L);
        MyVertex V12 = factory.createVertex(v2);
        V10.setId(11L);
        vertices.add(V1);
        vertices.add(V2);
        vertices.add(V3);
        vertices.add(V4);
        vertices.add(V5);
        vertices.add(V6);
        vertices.add(V7);
        vertices.add(V8);
        vertices.add(V9);
        vertices.add(V10);
        vertices.add(V11);
        vertices.add(V12);


        // add the vertices
        for(MyVertex v : vertices){
            g.addVertex(v);
        }

        g.addEdge(V1, V2);
        g.addEdge(V2, V3);
        g.addEdge(V3, V4);
        g.addEdge(V4, V1);
        g.addEdge(V8, V7);
        g.addEdge(V7, V4);
        g.addEdge(V6, V3);
        g.addEdge(V5, V6);
        g.addEdge(V8, V9);
        g.addEdge(V9, V10);
        g.addEdge(V10, V9);
        g.addEdge(V6, V8);

        g.addEdge(V5, V11);
        g.addEdge(V11, V12);
        g.addEdge(V12, V5);
        g.addEdge(V11, V5);
        g.addEdge(V12, V11);
        g.addEdge(V5, V12);
        this.graph = g;

    }


    @Test
    public void hoarePowerDomainShortestPath() throws Exception{
        SMuC.performQuery(HoareShortestPath.class, this.graph, "lfp z. min[]addNode(min(z, goal),1)");
        DOT.printGraph(graph, "hoare.pdf");
    }

}
