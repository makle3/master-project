package smucparser.VisitorTest;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.simple.IntDataType;
import smucparser.grammar.TypeValueAndResult;
import smucparser.grammar.generated.GrammarLexer;
import smucparser.grammar.generated.GrammarParser;
import smucparser.grammar.semantics.SmucListener;
import smucparser.grammar.visitor.SmucVisitor;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Martin on 01-03-2016.
 */
public class SmucVisitorTest {

    private String s = "lfp z. 1+min(goal,min><1)";
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;
    private MyVertex V1;
    private MyVertex V2;
    private MyVertex V8;
    private MyVertex V5;
    private AbstractDataType dataType = new IntDataType();

    @Before
    public void setUp() throws Exception {
        HashMap<String, AbstractDataType> v1 = new HashMap<String, AbstractDataType>()
        {{
            put("goal", AbstractDataType.newInstance(IntDataType.class, 0));
        }};
        HashMap<String, AbstractDataType> v2 = new HashMap<String, AbstractDataType>()
        {{
            put("goal", AbstractDataType.newInstance(IntDataType.class, 100));
        }};

        MyVertexFactory factory = new MyVertexFactory();

        V1 = factory.createVertex(v1);
        V2 = factory.createVertex(v2);
        MyVertex V3 = factory.createVertex(v2);
        MyVertex V4 = factory.createVertex(v2);
        V5 = factory.createVertex(v2);
        MyVertex V6 = factory.createVertex(v2);
        MyVertex V7 = factory.createVertex(v2);
        V8 = factory.createVertex(v2);
        MyVertex V9 = factory.createVertex(v2);
        MyVertex V10 = factory.createVertex(v1);

        // add the vertices
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);
        g.addVertex(V5);
        g.addVertex(V6);
        g.addVertex(V7);
        g.addVertex(V8);
        g.addVertex(V9);
        g.addVertex(V10);

        g.addEdge(V1, V2);
        g.addEdge(V2, V3);
        g.addEdge(V3, V4);
        g.addEdge(V4, V1);
        g.addEdge(V8, V7);
        g.addEdge(V7, V4);
        g.addEdge(V6, V3);
        g.addEdge(V5, V6);
        g.addEdge(V8, V9);
        g.addEdge(V9, V10);
        g.addEdge(V9, V10);

        this.graph = g;
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testPlusNumberVariableInt() throws Exception{
        String s = "1+goal";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);


        GrammarParser.PsiContext plus = tree.psi();
        GrammarParser.NumberContext number = plus.left.value().number();
        GrammarParser.VariableContext variable = plus.right.value().variable();



        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());

        visitor.setCurrentVertex(V2);
        assertEquals(1, ((AbstractDataType) visitor.visit(number)).getValue());
        assertEquals(100, ((AbstractDataType) visitor.visit(variable)).getValue());
        assertEquals(101, ((AbstractDataType) visitor.visit(plus)).getValue());
        visitor.setCurrentVertex(V1);
        assertEquals(1,((AbstractDataType) visitor.visit(number)).getValue());
        assertEquals(0,((AbstractDataType) visitor.visit(variable)).getValue());
        assertEquals(1,((AbstractDataType) visitor.visit(plus)).getValue());
    }

    @Test
    public void testFuncNumberVariableInt() throws Exception{
        String s = "min(1,goal)";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);


        GrammarParser.FunctionContext func = tree.psi().function();
        GrammarParser.Func_nameContext funcName = func.op;
        GrammarParser.Function_innerContext funcInner = func.inner;
        List<GrammarParser.PsiContext> values = funcInner.psi();

        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        ParseTreeProperty<TypeValueAndResult> types = SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
        visitor.setTypes(types);


        assertEquals("min", funcName.getText());

        visitor.setCurrentVertex(V2);
        assertEquals(1, ((AbstractDataType) visitor.visit(values.get(0))).getValue());
        assertEquals(100, ((AbstractDataType) visitor.visit(values.get(1))).getValue());
        assertEquals(1, ((AbstractDataType) visitor.visit(func)).getValue());
        visitor.setCurrentVertex(V1);
        assertEquals(1,((AbstractDataType) visitor.visit(values.get(0))).getValue());
        assertEquals(0,((AbstractDataType) visitor.visit(values.get(1))).getValue());
        assertEquals(0, ((AbstractDataType) visitor.visit(func)).getValue());
    }

    @Test
    public void testFuncNotFound() throws Exception{
        String s = "arkhja(1,goal)";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);

        try {
            SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
            ParseTreeWalker walker = new ParseTreeWalker();
            walker.walk(listener, tree);
            assertTrue(false);
        }
        catch (ParseCancellationException ex){
            assertTrue(true);
            assertEquals("Method arkhja has not been defined for smucparser.datatypes.simple.IntDataType", ex.getMessage());
        }
    }

    @Test
    public void testGetGraph() throws Exception{
        String s = "1";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        assertEquals(g, visitor.getGraph());
    }

    @Test
    public void testVariableNotFound() throws Exception{
        String s = "goal2";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();

        try {
            walker.walk(listener, tree);
            assertTrue(false);
        }
        catch (ParseCancellationException ex){
            assertTrue(true);

            assertTrue(ex.getMessage().endsWith("is missing NODE_ATTRIBUTE \"goal2\"."));
        }
    }

    @Test
    public void testVisitAggregateOutgoingSum() throws Exception{
        String s = "+<>goal";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);


        GrammarParser.AggregateContext aggr = tree.psi().aggregate();
        GrammarParser.PsiContext psi = aggr.psi();

        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        ParseTreeProperty<TypeValueAndResult> types = SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
        visitor.setTypes(types);




        visitor.setCurrentVertex(V8);
        assertEquals(200, ((AbstractDataType) visitor.visit(aggr)).getValue());
        assertEquals(100, ((AbstractDataType) visitor.visit(psi)).getValue());
        visitor.setCurrentVertex(V1);
        assertEquals(100,((AbstractDataType) visitor.visit(aggr)).getValue());
        assertEquals(0,  ((AbstractDataType) visitor.visit(psi)).getValue());  //Performing Psi on node 1 which has goal 0, and not the ingoing node
        visitor.setCurrentVertex(V5);
        assertEquals(100,((AbstractDataType) visitor.visit(aggr)).getValue());
        assertEquals(100,((AbstractDataType) visitor.visit(psi)).getValue());
    }

    @Test
    public void testVisitAggregateIngoingSum() throws Exception{
        String s = "+><goal";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        DefaultDirectedGraph g = (DefaultDirectedGraph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);


        GrammarParser.AggregateContext aggr = tree.psi().aggregate();
        GrammarParser.PsiContext psi = aggr.psi();

        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        ParseTreeProperty<TypeValueAndResult> types = SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
        visitor.setTypes(types);


        //DOT.printGraph(g,"test.pdf");

        visitor.setCurrentVertex(V8);
        assertEquals(0,  ((AbstractDataType) visitor.visit(aggr)).getValue());
        assertEquals(100,((AbstractDataType) visitor.visit(psi)).getValue());
        visitor.setCurrentVertex(V1);
        assertEquals(100,((AbstractDataType) visitor.visit(aggr)).getValue());
        assertEquals(0,  ((AbstractDataType) visitor.visit(psi)).getValue());   //Performing Psi on node 1 which has goal 0, and not the ingoing node
        visitor.setCurrentVertex(V5);
        assertEquals(0,  ((AbstractDataType) visitor.visit(aggr)).getValue());
        assertEquals(100,((AbstractDataType) visitor.visit(psi)).getValue());
    }

    @Test
    public void testVisitAggregateUndirectedSum() throws Exception{
        String s = "+[]goal";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);


        GrammarParser.AggregateContext aggr = tree.psi().aggregate();
        GrammarParser.PsiContext psi = aggr.psi();

        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        ParseTreeProperty<TypeValueAndResult> types = SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
        visitor.setTypes(types);




        visitor.setCurrentVertex(V8);
        assertEquals(200,((AbstractDataType) visitor.visit(aggr)).getValue());
        assertEquals(100,((AbstractDataType) visitor.visit(psi)).getValue());
        visitor.setCurrentVertex(V1);
        assertEquals(200,((AbstractDataType) visitor.visit(aggr)).getValue());
        assertEquals(0,  ((AbstractDataType) visitor.visit(psi)).getValue());  //Performing Psi on node 1 which has goal 0, and not the ingoing node
        visitor.setCurrentVertex(V5);
        assertEquals(100,((AbstractDataType) visitor.visit(aggr)).getValue());
        assertEquals(100,((AbstractDataType) visitor.visit(psi)).getValue());
    }

    @Test
    public void testVisitLfp1() throws Exception{
        String s = "lfp z. 0";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);

        GrammarParser.FixpointContext fp = tree.psi().fixpoint();

        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());


        visitor.setCurrentVertex(V8);
        visitor.visit(fp);
        Set<MyVertex> vertexes = visitor.getGraph().vertexSet();
        int i = 0;
        for(MyVertex v : vertexes){
            assertEquals(0, v.getFormulaVariable("z").getValue());
            i++;
        }
        assertEquals(i, vertexes.size());
        ////DOT.printGraph((DefaultDirectedGraph<Object, Object>) g,"output.pdf");
    }

    @Test
    public void testVisitLfp2() throws Exception{
        String s = "lfp z. max(z,1)";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);

        GrammarParser.FixpointContext fp = tree.psi().fixpoint();

        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        ParseTreeProperty<TypeValueAndResult> types = SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
        visitor.setTypes(types);



        visitor.setCurrentVertex(V8);
        visitor.visit(fp);
        Set<MyVertex> vertexes = visitor.getGraph().vertexSet();
        int i = 0;
        for(MyVertex v : vertexes){
            assertEquals(1, v.getFormulaVariable("z").getValue());
            i++;
        }
        assertEquals(i, vertexes.size());
    }

    @Test
    public void testVisitLfp3() throws Exception{
        String s = "lfp z. z";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);

        GrammarParser.FixpointContext fp = tree.psi().fixpoint();

        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());


        visitor.setCurrentVertex(V8);
        visitor.visit(fp);
        Set<MyVertex> vertexes = visitor.getGraph().vertexSet();
        int i = 0;
        for(MyVertex v : vertexes){
            assertEquals(0, v.getFormulaVariable("z").getValue());
            i++;
        }
        assertEquals(i, vertexes.size());
    }

    @Test
    public void testVisitLfp4() throws Exception{
        //String s = "lfp z. min(goal, +(min<>z,min><z))";
        String s = "lfp z. min(goal, min<>z+1)";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);

        GrammarParser.FixpointContext fp = tree.psi().fixpoint();

        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        ParseTreeProperty<TypeValueAndResult> types = SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
        visitor.setTypes(types);


        visitor.setCurrentVertex(V8);
        visitor.visit(fp);
        //DOT.printGraph((DefaultDirectedGraph<Object, Object>) g,"output.pdf");
        Iterator<MyVertex> vertexes = visitor.getGraph().vertexSet().iterator();
        assertEquals(0,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(3,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(2,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(1,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(4,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(3,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(2,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(2,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(1,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(0,vertexes.next().getFormulaVariable("z").getValue());
    }

    @Test
    public void testVisitGfp3() throws Exception{
        String s = "gfp z. z";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);

        GrammarParser.FixpointContext fp = tree.psi().fixpoint();

        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());


        visitor.setCurrentVertex(V8);
        visitor.visit(fp);
        Set<MyVertex> vertexes = visitor.getGraph().vertexSet();
        int i = 0;
        for(MyVertex v : vertexes){
            assertEquals(Integer.MAX_VALUE, v.getFormulaVariable("z").getValue());
            i++;
        }
        assertEquals(i, vertexes.size());
        assertTrue(i>7);
    }

    @Test
    public void testVisitGfp4() throws Exception{
        //String s = "lfp z. min(goal, +(min<>z,min><z))";
        String s = "gfp z. min(goal, min<>z+1)";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);

        GrammarParser.FixpointContext fp = tree.psi().fixpoint();

        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        ParseTreeProperty<TypeValueAndResult> types = SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
        visitor.setTypes(types);

        visitor.setCurrentVertex(V8);
        visitor.visit(fp);
        //DOT.printGraph((DefaultDirectedGraph<Object, Object>) g,"output.pdf");
        Iterator<MyVertex> vertexes = visitor.getGraph().vertexSet().iterator();
        assertEquals(0,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(3,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(2,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(1,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(4,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(3,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(2,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(2,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(1,vertexes.next().getFormulaVariable("z").getValue());
        assertEquals(0,vertexes.next().getFormulaVariable("z").getValue());
    }

    @Test
    public void testExistingFormulaVariable() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        MyVertexFactory factory = new MyVertexFactory();
        HashMap<String, AbstractDataType> v1 = new HashMap<>();
        v1.put("z", AbstractDataType.newInstance(IntDataType.class, 0));

        MyVertex V1 = factory.createVertex(v1);
        g.addVertex(V1);

        SMuC.performQuery(dataType.getClass(), g, "gfp z. z");

        assertEquals(Integer.MAX_VALUE, g.vertexSet().iterator().next().getFormulaVariable("z").getValue());
        assertEquals(0, g.vertexSet().iterator().next().getAttributeValue("z"));

    }



    @Test
    public void testVisitVariable1() throws Exception{
        //String s = "lfp z. min(goal, +(min<>z,min><z))";
        String s = "goal";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);

        GrammarParser.VariableContext var = tree.psi().value().variable();

        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());


        visitor.setCurrentVertex(V8);
        assertEquals(100, ((AbstractDataType) visitor.visit(var)).getValue());
        visitor.setCurrentVertex(V1);
        assertEquals(0, ((AbstractDataType) visitor.visit(var)).getValue());
    }

    @Test
    public void testVisitFunctionPlus1() throws Exception{
        //String s = "lfp z. min(goal, +(min<>z,min><z))";
        String s = "+(1)";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);


        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        ParseTreeProperty<TypeValueAndResult> types = SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
        visitor.setTypes(types);



        assertEquals(1, ((AbstractDataType) visitor.visit(tree)).getValue());
    }
    @Test
    public void testVisitFunctionPlus2() throws Exception{
        //String s = "lfp z. min(goal, +(min<>z,min><z))";
        String s = "+(1,2+3)";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);


        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        ParseTreeProperty<TypeValueAndResult> types = SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
        visitor.setTypes(types);



        assertEquals(6, ((AbstractDataType) visitor.visit(tree)).getValue());
        //DOT.printGraph((DefaultDirectedGraph<Object, Object>) g,"output.pdf");
    }
    @Test
    public void testVisitFunctionPlus3() throws Exception{
        //String s = "lfp z. min(goal, +(min<>z,min><z))";
        String s = "+(1,1,1,1,1,1,1,1,1)";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);


        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        ParseTreeProperty<TypeValueAndResult> types = SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
        visitor.setTypes(types);



        assertEquals(9, ((AbstractDataType) visitor.visit(tree)).getValue());
        //DOT.printGraph((DefaultDirectedGraph<Object, Object>) g,"output.pdf");
    }
    @Test
    public void testVisitFunctionPlus0() throws Exception{
        //String s = "lfp z. min(goal, +(min<>z,min><z))";
        String s = "+(0)";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
       
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        Graph g = (Graph) graph.clone();
        SmucListener listener = new SmucListener(g);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener,tree);


        SmucVisitor visitor = new SmucVisitor(dataType, g, listener.getVarnames());
        ParseTreeProperty<TypeValueAndResult> types = SMuC.checkTypes(dataType.getClass(), g, tree).getTypesProperty();
        visitor.setTypes(types);



        assertEquals(0, ((AbstractDataType) visitor.visit(tree)).getValue());
        //DOT.printGraph((DefaultDirectedGraph<Object, Object>) g,"output.pdf");
    }


}
