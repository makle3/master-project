package smucparser.VisitorTest;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.simple.StringDataType;
import smucparser.graph.Import.DOT;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import static org.junit.Assert.assertEquals;

/**
 * Created by Martin on 16-02-2016.
 */
public class StringTest {
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;

    @Before
    public void setUp() throws Exception {
        //First define the graph. We want to find the distance to the target nodes. The nodes with goal 0 are targets.

        MyVertexFactory factory = new MyVertexFactory();

        // add the vertices
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);

        MyVertex first = null;
        MyVertex prev = null;

        for(char alphabet = 'A'; alphabet <= 'Z';alphabet++) {
            MyVertex V1 = factory.createVertex();
            V1.addAttribute("letter", AbstractDataType.newInstance(StringDataType.class, String.valueOf(alphabet)));
            g.addVertex(V1);
            if(first == null){
                first = V1;
            }
            if(prev != null){
                g.addEdge(V1,prev);
            }
            prev = V1;
        }
        g.addEdge(first,prev);



        this.graph = g;




    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testParse() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        SMuC.performQuery(StringDataType.class, g, "\"hello world\"");
        SMuC.performQuery(StringDataType.class, g, "lfp x. letter + + <>letter");
        //DOT.printGraph(g, "string.pdf");
    }
}
