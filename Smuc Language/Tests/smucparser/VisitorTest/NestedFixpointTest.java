package smucparser.VisitorTest;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.simple.IntDataType;
import smucparser.graph.Import.DOT;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * Created by Martin on 11-03-2016.
 */
public class NestedFixpointTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testNestedFixpoint() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        MyVertexFactory factory = new MyVertexFactory();
        HashMap<String, AbstractDataType> v1 = new HashMap<>();

        MyVertex V1 = factory.createVertex(v1);
        MyVertex V2 = factory.createVertex(v1);
        MyVertex V3 = factory.createVertex(v1);
        MyVertex V4 = factory.createVertex(v1);
        MyVertex V5 = factory.createVertex(v1);
        MyVertex V6 = factory.createVertex(v1);
        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);
        g.addVertex(V5);
        g.addVertex(V6);
        g.addEdge(V1,V2);
        g.addEdge(V2,V3);
        g.addEdge(V3,V4);
        g.addEdge(V3,V5);
        g.addEdge(V5,V1);
        g.addEdge(V5,V6);

        SMuC.performQuery(IntDataType.class, g, "lfp x . (lfp y . 1) + 1");
        //DOT.printGraph(g,"test.pdf");


        assertEquals(2, g.vertexSet().iterator().next().getFormulaVariable("x").getValue());

    }
    @Test
    public void testNestedFixpoint2() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        MyVertexFactory factory = new MyVertexFactory();
        HashMap<String, AbstractDataType> v1 = new HashMap<>();
        IntDataType dt = new IntDataType();
        dt.setValue(0);
        v1.put("z", dt);

        MyVertex V1 = factory.createVertex(v1);
        MyVertex V2 = factory.createVertex(v1);
        MyVertex V3 = factory.createVertex(v1);
        MyVertex V4 = factory.createVertex(v1);
        MyVertex V5 = factory.createVertex(v1);
        MyVertex V6 = factory.createVertex(v1);
        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);
        g.addVertex(V5);
        g.addVertex(V6);
        g.addEdge(V1,V2);
        g.addEdge(V2,V3);
        g.addEdge(V3,V4);
        g.addEdge(V3,V5);
        g.addEdge(V5,V1);
        g.addEdge(V5,V6);

        SMuC.performQuery(IntDataType.class, g, "lfp x . lfp y. x");


        assertEquals(0, g.vertexSet().iterator().next().getFormulaVariable("x").getValue());

    }
}
