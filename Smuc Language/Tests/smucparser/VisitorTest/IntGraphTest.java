package smucparser.VisitorTest;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.simple.IntDataType;
import smucparser.graph.Import.DOT;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * Created by Martin on 16-02-2016.
 */
public class IntGraphTest {
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;

    @Before
    public void setUp() throws Exception {
        //First define the graph. We want to find the distance to the target nodes. The nodes with goal 0 are targets.
        HashMap<String, AbstractDataType> v1 = new HashMap<String, AbstractDataType>()
        {{
            put("goal", AbstractDataType.newInstance(IntDataType.class, 0));
        }};
        HashMap<String, AbstractDataType> v2 = new HashMap<String, AbstractDataType>()
        {{
            put("goal", AbstractDataType.newInstance(IntDataType.class, Integer.MAX_VALUE));
        }};

        MyVertexFactory factory = new MyVertexFactory();

        MyVertex V1 = factory.createVertex(v1);
        MyVertex V2 = factory.createVertex(v2);
        MyVertex V3 = factory.createVertex(v2);
        MyVertex V4 = factory.createVertex(v2);
        MyVertex V5 = factory.createVertex(v2);
        MyVertex V6 = factory.createVertex(v2);
        MyVertex V7 = factory.createVertex(v2);
        MyVertex V8 = factory.createVertex(v2);
        MyVertex V9 = factory.createVertex(v2);
        MyVertex V10 = factory.createVertex(v1);

        // add the vertices
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);
        g.addVertex(V5);
        g.addVertex(V6);
        g.addVertex(V7);
        g.addVertex(V8);
        g.addVertex(V9);
        g.addVertex(V10);

        g.addEdge(V1, V2);
        g.addEdge(V2, V3);
        g.addEdge(V3, V4);
        g.addEdge(V4, V1);
        g.addEdge(V8, V7);
        g.addEdge(V7, V4);
        g.addEdge(V6, V3);
        g.addEdge(V5, V6);
        g.addEdge(V8, V9);
        g.addEdge(V9, V10);
        g.addEdge(V9, V10);

        this.graph = g;



    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testParse() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        String s = "gfp z. min(goal, min[] +(z,1))";

        SMuC.performQuery(IntDataType.class, g,s);

        MyVertex[] vertices =  g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        assertEquals(0, (int) vertices[0].getFormulaVariable("z").getValue());
        assertEquals(1, (int) vertices[1].getFormulaVariable("z").getValue());
        assertEquals(2, (int) vertices[2].getFormulaVariable("z").getValue());
        assertEquals(1, (int) vertices[3].getFormulaVariable("z").getValue());
        assertEquals(4, (int) vertices[4].getFormulaVariable("z").getValue());
        assertEquals(3, (int) vertices[5].getFormulaVariable("z").getValue());
        assertEquals(2, (int) vertices[6].getFormulaVariable("z").getValue());
        assertEquals(2, (int) vertices[7].getFormulaVariable("z").getValue());
        assertEquals(1, (int) vertices[8].getFormulaVariable("z").getValue());
        assertEquals(0, (int) vertices[9].getFormulaVariable("z").getValue());
    }

    @Test
    public void testParse2() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = (DefaultDirectedGraph<MyVertex, MyEdge>) graph.clone();
        String s = "gfp z. min(goal, min<> +(z,1))";

        SMuC.performQuery(IntDataType.class, g,s);

        //DOT.printGraph(g, "parse2.pdf");

        MyVertex[] vertices =  g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        assertEquals(0, (int) vertices[0].getFormulaVariable("z").getValue());
        assertEquals(3, (int) vertices[1].getFormulaVariable("z").getValue());
        assertEquals(2, (int) vertices[2].getFormulaVariable("z").getValue());
        assertEquals(1, (int) vertices[3].getFormulaVariable("z").getValue());
        assertEquals(4, (int) vertices[4].getFormulaVariable("z").getValue());
        assertEquals(3, (int) vertices[5].getFormulaVariable("z").getValue());
        assertEquals(2, (int) vertices[6].getFormulaVariable("z").getValue());
        assertEquals(2, (int) vertices[7].getFormulaVariable("z").getValue());
        assertEquals(1, (int) vertices[8].getFormulaVariable("z").getValue());
        assertEquals(0, (int) vertices[9].getFormulaVariable("z").getValue());
    }
}
