package smucparser;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.Test;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.complex.NeighborPathDataType;
import smucparser.datatypes.simple.DoubleDataType;
import smucparser.graph.Import.CSV;
import smucparser.graph.Import.DOT;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Created by Martin on 27-05-2016.
 */
public class ShortestPathSecure {
    @Test
    public void testShortestPathTwo() throws Exception{
        String s = "";
        try (BufferedReader br = new BufferedReader(new FileReader("./example-graphs/MultiType.csv"))) {
            while (br.ready()){
                s += br.readLine()+"\n";
            }
        }

        DefaultDirectedGraph<MyVertex, MyEdge> g = CSV.deserialize(NeighborPathDataType.class, s);
        for (MyEdge e : g.edgeSet()) {
            e.addAttribute("Weight", AbstractDataType.newInstance(DoubleDataType.class, 1.0));
            e.addEdgeCapability("a(x)(n,m):=filter(+(x,getWeight(Weight)), b(n),b(m))");
        }



        SMuC.performQuery(NeighborPathDataType.class, g, "gfp z. min(goal, min[a] minValue(\"this\", z))");

        //DOT.printGraph(g, "sh2.pdf");
        int i = 0;

    }

}
