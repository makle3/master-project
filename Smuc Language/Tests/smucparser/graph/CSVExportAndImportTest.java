package smucparser.graph;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.Before;
import org.junit.Test;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.complex.NodeSetDataType;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.datatypes.simple.BoolDataType;
import smucparser.datatypes.simple.IntDataType;
import smucparser.datatypes.simple.StringDataType;
import smucparser.graph.Import.CSV;
import smucparser.graph.Import.DOT;

import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by Martin on 04-03-2016.
 */
public class CSVExportAndImportTest {
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;
    MyVertexFactory factory;
    private MyVertex V1;
    private MyVertex V2;
    private MyVertex V3;
    private MyVertex V4;

    @Before
    public void setUp() throws Exception {
        graph = new DefaultDirectedGraph<>(MyEdge.class);
        DefaultDirectedGraph<MyVertex, MyEdge> g = graph;
        factory = new MyVertexFactory(IntDataType.class);

        HashMap<String, AbstractDataType> v1 = new HashMap<>();
        v1.put("goal",AbstractDataType.newInstance(IntDataType.class, 0));
        V1 = factory.createVertex(v1);
        V1.setId(0L);


        HashMap<String, AbstractDataType> v2 = new HashMap<>();
        v2.put("goal",AbstractDataType.newInstance(IntDataType.class, Integer.MAX_VALUE));


        V2 = factory.createVertex(v2);
        V2.setId(1L);
        V3 = factory.createVertex(v2);
        V3.setId(2L);
        V4 = factory.createVertex(v2);
        V4.setId(3L);

        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);

        String capability = "f(x,source,target):= x+1";
        MyEdge e = g.addEdge(V1, V2);
        e.addEdgeCapability(capability);
        e = g.addEdge(V2, V3);
        e.addEdgeCapability(capability);
        e = g.addEdge(V3, V4);
        e.addEdgeCapability(capability);
        e = g.addEdge(V4, V1);
        e.addEdgeCapability(capability);
    }

    @Test
    public void testGraphToCSV() throws Exception {
        assertEquals("#Types (item,type)\n" +
                        "goal,smucparser.datatypes.simple.IntDataType\n" +
                        "#Vertexes (SourceID (, item)*)\n" +
                        "0,goal:0\n" +
                        "1,goal:2147483647\n" +
                        "2,goal:2147483647\n" +
                        "3,goal:2147483647\n" +
                        "#Edges (SourceID,TargetID,(Capability|Item)*)\n" +
                        "0,1,\"f(x,source,target):=x+1\"\n" +
                        "1,2,\"f(x,source,target):=x+1\"\n" +
                        "2,3,\"f(x,source,target):=x+1\"\n" +
                        "3,0,\"f(x,source,target):=x+1\""
                , CSV.serialize(graph));

    }

    @Test
    public void testGraphToCSV2() throws Exception {
        MyEdge e = graph.getEdge(V1,V2);
        BoolDataType dt = new BoolDataType();
        dt.setValue(true);
        e.addAttribute("hi", dt);
        assertEquals("#Types (item,type)\n" +
                        "hi,smucparser.datatypes.simple.BoolDataType\n" +
                        "goal,smucparser.datatypes.simple.IntDataType\n" +
                        "#Vertexes (SourceID (, item)*)\n" +
                        "0,goal:0\n" +
                        "1,goal:2147483647\n" +
                        "2,goal:2147483647\n" +
                        "3,goal:2147483647\n" +
                        "#Edges (SourceID,TargetID,(Capability|Item)*)\n" +
                        "0,1,hi:true,\"f(x,source,target):=x+1\"\n" +
                        "1,2,\"f(x,source,target):=x+1\"\n" +
                        "2,3,\"f(x,source,target):=x+1\"\n" +
                        "3,0,\"f(x,source,target):=x+1\""
                , CSV.serialize(graph));

    }

    @Test
    public void testGraphToCSVToGraph() throws Exception {
        MyEdge e = graph.getEdge(V1,V2);
        BoolDataType dt = new BoolDataType();
        dt.setValue(true);
        StringDataType dt2 = new StringDataType();
        dt2.setValue("hi");
        e.addAttribute("hi", dt);
        e.addAttribute("h2", dt2);
        assertEquals(CSV.serialize(graph), CSV.serialize(CSV.deserialize(BoolDataType.class,CSV.serialize(graph))));

    }

    @Test
    public void testCSVToGraph() throws Exception {
        String input = "#Vertexes (SourceID (, item)*)\n" +
                "0,,goal:0,test:43\n" +
                "1,,goal:2147483647\n" +
                "2,,goal:2147483647\n" +
                "3,goal:2147483647\n" +
                "#Edges (SourceID, TargetID, (Capability|Item)*)\n" +
                "0,1,c:3,,\"f(x,s,t):=x+c\"\n" +
                "1,2,c:2,f:2,\"f(x,s,t):=x+f+c\"\n" +
                "2,3,,,\"f(x,s,t):=x+1\"\n" +
                "3,0,,,\"f(x,s,t):=x+1\"";
        DefaultDirectedGraph<MyVertex, MyEdge> g = CSV.deserialize(IntDataType.class, input);
        MyVertex[] vertexSet = g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        MyEdge[] edgeSet = g.edgeSet().toArray(new MyEdge[g.edgeSet().size()]);

        assertEquals(4, edgeSet.length);
        assertEquals(4, vertexSet.length);


        assertEquals(0L,(long)vertexSet[0].getId());
        assertEquals(1L,(long)vertexSet[1].getId());
        assertEquals(2L,(long)vertexSet[2].getId());
        assertEquals(3L,(long)vertexSet[3].getId());

        assertEquals(0,vertexSet[0].getAttributeValue("goal"));
        assertEquals(43,vertexSet[0].getAttributeValue("test"));
        assertEquals(Integer.MAX_VALUE,vertexSet[1].getAttributeValue("goal"));
        assertEquals(null,vertexSet[1].getAttributeValue("test"));
        assertEquals(Integer.MAX_VALUE,vertexSet[2].getAttributeValue("goal"));
        assertEquals(null,vertexSet[2].getAttributeValue("test"));
        assertEquals(Integer.MAX_VALUE,vertexSet[3].getAttributeValue("goal"));
        assertEquals(null,vertexSet[3].getAttributeValue("test"));

        assertEquals(3,   edgeSet[0].getAttributeValue("c"));
        assertEquals(null,edgeSet[0].getAttributeValue("f"));
        assertEquals(2,   edgeSet[1].getAttributeValue("c"));
        assertEquals(2,   edgeSet[1].getAttributeValue("f"));
        assertEquals(null,edgeSet[2].getAttributeValue("c"));
        assertEquals(null,edgeSet[2].getAttributeValue("f"));
        assertEquals(null,edgeSet[3].getAttributeValue("c"));
        assertEquals(null,edgeSet[3].getAttributeValue("f"));
        assertEquals("f(x,s,t):=x+c",  edgeSet[0].getEdgeCapability("f").getText().replace("<EOF>",""));
        assertEquals("f(x,s,t):=x+f+c",edgeSet[1].getEdgeCapability("f").getText().replace("<EOF>",""));
        assertEquals("f(x,s,t):=x+1",  edgeSet[2].getEdgeCapability("f").getText().replace("<EOF>",""));
        assertEquals("f(x,s,t):=x+1",  edgeSet[3].getEdgeCapability("f").getText().replace("<EOF>",""));

        assertEquals(null,edgeSet[0].getEdgeCapability("g"));
        assertEquals(null,edgeSet[1].getEdgeCapability("g"));
        assertEquals(null,edgeSet[2].getEdgeCapability("g"));
        assertEquals(null,edgeSet[3].getEdgeCapability("g"));
    }


    @Test
    public void testCSVToGraph2() throws Exception {
        String input = "#Vertexes (SourceID (, item)*)\n" +
                "0,,goal:0\n" +
                "1,,goal:2147483647\n" +
                "2,,goal:2147483647\n" +
                "3,,goal:2147483647\n" +
                "#Edges (SourceID, TargetID, (Capability|Item)*)\n" +
                "0,1,c:3,,\"f(x,s,t):=x+c\",h:6\n" +
                "1,2,c:2,f:2,\"f(x,s,t):=x+f+c\"\n" +
                "2,3,,,\"f(x,s,t):=x+1\"\n" +
                "3,0,,,\"f(x,s,t):=x+1\"";
        DefaultDirectedGraph<MyVertex, MyEdge> g = CSV.deserialize(IntDataType.class, input);
        MyVertex[] vertexSet = g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        MyEdge[] edgeSet = g.edgeSet().toArray(new MyEdge[g.edgeSet().size()]);

        assertEquals(4, edgeSet.length);
        assertEquals(4, vertexSet.length);
    }

    @Test
    public void testCSVToGraph3() throws Exception {
        String input = "#Vertexes (SourceID (, item)*)\n" +
                "0,,goal:0,test\n";
        try {
            DefaultDirectedGraph<MyVertex, MyEdge> g = CSV.deserialize(IntDataType.class, input);
            assertTrue(false);
        }
        catch (Exception e){
            assertTrue(true);
            assertEquals("Input \"test\" was not recognized as a key, value pair.", e.getMessage());
        }

    }

    @Test
    public void testCSVToGraph4() throws Exception {
        String input = "#Vertexes (SourceID (, item)*)\n" +
                "0,,goal:0\n";
        try {
            DefaultDirectedGraph<MyVertex, MyEdge> g = CSV.deserialize(IntDataType.class, input);
            assertTrue(true);
        }
        catch (Exception e){
            assertTrue(false);
        }
    }

    @Test
    public void testCSVToGraph5() throws Exception {
        String input = "#Vertexes (SourceID (, item)*)\n" +
                "#0,,goal:0\n"+
                "1,,goal:0\n"+
                "#0,,goal:0\n"+
                "3,,goal:321\n";
        DefaultDirectedGraph<MyVertex, MyEdge> g = null;
        try {
            g = CSV.deserialize(IntDataType.class, input);
            assertTrue(true);
        }
        catch (Exception e){
            assertTrue(false);
        }
        assertEquals(2, g.vertexSet().size());
        MyVertex[] vertexes = g.vertexSet().toArray(new MyVertex[]{});
        assertEquals(1L,(long)vertexes[0].getId());
        assertEquals(3L,(long)vertexes[1].getId());
        assertEquals(0, vertexes[0].getAttributeValue("goal"));
        assertEquals(321, vertexes[1].getAttributeValue("goal"));
    }

    @Test
    public void testCSVToGraph6() throws Exception {
        String input = "#Vertexes (SourceID (, item)*)\n" +
                "#0,,goal:0\n"+
                "1,,goal:0\n"+
                "#0,,goal:0\n"+
                "3,,goal:321\n"+
                "#Edges\n"+
                "1,3\n"+
                "#3,1\n"+
                "#2,3\n"+
                "#Hello world\n";
        DefaultDirectedGraph<MyVertex, MyEdge> g = null;
        try {
            g = CSV.deserialize(IntDataType.class, input);
            assertTrue(true);
        }
        catch (Exception e){
            assertTrue(false);
        }
        assertEquals(2, g.vertexSet().size());
        MyVertex[] vertexes = g.vertexSet().toArray(new MyVertex[]{});
        assertEquals(1L,(long)vertexes[0].getId());
        assertEquals(3L,(long)vertexes[1].getId());
        assertEquals(0, vertexes[0].getAttributeValue("goal"));
        assertEquals(321, vertexes[1].getAttributeValue("goal"));
        assertNotEquals(null, g.getEdge(vertexes[0],vertexes[1]));
        assertEquals(null, g.getEdge(vertexes[1],vertexes[0]));
        assertEquals(null, g.getEdge(vertexes[1],vertexes[1]));
        assertEquals(null, g.getEdge(vertexes[0],vertexes[0]));
    }

    @Test
    public void testCSVToGraph7() throws Exception {
        String input = "#Vertexes (SourceID (, item)*)\n" +
                "#0,,goal:0\n"+
                "1,,goal:0\n"+
                "#0,,goal:0\n"+
                "3,,goal:321\n"+
                "#Edges\n"+
                "\n"+
                "1,3\n";
        DefaultDirectedGraph<MyVertex, MyEdge> g = null;
        try {
            g = CSV.deserialize(IntDataType.class, input);
            assertTrue(true);
        }
        catch (Exception e){
            assertTrue(false);
        }
        assertEquals(2, g.vertexSet().size());
        MyVertex[] vertexes = g.vertexSet().toArray(new MyVertex[]{});
        assertEquals(1L,(long)vertexes[0].getId());
        assertEquals(3L,(long)vertexes[1].getId());
        assertEquals(0, vertexes[0].getAttributeValue("goal"));
        assertEquals(321, vertexes[1].getAttributeValue("goal"));
        assertNotEquals(null, g.getEdge(vertexes[0],vertexes[1]));
        assertEquals(null, g.getEdge(vertexes[1],vertexes[0]));
        assertEquals(null, g.getEdge(vertexes[1],vertexes[1]));
        assertEquals(null, g.getEdge(vertexes[0],vertexes[0]));
    }

    @Test
    public void testCSVToGraph8() throws Exception {
        String input = "#Vertexes (SourceID (, item)*)\n" +
                "#0,,goal:0\n"+
                "1,,goal:0\n"+
                "#0,,goal:0\n"+
                "\n"+
                "3,,goal:321\n"+
                "#Edges\n"+
                "\n"+
                "1,3\n";
        DefaultDirectedGraph<MyVertex, MyEdge> g = null;
        try {
            g = CSV.deserialize(IntDataType.class, input);
            assertTrue(true);
        }
        catch (Exception e){
            assertTrue(false);
        }
        assertEquals(2, g.vertexSet().size());
        MyVertex[] vertexes = g.vertexSet().toArray(new MyVertex[]{});
        assertEquals(1L,(long)vertexes[0].getId());
        assertEquals(3L,(long)vertexes[1].getId());
        assertEquals(0, vertexes[0].getAttributeValue("goal"));
        assertEquals(321, vertexes[1].getAttributeValue("goal"));
        assertNotEquals(null, g.getEdge(vertexes[0],vertexes[1]));
        assertEquals(null, g.getEdge(vertexes[1],vertexes[0]));
        assertEquals(null, g.getEdge(vertexes[1],vertexes[1]));
        assertEquals(null, g.getEdge(vertexes[0],vertexes[0]));
    }

    @Test
    public void csvTest(){
        String serialize = CSV.serialize(graph);
        try {
            assertEquals(serialize, CSV.serialize(CSV.deserialize(IntDataType.class,CSV.serialize(graph))));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true);
        }
    }

    @Test
    public void multiTypeTest() throws Exception {
        String input = "#Types (item,type)\n" +
                "goal,smucparser.datatypes.simple.IntDataType\n" +
                "bool,smucparser.datatypes.simple.BoolDataType\n" +
                "#Vertexes (SourceID (, item)*)\n" +
                "0,,goal:0         , bool:true\n" +
                "1,,goal:2147483647, bool:false\n" +
                "2,,goal:2147483647, bool:true\n" +
                "3,,goal:2147483647, bool:true\n" +
                "4,,goal:2147483647, bool:true\n" +
                "5,,goal:2147483647, bool:true\n" +
                "6,,goal:2147483647, bool:true\n" +
                "#Edges (SourceID,TargetID,(Capability|Item)*)\n" +
                "0,1,\"f(x,s,t):=x+1\"\n" +
                "1,2,\"f(x,s,t):=x+1\"\n" +
                "2,3,\"f(x,s,t):=x+1\"\n" +
                "3,0,\"f(x,s,t):=x+1\"\n" +
                "4,3,\"f(x,s,t):=x+1\"\n" +
                "5,4,\"f(x,s,t):=x+1\"\n" +
                "6,3,\"f(x,s,t):=x+1\"\n" +
                "4,1,\"f(x,s,t):=x+1\"";
        DefaultDirectedGraph<MyVertex, MyEdge> g = CSV.deserialize(IntDataType.class, input);

        SMuC.performQuery(IntDataType.class, g, "gfp distance. min(goal, min<f> distance)");
        SMuC.performQuery(IntDataType.class, g, "gfp lessThanThree. lt(distance, 3)");
        //DOT.printGraph(g, "csvMulti.pdf");

        MyVertex[] vertexSet = g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);

        assertEquals(0,vertexSet[0].getFormulaVariable("distance").getValue());
        assertEquals(3,vertexSet[1].getFormulaVariable("distance").getValue());
        assertEquals(2,vertexSet[2].getFormulaVariable("distance").getValue());
        assertEquals(1,vertexSet[3].getFormulaVariable("distance").getValue());
        assertEquals(2,vertexSet[4].getFormulaVariable("distance").getValue());
        assertEquals(3,vertexSet[5].getFormulaVariable("distance").getValue());
        assertEquals(2,vertexSet[6].getFormulaVariable("distance").getValue());

        assertEquals(true, vertexSet[0].getFormulaVariable("lessThanThree").getValue());
        assertEquals(false,vertexSet[1].getFormulaVariable("lessThanThree").getValue());
        assertEquals(true, vertexSet[2].getFormulaVariable("lessThanThree").getValue());
        assertEquals(true, vertexSet[3].getFormulaVariable("lessThanThree").getValue());
        assertEquals(true, vertexSet[4].getFormulaVariable("lessThanThree").getValue());
        assertEquals(false,vertexSet[5].getFormulaVariable("lessThanThree").getValue());
        assertEquals(true, vertexSet[6].getFormulaVariable("lessThanThree").getValue());

    }

    @Test
    public void multiTypeTestFail() throws Exception {
        String input = "#Types (item,type)\n" +
                "goal,smucparser.datatypes.simple.IntDataType\n" +
                "bool,smucparser.datatypes.simple.BoolDataType\n" +
                "#Vertexes (SourceID (, item)*)\n" +
                "0,,goal:0         , bool:true\n" +
                "1,,goal:true, bool:false\n" +
                "#Edges (SourceID,TargetID,(Capability|Item)*)\n";
        try {
            CSV.deserialize(IntDataType.class, input);
            assertFalse(true);
        }
        catch (Exception e){
            assertTrue(true);
        }
    }

    /**
     * Should fail if the datatype doesn't exist.
     */
    @Test
    public void multiTypeTestFail2() throws Exception {
        String input = "#Types (item,type)\n" +
                "goal,smucparser.datatypes.simple.IntDataType\n" +
                "bool,smucparser.datatypes.FailPlease\n" +
                "#Vertexes (SourceID (, item)*)\n" +
                "#Edges (SourceID,TargetID,(Capability|Item)*)\n";
        try {
            CSV.deserialize(IntDataType.class, input);
            assertFalse(true);
        }
        catch (ClassNotFoundException e){
            assertTrue(true);
        }
    }
    /**
     * Should fail if the datatype is not a subclass of Abstractdatatype
     */
    @Test
    public void multiTypeTestFail2_1() throws Exception {
        String input = "#Types (item,type)\n" +
                "goal,java.lang.Integer\n" +
                "bool,smucparser.datatypes.simple.BoolDataType\n" +
                "#Vertexes (SourceID (, item)*)\n" +
                "#Edges (SourceID,TargetID,(Capability|Item)*)\n";
        try {
            CSV.deserialize(IntDataType.class, input);
            assertFalse(true);
        }
        catch (Exception e){
            assertTrue(true);
        }
    }

    /**
     * If there are no types specified for the items, then assume they are of the type defined
     * when calling deserialize. Since bool is not an integer, an exception is thrown when trying to
     * interpret it.
     */
    @Test
    public void multiTypeTestFail3() throws Exception {
        String input = "#Types (item,type)\n" +
                "#Vertexes (SourceID (, item)*)\n" +
                "0,,goal:0         , bool:true\n" +
                "1,,goal:2147483647, bool:false\n" +
                "2,,goal:2147483647, bool:true\n" +
                "3,,goal:2147483647, bool:true\n" +
                "4,,goal:2147483647, bool:true\n" +
                "5,,goal:2147483647, bool:true\n" +
                "6,,goal:2147483647, bool:true\n" +
                "#Edges (SourceID,TargetID,(Capability|Item)*)\n" +
                "0,1,f(x,s,t):=x+1\n" +
                "1,2,f(x,s,t):=x+1\n" +
                "2,3,f(x,s,t):=x+1\n" +
                "3,0,f(x,s,t):=x+1\n" +
                "4,3,f(x,s,t):=x+1\n" +
                "5,4,f(x,s,t):=x+1\n" +
                "6,3,f(x,s,t):=x+1\n" +
                "4,1,f(x,s,t):=x+1";
        try {
            CSV.deserialize(IntDataType.class, input);
            assertTrue(false);
        }
        catch (NumberFormatException e){
            assertTrue(true);
        }
    }

    @Test
    public void csvFormulaVarTest() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> graph = new DefaultDirectedGraph<>(MyEdge.class);

        MyVertexFactory factory = new MyVertexFactory();
        MyVertex V1 = factory.createVertex();
        MyVertex V2 = factory.createVertex();

        V1.setId(10L);
        V2.setId(11L);

        graph.addVertex(V1);
        graph.addVertex(V2);

        graph.addEdge(V1,V2);
        graph.addEdge(V2,V2);

        SMuC.performQuery(NodeSetDataType.class, graph, "lfp x. 1 + + ><1");
        SMuC.performQuery(NodeSetDataType.class, graph, "lfp y. 1 + + <>1");
        String serialize = CSV.serialize(graph);
        DefaultDirectedGraph<MyVertex, MyEdge> deserialize = CSV.deserialize(NodeSetDataType.class, CSV.serialize(graph));
        assertEquals(serialize, CSV.serialize(deserialize));


        MyVertex[] vertices =  deserialize.vertexSet().toArray(new MyVertex[deserialize.vertexSet().size()]);
        assertEquals("[10]",vertices[0].getFormulaVariable("x").getValue().toString());
        assertEquals("[10, 11]",vertices[1].getFormulaVariable("x").getValue().toString());
        assertEquals("[10, 11]",vertices[0].getFormulaVariable("y").getValue().toString());
        assertEquals("[11]",vertices[1].getFormulaVariable("y").getValue().toString());
        //DOT.printGraph(graph, "asdf.pdf");
    }
}
