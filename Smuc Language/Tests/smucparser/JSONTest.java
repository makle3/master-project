package smucparser;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.Before;
import org.junit.Test;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.complex.NodeSetDataType;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.datatypes.simple.IntDataType;
import smucparser.graph.Import.JSON;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * Created by martin on 06-03-2016.
 */
public class JSONTest {
    private DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);

    @Before
    public void setUp() throws Exception {
        MyVertexFactory factory = new MyVertexFactory(IntDataType.class);

        //String v1 = "1 goal = 0;
        HashMap<String, AbstractDataType> v1 = new HashMap<>();
        v1.put("goal", AbstractDataType.newInstance(IntDataType.class, 0));
        v1.put("a", AbstractDataType.newInstance(IntDataType.class, 0));
        MyVertex V1 = factory.createVertex(v1);


        HashMap<String, AbstractDataType> v2 = new HashMap<>();
        v2.put("goal", AbstractDataType.newInstance(IntDataType.class, Integer.MAX_VALUE));


        MyVertex V2 = factory.createVertex(v2);
        MyVertex V3 = factory.createVertex(v2);
        MyVertex V4 = factory.createVertex(v2);
        MyVertex V5 = factory.createVertex(v2);
        MyVertex V6 = factory.createVertex(v2);
        MyVertex V7 = factory.createVertex(v2);
        MyVertex V8 = factory.createVertex(v2);
        MyVertex V9 = factory.createVertex(v2);
        MyVertex V10 = factory.createVertex(v1);
//
//
        // add the vertices
        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);
        g.addVertex(V5);
        g.addVertex(V6);
        g.addVertex(V7);
        g.addVertex(V8);
        g.addVertex(V9);
        g.addVertex(V10);
//

        String capability = "f(x,s,t):= x+1";
        String capability2 = "g(x,s,t):= x+1";
        MyEdge e = g.addEdge(V1, V2);
        e.addEdgeCapability(capability);
        e.addEdgeCapability(capability2);
        e = g.addEdge(V2, V3);
        e.addEdgeCapability(capability);
        e = g.addEdge(V3, V4);
        e.addEdgeCapability(capability);
        e = g.addEdge(V4, V1);
        e.addEdgeCapability(capability);
        e = g.addEdge(V8, V7);
        e.addEdgeCapability(capability);
        e = g.addEdge(V7, V4);
        e.addEdgeCapability(capability);
        e = g.addEdge(V6, V3);
        e.addEdgeCapability(capability);
        e = g.addEdge(V5, V6);
        e.addAttribute("c", AbstractDataType.newInstance(IntDataType.class, 10));
        e.addEdgeCapability("f(x,s,t):=x+c");
        e = g.addEdge(V8, V9);
        e.addEdgeCapability(capability);
        e = g.addEdge(V9, V10);
        e.addEdgeCapability(capability);
        e = g.addEdge(V10, V9);
        e.addEdgeCapability(capability);
        e = g.addEdge(V6, V8);
        e.addEdgeCapability(capability);

    }

    @Test
    public void jsonParseTest(){
        String serialize = JSON.serialize(g);
        Main.writeFile(serialize, "json.json");
        assertEquals(serialize, JSON.serialize(JSON.deserialize(IntDataType.class,JSON.serialize(g))));
    }


    @Test
    public void jsonFormulaVarTest() throws DataTypeException {
        DefaultDirectedGraph<MyVertex, MyEdge> graph = new DefaultDirectedGraph<>(MyEdge.class);

        MyVertexFactory factory = new MyVertexFactory();
        MyVertex V1 = factory.createVertex();
        MyVertex V2 = factory.createVertex();
        V1.setId(10L);
        V2.setId(11L);

        graph.addVertex(V1);
        graph.addVertex(V2);

        graph.addEdge(V1,V2);
        graph.addEdge(V2,V2);

        SMuC.performQuery(NodeSetDataType.class, graph, "lfp x. 1 + + ><1");
        SMuC.performQuery(NodeSetDataType.class, graph, "lfp y. 1 + + <>1");
        String serialize = JSON.serialize(graph);
        DefaultDirectedGraph<MyVertex, MyEdge> deserialize = JSON.deserialize(NodeSetDataType.class, JSON.serialize(graph));
        assertEquals(serialize, JSON.serialize(deserialize));


        MyVertex[] vertices =  deserialize.vertexSet().toArray(new MyVertex[deserialize.vertexSet().size()]);
        assertEquals("[10]",vertices[0].getFormulaVariable("x").getValue().toString());
        assertEquals("[10, 11]",vertices[1].getFormulaVariable("x").getValue().toString());
        assertEquals("[10, 11]",vertices[0].getFormulaVariable("y").getValue().toString());
        assertEquals("[11]",vertices[1].getFormulaVariable("y").getValue().toString());
    }
}
