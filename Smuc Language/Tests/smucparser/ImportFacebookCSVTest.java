package smucparser;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import org.antlr.v4.runtime.misc.Pair;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.complex.MST.MSTDataType;
import smucparser.datatypes.complex.NeighborPathDataType;
import smucparser.datatypes.complex.Path;
import smucparser.datatypes.complex.PathSetDataType;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.datatypes.simple.BoolDataType;
import smucparser.datatypes.simple.DoubleDataType;
import smucparser.datatypes.simple.IntDataType;
import smucparser.graph.Import.CSV;
import smucparser.graph.Import.DOT;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static smucparser.graph.Import.SocialCircles.getFacebookGraph;

/**
 * Created by Martin on 17-05-2016.
 */
public class ImportFacebookCSVTest {

    @Before
    public void setUp() throws Exception {


    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void asd() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> graph = CSV.deserialize(MSTDataType.class, Main.readFile("./example-graphs/facebook_ego.csv", Charset.defaultCharset()));
        HashMap<Long, MyVertex> vertexes = new HashMap<>();
        Multimap<Long, Long> edgeMap = TreeMultimap.create();
        for (MyVertex v : graph.vertexSet()) {
            //Populate a hashmap with the vertexes, so it's easier to find them given an ID
            vertexes.put(v.getId(), v);
            HashSet adjList = (HashSet) v.getFormulaVariable("adjList").getValue();

            for (Object adj : adjList) {
                Long value = (Long)adj;
                edgeMap.put(v.getId(), value);
            }
        }

        //Remove all the edges
        HashSet<MyEdge> edges = new HashSet<>(graph.edgeSet());
        System.out.println(edges.size());
        for (MyEdge e : edges) {
            graph.removeEdge(e);
        }

        //Add the edges from adjList
        for (Map.Entry<Long,Long> e : edgeMap.entries()) {
            graph.addEdge(vertexes.get(e.getKey()), vertexes.get(e.getValue()));
        }

        System.out.println(graph.edgeSet().size());
        String s = "gfp short. min(isNodeOne(1), min<> addNode(short,1))";
        SMuC.performQuery(PathSetDataType.class, graph, s);

        edges = new HashSet<>(graph.edgeSet());
        for (MyEdge e : edges) {
            graph.removeEdge(e);
        }

        //Add the edges from the first path in the set found in the query
        for (MyVertex v : graph.vertexSet()) {
            HashSet<Path> z = (HashSet<Path>) v.getFormulaVariable("short").getValue();
            if(z.size() == 0){
                continue;
            }
            Path firstPath = z.iterator().next();
            Long last = firstPath.getLast();

            graph.addEdge(vertexes.get(last), v);
        }

        s = "lfp gender1. asInt(gender_77) + +<>gender1";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender2. asInt(gender_78) + +<>gender2";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender1_shared. max(gender1_shared, max[]max(gender1,gender1_shared))";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender2_shared. max(gender2_shared, max[]max(gender2,gender2_shared))";
        SMuC.performQuery(IntDataType.class, graph, s);

        DOT.printGraph(graph, "MST1_TREE.pdf",false,false, true);
    }

    @Test
    public void shortestPathTree() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> graph = getFacebookGraph();
        HashMap<Long, MyVertex> vertexes = new HashMap<>();
        for (MyVertex v : graph.vertexSet()) {
            vertexes.put(v.getId(), v);
        }

        String s = "gfp short. min(isNodeOne(1), min[] addNode(short,1))";
        SMuC.performQuery(PathSetDataType.class, graph, s);

        //Remove all the old edges from the graph
        HashSet<MyEdge> edges = new HashSet<>(graph.edgeSet());
        for (MyEdge e : edges) {
            graph.removeEdge(e);
        }

        //Add the edges from the first path in the set found in the query
        for (MyVertex v : graph.vertexSet()) {
            HashSet<Path> z = (HashSet<Path>) v.getFormulaVariable("short").getValue();
            if(z.size() == 0){
                continue;
            }
            Path firstPath = z.iterator().next();
            Long last = firstPath.getLast();

            graph.addEdge(vertexes.get(last), v);
        }

        s = "lfp gender1. asInt(gender_77) + +<>gender1";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender2. asInt(gender_78) + +<>gender2";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender1_shared. max(gender1, max[]gender1_shared)";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender2_shared. max(gender2, max[]gender2_shared)";
        SMuC.performQuery(IntDataType.class, graph, s);

        for (MyVertex v : graph.vertexSet()) {
            vertexes.put(v.getId(), v);
            v.getDoNotPrint().add("short");
        }
        DOT.printGraph(graph, "SPT3_TREE.pdf",false,true, true);
    }

    @Test
    public void shortestPathTreeFilterTest() throws Exception {
        MyVertexFactory factory = new MyVertexFactory();

        MyVertex V1 = factory.createVertex();
        V1.setId(1L);
        MyVertex V2 = factory.createVertex();
        MyVertex V3 = factory.createVertex();
        MyVertex V4 = factory.createVertex();
        MyVertex V5 = factory.createVertex();

        // add the vertices
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);
        g.addVertex(V5);

        g.addEdge(V1, V2);
        MyEdge e = g.addEdge(V2, V3);
        e.addEdgeCapability("a(x)(s,t):=filter(x,unionNodes(s,t))");
        g.addEdge(V3, V4);
        g.addEdge(V5, V2);

        String s = "gfp short. min(isNodeOne(0), min[a] addNode(short,1))";
        SMuC.performQuery(PathSetDataType.class, g, s);
    }

    @Test
    public void MSTFilterTest() throws Exception {
        MyVertexFactory factory = new MyVertexFactory();

        MyVertex V1 = factory.createVertex();
        V1.setId(1L);
        MyVertex V2 = factory.createVertex();
        MyVertex V3 = factory.createVertex();
        MyVertex V4 = factory.createVertex();
        MyVertex V5 = factory.createVertex();

        // add the vertices
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);
        g.addVertex(V5);

        g.addEdge(V1, V2);
        MyEdge e = g.addEdge(V2, V3);
        e.addEdgeCapability("a(x)(s,t):=filter(x,unionNodes(s,t))");
        g.addEdge(V3, V4);
        g.addEdge(V5, V2);

        String s = "lfp z. connect(" +
                "lfp y. shareSmallestPotentialInGroup(" +
                "lfp x. findSmallest(z+1, asSet[a]z+1)," +
                "asSet[]x)," +
                "asSet[]y)";
        SMuC.performQuery(MSTDataType.class, g, s);
        DOT.printGraph(g, "MST-Filter.pdf");
    }

    @Test
    public void MST123() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> graph = getFacebookGraph();

        SMuC.performQuery(MSTDataType.class, graph,"lfp z. connect(" +
                                                        "lfp y. shareSmallestPotentialInGroup(" +
                                                            "lfp x. findSmallest(z+1, asSet[]z+1)," +
                                                            "asSet[]x)," +
                                                        "asSet[]y)");


        SMuC.performQuery(MSTDataType.class,graph,
                "lfp adjList. asNodeSet(z)");

        HashMap<Long, MyVertex> vertexes = new HashMap<>();
        Multimap<Long, Long> edgeMap = TreeMultimap.create();
        for (MyVertex v : graph.vertexSet()) {
            //Populate a hashmap with the vertexes, so it's easier to find them given an ID
            vertexes.put(v.getId(), v);
            HashSet adjList = (HashSet) v.getFormulaVariable("adjList").getValue();

            for (Object adj : adjList) {
                Long value = (Long)adj;
                edgeMap.put(v.getId(), value);
            }
        }

        //Remove all the edges
        HashSet<MyEdge> edges = new HashSet<>(graph.edgeSet());
        for (MyEdge e : edges) {
            graph.removeEdge(e);
        }

        //Add the edges from adjList
        for (Map.Entry<Long,Long> e : edgeMap.entries()) {
            graph.addEdge(vertexes.get(e.getKey()), vertexes.get(e.getValue()));
        }

        String s = "gfp short. min(isNodeOne(1), min[] addNode(short,1))";
        SMuC.performQuery(PathSetDataType.class, graph, s);

        //Remove all the old edges from the graph
        edges = new HashSet<>(graph.edgeSet());
        for (MyEdge e : edges) {
            graph.removeEdge(e);
        }

        //Add the edges from the first path in the set found in the query
        for (MyVertex v : graph.vertexSet()) {
            HashSet<Path> z = (HashSet<Path>) v.getFormulaVariable("short").getValue();
            if(z.size() == 0){
                continue;
            }
            Path firstPath = z.iterator().next();
            Long last = firstPath.getLast();

            graph.addEdge(vertexes.get(last), v);
        }


        s = "lfp gender1. asInt(gender_77) + +<>gender1";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender2. asInt(gender_78) + +<>gender2";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender1_shared. max(gender1, max[]gender1_shared)";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender2_shared. max(gender2, max[]gender2_shared)";
        SMuC.performQuery(IntDataType.class, graph, s);

        DOT.printGraph(graph, "MST_TREE_V2.pdf",false,true, true);

    }

    @Test
    public void SPT_Secure() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> graph = getFacebookGraph();

        for (MyEdge e : graph.edgeSet()) {
            e.addAttribute("Weight", AbstractDataType.newInstance(DoubleDataType.class, 1.0));
            e.addEdgeCapability("a(x)(n,m):=+(x,getWeight(Weight))");
        }

        System.out.println("starting query");
        SMuC.performQuery(NeighborPathDataType.class, graph,"gfp z. min(isNodeOne(1), min[a] minValue(\"this\", z))");
        System.out.println("ending query");


        HashMap<Long, MyVertex> vertexes = new HashMap<>();
        HashMap<Long, Long> edgeMap = new HashMap<>();
        for (MyVertex v : graph.vertexSet()) {
            //Populate a hashmap with the vertexes, so it's easier to find them given an ID
            vertexes.put(v.getId(), v);
            Pair<Long, Double> neigh = (Pair<Long, Double>) v.getFormulaVariable("z").getValue();

            if(neigh.a != Long.MAX_VALUE) {
                if(!Objects.equals(v.getId(), neigh.a)){
                    edgeMap.put(v.getId(), neigh.a);
                }
            }
        }

        //Remove all the edges
        HashSet<MyEdge> edges = new HashSet<>(graph.edgeSet());
        for (MyEdge e : edges) {
            graph.removeEdge(e);
        }

        //Add the edges from adjList
        for (Map.Entry<Long,Long> e : edgeMap.entrySet()) {
            graph.addEdge(vertexes.get(e.getValue()), vertexes.get(e.getKey()));
        }

        String s = "lfp gender1. asInt(gender_77) + +<>gender1";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender2. asInt(gender_78) + +<>gender2";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender1_shared. max(gender1, max[]gender1_shared)";
        SMuC.performQuery(IntDataType.class, graph, s);
        s = "lfp gender2_shared. max(gender2, max[]gender2_shared)";
        SMuC.performQuery(IntDataType.class, graph, s);

        DOT.printGraph(graph, "S-SPT.pdf",false,true, true);

    }

    @Test
    public void count() throws DataTypeException {
        HashMap<String, AbstractDataType> v1 = new HashMap<String, AbstractDataType>()
        {{
            put("v", AbstractDataType.newInstance(BoolDataType.class, false));
        }};
        HashMap<String, AbstractDataType> v2 = new HashMap<String, AbstractDataType>()
        {{
            put("v", AbstractDataType.newInstance(BoolDataType.class, true));
        }};

        MyVertexFactory factory = new MyVertexFactory();

        MyVertex V1 = factory.createVertex(v1);
        MyVertex V2 = factory.createVertex(v2);
        MyVertex V3 = factory.createVertex(v1);
        MyVertex V4 = factory.createVertex(v2);
        MyVertex V5 = factory.createVertex(v2);
        MyVertex V6 = factory.createVertex(v1);
        MyVertex V7 = factory.createVertex(v2);
        MyVertex V8 = factory.createVertex(v2);

        // add the vertices
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);
        g.addVertex(V4);
        g.addVertex(V5);
        g.addVertex(V6);
        g.addVertex(V7);
        g.addVertex(V8);

        g.addEdge(V1, V2);
        g.addEdge(V2, V3);
        g.addEdge(V3, V4);
        g.addEdge(V3, V5);
        g.addEdge(V3, V6);
        g.addEdge(V6, V7);
        g.addEdge(V6, V8);


        String s = "lfp gender1. asInt(v) + +<>gender1";
        SMuC.performQuery(IntDataType.class, g, s);
        s = "lfp shared. max(shared, max[]max(gender1,shared))";
        SMuC.performQuery(IntDataType.class, g, s);


        MyVertex[] vertices =  g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        for (MyVertex vertex : vertices) {
            assertEquals(5, (int) vertex.getFormulaVariable("shared").getValue());
        }
        assertEquals(5, (int) vertices[0].getFormulaVariable("gender1").getValue());
        assertEquals(5, (int) vertices[1].getFormulaVariable("gender1").getValue());
        assertEquals(4, (int) vertices[2].getFormulaVariable("gender1").getValue());
        assertEquals(1, (int) vertices[3].getFormulaVariable("gender1").getValue());
        assertEquals(1, (int) vertices[4].getFormulaVariable("gender1").getValue());
        assertEquals(2, (int) vertices[5].getFormulaVariable("gender1").getValue());
        assertEquals(1, (int) vertices[6].getFormulaVariable("gender1").getValue());
        assertEquals(1, (int) vertices[7].getFormulaVariable("gender1").getValue());
    }
}
