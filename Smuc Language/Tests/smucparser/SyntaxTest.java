package smucparser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.grammar.generated.GrammarLexer;
import smucparser.grammar.generated.GrammarParser;
import smucparser.grammar.semantics.SmucListener;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by Martin on 17-03-2016.
 */
public class SyntaxTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void asd() throws Exception {

        String s = "1+<>a";


        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(s));
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));

        GrammarParser.Start_ruleContext tree = parser.start_rule();
        ParseTreeWalker walker = new ParseTreeWalker();
        SmucListener listener = new SmucListener(g);
        try {
            walker.walk(listener, tree);
            assertTrue(false);
        }
        catch (Exception e){
            assertTrue(true);
        }
    }
}
