package smucparser.datatypes.complex;

import org.apache.commons.cli.*;
import org.junit.Before;
import org.junit.Test;
import smucparser.datatypes.AbstractDataType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.*;

/**
 * Created by Martin on 19-04-2016.
 */
public class SetDataTypeTest {
    private SetDataType<Integer> dataType;
    HashSet<Integer> set1 = new HashSet<>();
    HashSet<Integer> set2 = new HashSet<>();
    HashSet<Integer> set3 = new HashSet<>();
    HashSet<Integer> set1_2 = new HashSet<>();
    HashSet<Integer> set1_2_3 = new HashSet<>();


    @Before
    public void setUp() throws Exception {
        dataType = new SetDataType<>(HashSet.class);
        set1.add(1);
        set2.add(2);
        set3.add(3);
        set1_2.add(1);
        set1_2.add(2);
        set1_2_3.add(1);
        set1_2_3.add(2);
        set1_2_3.add(3);
    }

    @Test
    public void getTop() throws Exception {
        assertTrue(dataType.getTop() instanceof TopSet);
        assertFalse(dataType.getTop() == null);
        assertEquals(0, dataType.getTop().size());
    }

    @Test
    public void getBottom() throws Exception {
        assertTrue(dataType.getBottom() instanceof HashSet);
        assertEquals(0, dataType.getBottom().size());
        assertFalse(dataType.getBottom() == null);
    }

    @Test
    public void union() throws Exception {
        // Union between 1 and 2 should contain both
        Set union1_2 = dataType.union(set1, set2);
        assertEquals(2, union1_2.size());
        assertTrue(union1_2.contains(1));
        assertTrue(union1_2.contains(2));

        Set union2_1 = dataType.union(set2, set1);
        assertEquals(2, union2_1.size());
        assertTrue(union2_1.contains(1));
        assertTrue(union2_1.contains(2));

        //The union of two sets should be be the same independent of order
        assertEquals(union1_2, union2_1);

        //Union with itself gives itself
        assertEquals(set1, dataType.union(set1, set1));

        //3 items for union
        Set union1_2_3 = dataType.union(set1, set2, set3);
        assertEquals(3, union1_2_3.size());
        assertTrue(union1_2_3.contains(1));
        assertTrue(union1_2_3.contains(2));
        assertTrue(union1_2_3.contains(3));

        assertEquals(set1_2_3, dataType.union(set1_2, set3));

        //Get bottom if union with null
        assertEquals(dataType.getBottom(), dataType.union(null));

        //null acts as bottom
        assertEquals(set1, dataType.union(null, set1));
    }

    @Test
    public void intersection() throws Exception {
        // Union between 1 and 2 should contain both
        Set intersection1_2 = dataType.intersection(set1, set2);
        assertEquals(0, intersection1_2.size());

        Set intersection2_1 = dataType.intersection(set2, set1);
        assertEquals(0, intersection2_1.size());

        //Intersection with itself gives itself
        assertEquals(set1, dataType.intersection(set1, set1));

        //3 items for intersection
        Set intersection1_2_3 = dataType.intersection(set1, set2, set3);
        assertEquals(0, intersection1_2_3.size());


        Set intersection123 = dataType.intersection(set1_2, set1_2_3);
        assertEquals(2, intersection123.size());
        assertEquals(set1_2, intersection123);

        //Get bottom if intersection with null
        assertEquals(dataType.getBottom(), dataType.intersection(null));

        //null acts as bottom
        assertEquals(dataType.getBottom(), dataType.intersection(null, set1));

    }

    @Test
    public void complements() throws Exception {
    }

    @Test
    public void size() throws Exception {
        assertEquals((Integer) 1, dataType.size(set1));
        assertEquals((Integer) 2, dataType.size(set1_2));
        assertEquals((Integer) 3, dataType.size(set1_2_3));
        assertEquals((Integer) 0, dataType.size());
        assertEquals((Integer) 0, dataType.size(null));

        try{
            dataType.size(set1_2_3, set1);
            assertTrue(false);
        }
        catch (Exception e){
            assertTrue(true);
        }
    }

    @Test
    public void difference() throws Exception{
        assertEquals(0.666, dataType.difference(set1,set1_2_3), 0.001);
        assertEquals(0.0, dataType.difference(set1,set1), 0.0);
        assertEquals(0.5, dataType.difference(set1,set1_2), 0.001);
        assertEquals(1.0, dataType.difference(null,set1), 0.001);
        assertEquals(1.0, dataType.difference(set1, null), 0.001);
        assertEquals(0.0, dataType.difference(null, null), 0.001);
        assertEquals(0.0, dataType.difference(new HashSet<Integer>(), new HashSet<Integer>()), 0.001);
        assertEquals(1.0, dataType.difference(new HashSet<Integer>(),set1), 0.001);
        assertEquals(1.0, dataType.difference(set1, new HashSet<Integer>()), 0.001);
    }

    @Test
    public void equals(){
        SetDataType<Integer> dt1 = new SetDataType<>(HashSet.class);
        SetDataType<Integer> dt2 = new SetDataType<>(HashSet.class);
        dt1.setValue(set1);
        dt2.setValue(set1);
        assertTrue( dt1.equals(dt2));
        dt1.setValue(set1_2);
        dt2.setValue(set1_2);
        assertFalse(dt1.getValue().equals(set1));
        assertTrue( dt1.equals(dt2));
        dt1.setValue(set1_2_3);
        dt2.setValue(set1_2_3);
        assertTrue( dt1.equals(dt2));
        dt1.setValue(set1);
        dt2.setValue(set1_2);
        assertFalse(dt1.equals(dt2));
        assertFalse(dt2.equals(dt1));
        dt1.setValue(set1);
        dt2.setValue(null);
        assertTrue(dt2.getValue() == null);
        assertFalse(dt1.equals(dt2));
        assertFalse(dt2.equals(dt1));
        dt1.setValue(null);
        dt2.setValue(null);
        assertTrue( dt1.equals(dt2));
    }

}