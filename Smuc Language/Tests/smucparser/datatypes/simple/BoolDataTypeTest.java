package smucparser.datatypes.simple;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Martin on 01-03-2016.
 */
public class BoolDataTypeTest {
    private BoolDataType dataType;
//FixMe Add more values for the datatype
    @Before
    public void setUp() throws Exception {
        dataType =  new BoolDataType();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void toInt(){
        assertEquals(1, (int)dataType.asInt(true));
        assertEquals(0, (int)dataType.asInt(false));

        assertEquals(1, (int)dataType.function("asInt", null, true));
        assertEquals(0, (int)dataType.function("asInt", null, false));


    }
}
