package smucparser.datatypes.simple;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.datatypes.DataTypeDifference;
import smucparser.datatypes.helper.SmucFunction;

import static org.junit.Assert.*;

/**
 * Created by Martin on 01-03-2016.
 */
public class IntDataTypeTest {
    private IntDataType dataType;
//FixMe Add more values for the datatype
    @Before
    public void setUp() throws Exception {
        dataType =  new IntDataType();
    }

    @After
    public void tearDown() throws Exception {
    }
    @Test
    public void testGetTop() throws Exception {
        assertEquals(Integer.MAX_VALUE, (int)dataType.getTop());
    }

    @Test
    public void testGetBottom() throws Exception {
        assertEquals(0, (int)dataType.getBottom());
    }
    @Test
    public void testPlus() throws Exception {
        assertEquals(10, (int)dataType.plus(4, 6));
        assertEquals(4, (int)dataType.plus(0, 4));
        assertEquals(4, (int)dataType.plus(4, 0));
        assertEquals(Integer.MAX_VALUE, (int)dataType.plus(dataType.getTop(), 0));
        assertEquals(Integer.MAX_VALUE, (int)dataType.plus(dataType.getTop(), 1));
        assertEquals(Integer.MAX_VALUE, (int)dataType.plus(dataType.getTop(), dataType.getTop()));
        assertEquals(5, (int)dataType.plus((5)));
        assertEquals(7, (int)dataType.plus((7)));
        assertEquals(0, (int)dataType.plus());
    }

    @Test
    public void testMult() throws Exception {
        assertEquals(24, (int)dataType.times(4, 6));
        assertEquals(0, (int)dataType.times(0, 4));
        assertEquals(0, (int)dataType.times(4, 0));
        assertEquals(0, (int)dataType.times(dataType.getTop(), 0));
        assertEquals(Integer.MAX_VALUE, (int)dataType.times(dataType.getTop(), 1));
        assertEquals(Integer.MAX_VALUE, (int)dataType.times(dataType.getTop(), dataType.getTop()));
        assertEquals(5, (int)dataType.times((5)));
        assertEquals(7, (int)dataType.times((7)));
        assertEquals(null, dataType.times());
    }

    @Test
    public void testMin() throws Exception {
        assertEquals(4, (int)dataType.min(4, 6));
         assertEquals(0, (int)dataType.min(0, 4));
        assertEquals(0, (int)dataType.min(4, 0));
        assertEquals(0, (int)dataType.min(dataType.getTop(), 0));
        assertEquals(1, (int)dataType.min(dataType.getTop(), 1));
        assertEquals(Integer.MAX_VALUE, (int)dataType.min(dataType.getTop(), dataType.getTop()));
        assertEquals(5, (int)dataType.min((5)));
        assertEquals(7, (int)dataType.min((7)));
        assertEquals(Integer.MAX_VALUE, (int)dataType.min());
    }
    @Test
    public void testMax() throws Exception {
        assertEquals(6, (int)dataType.max(4, 6));
        assertEquals(4, (int)dataType.max(0, 4));
        assertEquals(4, (int)dataType.max(4, 0));
        assertEquals(Integer.MAX_VALUE, (int)dataType.max(dataType.getTop(), 0));
        assertEquals(Integer.MAX_VALUE, (int)dataType.max(dataType.getTop(), 1));
        assertEquals(Integer.MAX_VALUE, (int)dataType.max(dataType.getTop(), dataType.getTop()));
        assertEquals(5, (int)dataType.max((5)));
        assertEquals(7, (int)dataType.max((7)));
        assertEquals(0, (int)dataType.max());
    }

    @Test
    public void testPlus2() throws Exception {
        assertEquals(this.dataType.getTop(), this.dataType.plus(this.dataType.getTop(),1));
        assertEquals(this.dataType.getTop(), this.dataType.plus(this.dataType.getTop(),0));
        assertEquals(this.dataType.getTop(), this.dataType.plus(this.dataType.getTop(),this.dataType.getTop()));
//
        assertEquals(this.dataType.plus(1,2),this.dataType.plus(2,1));
    }
//
    @Test
    public void testTimes() throws Exception {
        assertEquals(this.dataType.getTop(), this.dataType.times(this.dataType.getTop(),1));
        assertEquals(0, (int)this.dataType.times(this.dataType.getTop(),0));
        assertEquals(this.dataType.getTop(), this.dataType.times(this.dataType.getTop(),2));
        assertEquals(this.dataType.times(1,2),this.dataType.times(2,1));
    }

    @Test
    public void testCompare() throws Exception{
        assertTrue(this.dataType.eq(0,0));
        assertTrue(this.dataType.eq(Integer.MAX_VALUE,Integer.MAX_VALUE));
        assertFalse(this.dataType.eq(Integer.MAX_VALUE,Integer.MIN_VALUE));
        assertFalse(this.dataType.eq(Integer.MIN_VALUE,Integer.MAX_VALUE));
        assertFalse(this.dataType.eq(0,Integer.MAX_VALUE));
        assertFalse(this.dataType.eq(Integer.MAX_VALUE,0));
        assertFalse(this.dataType.eq(0,Integer.MIN_VALUE));
        assertFalse(this.dataType.eq(Integer.MIN_VALUE,0));

        assertFalse(this.dataType.neq(0,0));
        assertFalse(this.dataType.neq(Integer.MAX_VALUE,Integer.MAX_VALUE));
        assertTrue(this.dataType.neq(Integer.MAX_VALUE,Integer.MIN_VALUE));
        assertTrue(this.dataType.neq(Integer.MIN_VALUE,Integer.MAX_VALUE));
        assertTrue(this.dataType.neq(0,Integer.MAX_VALUE));
        assertTrue(this.dataType.neq(Integer.MAX_VALUE,0));
        assertTrue(this.dataType.neq(0,Integer.MIN_VALUE));
        assertTrue(this.dataType.neq(Integer.MIN_VALUE,0));

        assertFalse(this.dataType.lt(0,0));
        assertFalse(this.dataType.lt(Integer.MAX_VALUE,Integer.MAX_VALUE));
        assertFalse(this.dataType.lt(Integer.MAX_VALUE,Integer.MIN_VALUE));
        assertTrue( this.dataType.lt(Integer.MIN_VALUE,Integer.MAX_VALUE));
        assertTrue( this.dataType.lt(0,Integer.MAX_VALUE));
        assertFalse(this.dataType.lt(Integer.MAX_VALUE,0));
        assertFalse(this.dataType.lt(0,Integer.MIN_VALUE));
        assertTrue( this.dataType.lt(Integer.MIN_VALUE,0));

        assertTrue(this.dataType.lte(0,0));
        assertTrue(this.dataType.lte(Integer.MAX_VALUE,Integer.MAX_VALUE));
        assertFalse(this.dataType.lte(Integer.MAX_VALUE,Integer.MIN_VALUE));
        assertTrue( this.dataType.lte(Integer.MIN_VALUE,Integer.MAX_VALUE));
        assertTrue( this.dataType.lte(0,Integer.MAX_VALUE));
        assertFalse(this.dataType.lte(Integer.MAX_VALUE,0));
        assertFalse(this.dataType.lte(0,Integer.MIN_VALUE));
        assertTrue( this.dataType.lte(Integer.MIN_VALUE,0));

        assertFalse(this.dataType.gt(0,0));
        assertFalse(this.dataType.gt(Integer.MAX_VALUE,Integer.MAX_VALUE));
        assertTrue(this.dataType.gt(Integer.MAX_VALUE,Integer.MIN_VALUE));
        assertFalse( this.dataType.gt(Integer.MIN_VALUE,Integer.MAX_VALUE));
        assertFalse( this.dataType.gt(0,Integer.MAX_VALUE));
        assertTrue(this.dataType.gt(Integer.MAX_VALUE,0));
        assertTrue(this.dataType.gt(0,Integer.MIN_VALUE));
        assertFalse( this.dataType.gt(Integer.MIN_VALUE,0));

        assertTrue( this.dataType.gte(0,0));
        assertTrue( this.dataType.gte(Integer.MAX_VALUE,Integer.MAX_VALUE));
        assertTrue( this.dataType.gte(Integer.MAX_VALUE,Integer.MIN_VALUE));
        assertFalse(this.dataType.gte(Integer.MIN_VALUE,Integer.MAX_VALUE));
        assertFalse(this.dataType.gte(0,Integer.MAX_VALUE));
        assertTrue( this.dataType.gte(Integer.MAX_VALUE,0));
        assertTrue( this.dataType.gte(0,Integer.MIN_VALUE));
        assertFalse(this.dataType.gte(Integer.MIN_VALUE,0));
    }


    @Test
    public void difference() throws Exception{
        assertEquals(2.0, dataType.getDifference(1,3), 0.001);
        assertEquals(0.0, dataType.getDifference(1,1), 0.0);
        assertEquals(1.0, dataType.getDifference(1,2), 0.001);
        assertEquals(0.8, dataType.getDifference(10,2), 0.001);
        assertEquals(1.0, dataType.getDifference(null,1), 0.001);
        assertEquals(1.0, dataType.getDifference(1, null), 0.001);
        assertEquals(0.0, dataType.getDifference(null, null), 0.001);
        assertEquals(0.0, dataType.getDifference(0, 0), 0.001);
        assertEquals(1.0, dataType.getDifference(0,1), 0.001);
        assertEquals(1.0, dataType.getDifference(1, 0), 0.001);
    }

    @Test
    public void customDifference() throws Exception{
        dataType.setDataTypeDifference(new DataTypeDifference<Integer>() {
            @Override
            public Double difference(Integer a, Integer b) {
                return (double) (Math.abs(a - b));
            }
        });
        assertEquals(2.0, dataType.getDifference(1,3), 0.001);
        assertEquals(0.0, dataType.getDifference(1,1), 0.0);
        assertEquals(1.0, dataType.getDifference(1,2), 0.001);
        assertEquals(8, dataType.getDifference(10,2), 0.001);
        assertEquals(0.0, dataType.getDifference(0, 0), 0.001);
        assertEquals(1.0, dataType.getDifference(0,1), 0.001);
        assertEquals(1.0, dataType.getDifference(1, 0), 0.001);
    }

    @Test
    public void testOverwrite(){
        IntDataType dt = new IntDataType(){
            @Override
            @SmucFunction
            public Integer plus(Integer[] values) {
                return 1 + 1;
            }
            @Override
            public Integer times(Integer[] values) {
                return 1 + 1;
            }
        };

        assertEquals(2, (int)dt.plus(1,0));
        assertEquals(2, (int)dt.function("plus", null, 1,0));
        assertEquals(2, (int)dt.function("plus", null, 1,1));
        assertEquals(2, (int)dt.function("plus", null, 1,2));
        assertEquals(2, (int)dt.function("plus", null, 0,0));
        assertEquals(2, (int)dt.function("plus", null));

        assertEquals(2, (int)dt.function("times", null, 1,0));
        assertEquals(2, (int)dt.function("times", null, 1,1));
        assertEquals(2, (int)dt.function("times", null, 1,2));
        assertEquals(2, (int)dt.function("times", null, 0,0));
        assertEquals(2, (int)dt.function("times", null));

        assertEquals(1, (int)dt.function("-", null, 1,0));
        assertEquals(0, (int)dt.function("-", null, 1,1));
        assertEquals(-1, (int)dt.function("-", null, 1,2));
        assertEquals(0, (int)dt.function("-", null, 0,0));
        assertEquals(0, (int)dt.function("-", null));

        try {
            assertEquals(2, (int) dt.function("+", null, 1, 0));
            assertTrue(false);
        }
        catch (Exception e){
            assertTrue(true);
        }


    }
}
