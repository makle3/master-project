package smucparser.datatypes;

import org.antlr.v4.runtime.misc.Pair;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.Test;
import smucparser.MyException;
import smucparser.SMuC;
import smucparser.TargetOption;
import smucparser.datatypes.complex.PrismBoolPairInitialValue;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.graph.Import.CSV;
import smucparser.graph.Import.DOT;
import smucparser.graph.Import.PRISMTransitionMatrix;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by Martin on 25-03-2016.
 */
public class PrismInitialValueDataTypeTest {
    @Test
    public void test(){
        String input = "#Types\n" +
                "#Vertexes (SourceID, Name (, item)*)\n" +
                "1,,i:1.0\n" +
                "2,,i:bot\n" +
                "\n" +
                "#Edges (SourceID, TargetID, (Capability|Item)*)\n" +
                "1,1,\"p(x,s,t):=0.5*x\"\n" +
                "1,2,\"p(x,s,t):=0.5*x\"\n" +
                "2,2,\"p(x,s,t):=1.0*x\"";
        DefaultDirectedGraph<MyVertex,MyEdge> g = null;
        try {
            g = CSV.deserialize(PrismBoolPairInitialValue.class, input);
            SMuC.performQuery(PrismBoolPairInitialValue.class, g, "lfp steady. +>p<(plusI(i,steady))");
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
        MyVertex[] vertexSet = g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        assertEquals(0d, ((Pair) vertexSet[0].getFormulaVariable("steady").getValue()).a);
        assertEquals(1d, ((Pair) vertexSet[1].getFormulaVariable("steady").getValue()).a);
    }

    @Test
    public void test2(){
        String input = "#Types\n" +
                "#Vertexes (SourceID, Name (, item)*)\n" +
                "1,,i:1.0\n" +
                "2,,i:bot\n" +
                "\n" +
                "#Edges (SourceID, TargetID, (Capability|Item)*)\n" +
                "1,1,\"p(x,s,t):=0.5*x\"\n" +
                "1,2,\"p(x,s,t):=0.5*x\"\n" +
                "2,2,\"p(x,s,t):=1.0*x\"";
        DefaultDirectedGraph<MyVertex,MyEdge> g = null;
        try {
            g = CSV.deserialize(PrismBoolPairInitialValue.class, input);
            SMuC.performQuery(PrismBoolPairInitialValue.class, g, "lfp steady. +>p<(plusI(i,steady))");
            SMuC.performQuery(PrismBoolPairInitialValue.class, g, "lfp left. left(steady)");
            assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
        MyVertex[] vertexSet = g.vertexSet().toArray(new MyVertex[g.vertexSet().size()]);
        assertEquals(0d, ((Pair) vertexSet[0].getFormulaVariable("steady").getValue()).a);
        assertEquals(1d, ((Pair) vertexSet[1].getFormulaVariable("steady").getValue()).a);
    }

    @Test
    public void testPrism(){
        DefaultDirectedGraph<MyVertex, MyEdge> g = null;
        try {
            g = PRISMTransitionMatrix.importMatrix("./example-graphs/PRISM/transitionMatrix.txt","./example-graphs/PRISM/states.txt",PrismBoolPairInitialValue.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            SMuC.performQuery(PrismBoolPairInitialValue.class, g, "lfp steady. +>f<(plusI(i,steady))");
            SMuC.performQuery(PrismBoolPairInitialValue.class, g, "gfp x. gt(left(steady),left(0.0))");
            assertTrue(true);
        } catch (DataTypeException e) {
            assertFalse(true);
            e.printStackTrace();

        }
        //DOT.printGraph(g, "prism.pdf");
    }
    @Test
    public void testPrismEpsilon(){
        DefaultDirectedGraph<MyVertex, MyEdge> g = null;
        try {
            g = PRISMTransitionMatrix.importMatrix("./example-graphs/PRISM/transitionMatrix.txt","./example-graphs/PRISM/states.txt",PrismBoolPairInitialValue.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            SMuC smuc = new SMuC();
            smuc.setEpsilon(0.01);
            smuc.setTargetOption(TargetOption.EPSILON);
            smuc.setDefaultDataType(PrismBoolPairInitialValue.class);
            smuc.addQuery("lfp steady. +>f<(plusI(i,steady))");
            smuc.addQuery("gfp x. gt(left(steady),left(0.0))");
            smuc.run(g);
            assertTrue(true);
        } catch (MyException e) {
            e.printStackTrace();
        }
        //DOT.printGraph(g, "prism.pdf");
    }
}
