package smucparser;

import org.apache.commons.lang3.time.StopWatch;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.complex.MST.MSTDataType;
import smucparser.datatypes.simple.BoolDataType;
import smucparser.datatypes.simple.IntDataType;
import smucparser.graph.Import.DOT;
import smucparser.graph.Import.JSON;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by Martin on 06-05-2016.
 */
public class CTLTest {
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;

    @Before
    public void setUp() throws Exception {


        MyVertexFactory factory = new MyVertexFactory();
        AbstractDataType dataType = AbstractDataType.getInstance(BoolDataType.class);

        MyVertex V0 = factory.createVertex();
        MyVertex V1 = factory.createVertex();
        MyVertex V2 = factory.createVertex();
        MyVertex V3 = factory.createVertex();

        V0.addAttribute("a", dataType.newInstance(true));
        V1.addAttribute("a", dataType.newInstance(true));
        V2.addAttribute("a", dataType.newInstance(false));
        V3.addAttribute("a", dataType.newInstance(true));

        V0.addAttribute("b", dataType.newInstance(false));
        V1.addAttribute("b", dataType.newInstance(true));
        V2.addAttribute("b", dataType.newInstance(true));
        V3.addAttribute("b", dataType.newInstance(false));

        // add the vertices
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        g.addVertex(V0);
        g.addVertex(V1);
        g.addVertex(V2);
        g.addVertex(V3);


        g.addEdge(V0, V1);
        g.addEdge(V1, V0);
        g.addEdge(V0, V2);
        g.addEdge(V2, V1);
        g.addEdge(V1, V3);
        g.addEdge(V3, V3);

        this.graph = g;
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void EX() throws Exception {
        SMuC.performQuery(BoolDataType.class,this.graph,"lfp Z. or<>a");
        MyVertex[] vertices =  graph.vertexSet().toArray(new MyVertex[graph.vertexSet().size()]);
        assertEquals(true, vertices[0].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[1].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[2].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[3].getFormulaVariable("Z").getValue());
    }
    @Test
    public void AX() throws Exception {
        SMuC.performQuery(BoolDataType.class,this.graph,"lfp Z. and<>a");
        MyVertex[] vertices =  graph.vertexSet().toArray(new MyVertex[graph.vertexSet().size()]);
        assertEquals(false, vertices[0].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[1].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[2].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[3].getFormulaVariable("Z").getValue());
    }
    @Test
    public void EG() throws Exception {
        SMuC.performQuery(BoolDataType.class,this.graph,"gfp Z. and(a,or<>Z)");
        MyVertex[] vertices =  graph.vertexSet().toArray(new MyVertex[graph.vertexSet().size()]);
        assertEquals(true, vertices[0].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[1].getFormulaVariable("Z").getValue());
        assertEquals(false, vertices[2].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[3].getFormulaVariable("Z").getValue());
    }
    @Test
    public void AG() throws Exception {
        SMuC.performQuery(BoolDataType.class,this.graph,"gfp Z. and(a,and<>Z)");
        MyVertex[] vertices =  graph.vertexSet().toArray(new MyVertex[graph.vertexSet().size()]);
        assertEquals(false, vertices[0].getFormulaVariable("Z").getValue());
        assertEquals(false, vertices[1].getFormulaVariable("Z").getValue());
        assertEquals(false, vertices[2].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[3].getFormulaVariable("Z").getValue());
    }
    @Test
    public void EFEG() throws Exception {
        SMuC.performQuery(BoolDataType.class,this.graph,"lfp Z. or(gfp Z2. and(a,or<>Z2),(or<>Z))");
        MyVertex[] vertices =  graph.vertexSet().toArray(new MyVertex[graph.vertexSet().size()]);
        assertEquals(true, vertices[0].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[1].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[2].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[3].getFormulaVariable("Z").getValue());
    }
    @Test
    public void AaUb() throws Exception {
        SMuC.performQuery(BoolDataType.class,this.graph,"lfp Z. or(b,and(a,and<>Z))");
        MyVertex[] vertices =  graph.vertexSet().toArray(new MyVertex[graph.vertexSet().size()]);
        assertEquals(true, vertices[0].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[1].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[2].getFormulaVariable("Z").getValue());
        assertEquals(false, vertices[3].getFormulaVariable("Z").getValue());
    }

    /**
     * E(a U (not(a) and A(not(a) U b)))
     */
    @Test
    public void EaUnaandAnaUb() throws Exception {
        SMuC.performQuery(BoolDataType.class,this.graph,"lfp Z. or((and(not(a), lfp Z2. or(b,and(not(a),and<>Z2)))), and(a, or<>Z))");
        MyVertex[] vertices =  graph.vertexSet().toArray(new MyVertex[graph.vertexSet().size()]);
        assertEquals(true, vertices[0].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[1].getFormulaVariable("Z").getValue());
        assertEquals(true, vertices[2].getFormulaVariable("Z").getValue());
        assertEquals(false, vertices[3].getFormulaVariable("Z").getValue());
    }
}
