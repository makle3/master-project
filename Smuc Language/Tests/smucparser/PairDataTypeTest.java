package smucparser;

import org.antlr.v4.runtime.misc.Pair;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.simple.BoolDataType;
import smucparser.datatypes.simple.IntDataType;
import smucparser.datatypes.complex.PairDataType;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * Created by Martin on 08-04-2016.
 */
public class PairDataTypeTest {
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void asd() throws Exception {
        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);

        PairDataType dataType = new PairDataType<>(IntDataType.class, BoolDataType.class);

        System.out.println(dataType.getTop());
        System.out.println(dataType.getBottom());

        MyVertexFactory factory = new MyVertexFactory();


        HashMap<String, AbstractDataType> v1 = new HashMap<>();
        MyVertex V1 = factory.createVertex(v1);
        g.addVertex(V1);

        assertEquals(new Pair<Integer, Boolean>(0,false), dataType.getBottom());
        assertEquals(new Pair<Integer, Boolean>(Integer.MAX_VALUE,true), dataType.getTop());
    }
}
