grammar Grammar;

start_rule : psi EOF;

//Operators at the top have higher precedence
psi :
      '(' psi_inner=psi ')'
      //| edge_vertex_interpret
      | function
      | aggregate
      | left=psi op=(STAR | SLASH) right=psi
      | left=psi op=(PLUS | DASH) right=psi
      | fixpoint
      | value;

value : number | variable | string;

variable : VARNAME;

string : '"' ( ~('"')|WS|'\\"' )* '"';

number : DASH?(num=INTEGER | num=DOUBLE);

function : op=func_name '(' inner=function_inner ')';

aggregate : op=func_name direction=edge_direction right=psi;

func_name : VARNAME | (PLUS | DASH | SLASH | STAR);



edge_direction :  dir=UNDIRECTED_EDGE_START capability=edge_capability? UNDIRECTED_EDGE_END
                | dir=OUTGOING_EDGE_START   capability=edge_capability? INGOING_EDGE_START
                | dir=INGOING_EDGE_START    capability=edge_capability? OUTGOING_EDGE_START;

edge_capability : capability=VARNAME;

fixpoint : fp=('lfp'|'gfp') op=VARNAME DOT right=psi;
//gfp : 'gfp' op=VARNAME DOT right=psi;

function_inner: (psi ',')* psi;

//f(source, target, x):=x+1;
edge_function : op=VARNAME'('parameter=VARNAME','source=VARNAME','target=VARNAME')' ':=' psi EOF
              | op=VARNAME'('parameter=VARNAME')''('source=VARNAME','target=VARNAME')' ':=' psi EOF;




WS  :   [ \t\r\n]+ ->  channel(HIDDEN) ;
END :   EOF ->  channel(HIDDEN) ;
VARNAME : [a-zA-Z]([a-zA-Z]|UNDERSCORE|INTEGER)*;
INTEGER : [0-9]+;
DOUBLE : INTEGER DOT INTEGER;


PLUS : '+';
DASH : '-';
SLASH : '/';
STAR : '*';
UNDERSCORE: '_';
DOT : '.';
UNDIRECTED_EDGE_START : '[';
UNDIRECTED_EDGE_END : ']';
INGOING_EDGE_START: '>';
OUTGOING_EDGE_START: '<';