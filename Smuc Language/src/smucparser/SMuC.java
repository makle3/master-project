package smucparser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.grammar.ThrowingErrorListener;
import smucparser.grammar.TypeValueAndResult;
import smucparser.grammar.generated.GrammarLexer;
import smucparser.grammar.generated.GrammarParser;
import smucparser.grammar.semantics.SmucListener;
import smucparser.grammar.typecheck.EdgeFunctionTypeCheck;
import smucparser.grammar.typecheck.TypeCheck;
import smucparser.grammar.visitor.SmucVisitor;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;

import java.util.*;


/**
 * Created by Martin on 23-02-2016.
 */
public class SMuC {
    private TargetOption targetOption = TargetOption.EQUALS;
    private Long maxIterations = 0L;
    private Class<? extends AbstractDataType> dataType;
    private List<String> queries = new ArrayList<>();
    private DefaultDirectedGraph<MyVertex, MyEdge> graph;
    private Double epsilon = 0.0;


    public SMuC setMaxIterations(Long iterations){
        this.maxIterations = iterations;
        return this;
    }
    public SMuC setDefaultDataType(Class<? extends AbstractDataType> dataType) {
        this.dataType = dataType;
        return this;
    }

    public SMuC addQuery(String query){
        this.queries.add(query);
        return this;
    }

    public SMuC setTargetOption(TargetOption targetOption) {
        this.targetOption = targetOption;
        return this;
    }

    public SMuC setEpsilon(Double epsilon){
        this.epsilon = epsilon;
        return this;
    }


    public void run(DefaultDirectedGraph<MyVertex, MyEdge> graph) throws MyException {
        this.graph = graph;
        for (int i = 0; i < this.queries.size(); i++) {
            performQuery1(this.queries.get(i));
        }
    }

    private void performQuery1(String query) throws MyException {

        // Begin syntax checking + type checking + performing query
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(query));
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));
        parser.removeErrorListeners();
        parser.addErrorListener(ThrowingErrorListener.INSTANCE);
        ParseTreeWalker walker = new ParseTreeWalker();

        GrammarParser.Start_ruleContext tree = parser.start_rule();

        // Listener check syntax and semantics
        AbstractDataType dataTypeObj = AbstractDataType.getInstance(dataType);
        SmucListener listener = new SmucListener(this.graph);
        walker.walk(listener,tree);


        TypeCheck tc = checkTypes(dataType, this.graph, tree);
        ParseTreeProperty<TypeValueAndResult> types = tc.getTypesProperty();

        //Check types of edge capabilities
        //Only check the types of edge capabilities that are used in a query.
        getEdgeFunctionTypes(dataType, this.graph, listener.getEdgeCapabilitiesUsed(), tc.getTypes());


        SmucVisitor visitor = new SmucVisitor(dataTypeObj, this.graph, listener.getVarnames());
        visitor.setTypes(types);
        visitor.setNestedLevel(SmucListener.getNestedLevelProperty());
        visitor.setMaxIterations(maxIterations);
        visitor.setTargetOption(this.targetOption);
        visitor.setEpsilon(this.epsilon);
        visitor.visit(tree);

    }


    /**
     * This method is used to perform a query on the given graph.
     * @param dataType The datatype given to literals as a default.
     * @param g The graph to perform the query on.
     * @param s The query itself.
     * @throws DataTypeException
     */
    public static void performQuery(Class<? extends AbstractDataType>  dataType,
                                    DefaultDirectedGraph<MyVertex, MyEdge> g,
                                    String s) throws DataTypeException {
        performQuery(dataType,g,s,0L);
    }
    public static void performQuery(Class<? extends AbstractDataType>  dataType,
                                    DefaultDirectedGraph<MyVertex, MyEdge> g,
                                    String s, Long maxIterations) throws DataTypeException {
        SMuC smuc = new SMuC();
        try {
            smuc.setDefaultDataType(dataType)
                    .addQuery(s)
                    .setMaxIterations(maxIterations)
                    .run(g);
        } catch (MyException  e) {
            e.printStackTrace();
        }
    }

    public static TypeCheck checkTypes(Class<? extends AbstractDataType> dataType,
                                                                            Graph<MyVertex, MyEdge> g,
                                                                            GrammarParser.Start_ruleContext tree) throws MyException {
        Map<String, AbstractDataType> graphTypeMap;
        graphTypeMap = checkGraphItemTypes(g);
        TypeCheck tc = new TypeCheck(dataType);

        for (Map.Entry<String,AbstractDataType> item : graphTypeMap.entrySet()) {
            tc.addType(item.getKey(), item.getValue().getClass());
        }
        tc.visit(tree);
        return tc;
    }

    private static HashMap<String, Class<? extends AbstractDataType>> getEdgeFunctionTypes(Class<? extends AbstractDataType> dataType,
                                                                                           Graph<MyVertex, MyEdge> g,
                                                                                           HashSet<String> edgeCapabilitiesUsed,
                                                                                           HashMap<String, Class<? extends AbstractDataType>> types) {
        HashMap<String, Class<? extends AbstractDataType>> dt = new HashMap<>();
        for (MyEdge e : g.edgeSet()) {
            for (Object o : e.getEdgeCapabilities().values()) {
                if(o == null){
                    continue;
                }
                EdgeFunctionTypeCheck edgeTc = new EdgeFunctionTypeCheck(dataType, edgeCapabilitiesUsed);
                edgeTc.setTypes(types);
                GrammarParser.Edge_functionContext parseTree = (GrammarParser.Edge_functionContext)o;
                dt.put(parseTree.op.getText(),(Class<? extends AbstractDataType>) edgeTc.visit(parseTree));
            }
        }
        return dt;
    }

    public static Map<String, AbstractDataType> checkGraphItemTypes(Graph<MyVertex, MyEdge> graph) throws MyException {
        Map<String, AbstractDataType> resultMap = new HashMap<>();
        for (MyVertex v : graph.vertexSet()) {
            Map<String, AbstractDataType> itemMap = v.getAttributes();
            for (Map.Entry<String,AbstractDataType> item : itemMap.entrySet()) {
                if(resultMap.containsKey(item.getKey())){
                    if(!resultMap.get(item.getKey()).getClass().equals(item.getValue().getClass())){
                        throw new MyException("Some node variables with the same name have different types.");
                    }
                }
                else{
                    resultMap.put(item.getKey(),item.getValue());
                }
            }

            // Add the types of formula variables used, in case multiple queries are run in succession
            for(Map.Entry<String,AbstractDataType> item : v.getFormulaVariableTypes().entrySet()){
                resultMap.put(item.getKey(),item.getValue());
            }
        }

        for(MyEdge e : graph.edgeSet()){ //// FIXME: 02-04-2016 duplicate code
            Map<String, AbstractDataType> itemMap = e.getAttributeTypes();
            for (Map.Entry<String,AbstractDataType> item : itemMap.entrySet()) {
                if(resultMap.containsKey(item.getKey())){
                    if(!resultMap.get(item.getKey()).getClass().equals(item.getValue().getClass())){
                        throw new MyException("Some edge variables with the same name have different types.");
                    }
                }
                else{
                    resultMap.put(item.getKey(),item.getValue());
                }
            }
        }

        return resultMap;
    }

    public static String getTypeName(Class clazz){
        if(clazz == null){
            return null;
        }
        return clazz.getName().replaceFirst("class ", "");
    }



}
