package smucparser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import smucparser.grammar.ThrowingErrorListener;
import smucparser.grammar.generated.GrammarBaseListener;
import smucparser.grammar.generated.GrammarLexer;
import smucparser.grammar.generated.GrammarParser;

import java.util.HashMap;

/**
 * Created by Martin on 03-05-2016.
 */
public class GraphPrinter extends GrammarBaseListener{
    private final StringBuilder stringBuilder;
    private Integer nodeId = 0;
    Integer indent = 0;
    private HashMap<Integer, Integer> currentAtLevel = new HashMap<>();



    public GraphPrinter(StringBuilder stringBuilder) {
        this.stringBuilder = stringBuilder;
    }

    //gfp z. min(goal, min[] +(z,1))
    public static void main(String[] args) {
        String query = "gfp z. min(goal, min[] z+1)";
        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(query));
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));
        parser.removeErrorListeners();
        parser.addErrorListener(ThrowingErrorListener.INSTANCE);
        ParseTreeWalker walker = new ParseTreeWalker();
        StringBuilder stringBuilder = new StringBuilder();
        walker.walk(new GraphPrinter(stringBuilder), parser.start_rule());

        System.out.println(stringBuilder.toString());
    }

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        if (test(ctx)) return;

        if(ctx instanceof GrammarParser.Start_ruleContext) {
            this.stringBuilder.append("graph{\n");
            return;
        }
        if(ctx instanceof GrammarParser.FixpointContext){
            addNode(nodeId + 1000, ((GrammarParser.FixpointContext) ctx).op.getText());

            addEdge(nodeId, nodeId+1000);
        }
        addNode(nodeId, ctx.getClass().getSimpleName().replace("Context", ""));

        currentAtLevel.put(indent, nodeId);

        if(currentAtLevel.containsKey(indent-1)){
            addEdge(currentAtLevel.get(indent-1), nodeId);
        }

        indent += 1;
        nodeId += 1;
    }

    private void addNode(Integer id, String text) {
        this.stringBuilder.append(String.format("%d[label=%s];\n", id, text));
    }
    private void addEdge(Integer source, Integer target) {
        this.stringBuilder.append(source + "--" + target + ";\n");
    }

    private boolean test(ParserRuleContext ctx) {
        if(ctx instanceof GrammarParser.PsiContext){
            if(((GrammarParser.PsiContext) ctx).op == null) {
                return true;
            }
        }
        if(ctx instanceof GrammarParser.ValueContext){
            return true;
        }
        if(ctx instanceof GrammarParser.Function_innerContext){
            return true;
        }
        return false;
    }

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        if (test(ctx)) return;
        if(ctx instanceof GrammarParser.Start_ruleContext) {
            this.stringBuilder.append("}");//End graph
            return;
        }
        indent -= 1;
    }
}
