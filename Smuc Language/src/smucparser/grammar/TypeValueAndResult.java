package smucparser.grammar;

import smucparser.datatypes.AbstractDataType;

/**
 * A container class used as a part of the type checking, to keep track if the input type, and the output type
 * of a function.
 */
public class TypeValueAndResult {
    private Class<? extends AbstractDataType> abstractResultType;
    private Class<? extends AbstractDataType> abstractValueType;

    /**
     * Used to initialize {@link TypeValueAndResult} when the value type and the result type are the same.
     * @param value The type of the value, extending {@link AbstractDataType}
     */
    public TypeValueAndResult(Class<? extends AbstractDataType> value){
        this(value, value);
    }

    /**
     * Used to initialize {@link TypeValueAndResult} when the value type and the result type are different.
     * @param value The type of the value, extending {@link AbstractDataType}
     * @param result The type of the result, extending {@link AbstractDataType}
     */
    public TypeValueAndResult(Class<? extends AbstractDataType> value,Class<? extends AbstractDataType> result){
        this.abstractResultType = result;
        this.abstractValueType = value;
    }

    /**
     * @return the {@link AbstractDataType} of the value.
     */
    public Class<? extends AbstractDataType> getAbstractValueType() {
        return abstractValueType;
    }

    /**
     * Set the datatype of a value used during type checking.
     * @param abstractValueType the {@link AbstractDataType} of the value.
     */
    public void setAbstractValueType(Class<? extends AbstractDataType> abstractValueType) {
        this.abstractValueType = abstractValueType;
        if(this.abstractResultType == null){
            this.abstractResultType = abstractValueType;
        }
    }

    /**
     * @return the {@link AbstractDataType} of the result.
     */
    public Class<? extends AbstractDataType> getAbstractResultType() {
        return abstractResultType;
    }

    /**
     * Set the resulting datatype of performing a function.
     * @param abstractResultType the resulting {@link AbstractDataType} of performing a function.
     */
    public void setAbstractResultType(Class<? extends AbstractDataType> abstractResultType) {
        this.abstractResultType = abstractResultType;
    }
}
