// Generated from C:/Users/martin/Documents/Git/master-project/Smuc Language\Grammar.g4 by ANTLR 4.5.1
package smucparser.grammar.generated;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GrammarParser}.
 */
public interface GrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link GrammarParser#start_rule}.
	 * @param ctx the parse tree
	 */
	void enterStart_rule(GrammarParser.Start_ruleContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#start_rule}.
	 * @param ctx the parse tree
	 */
	void exitStart_rule(GrammarParser.Start_ruleContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#psi}.
	 * @param ctx the parse tree
	 */
	void enterPsi(GrammarParser.PsiContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#psi}.
	 * @param ctx the parse tree
	 */
	void exitPsi(GrammarParser.PsiContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(GrammarParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(GrammarParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(GrammarParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(GrammarParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(GrammarParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(GrammarParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumber(GrammarParser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumber(GrammarParser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(GrammarParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(GrammarParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#aggregate}.
	 * @param ctx the parse tree
	 */
	void enterAggregate(GrammarParser.AggregateContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#aggregate}.
	 * @param ctx the parse tree
	 */
	void exitAggregate(GrammarParser.AggregateContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#func_name}.
	 * @param ctx the parse tree
	 */
	void enterFunc_name(GrammarParser.Func_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#func_name}.
	 * @param ctx the parse tree
	 */
	void exitFunc_name(GrammarParser.Func_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#edge_direction}.
	 * @param ctx the parse tree
	 */
	void enterEdge_direction(GrammarParser.Edge_directionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#edge_direction}.
	 * @param ctx the parse tree
	 */
	void exitEdge_direction(GrammarParser.Edge_directionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#edge_capability}.
	 * @param ctx the parse tree
	 */
	void enterEdge_capability(GrammarParser.Edge_capabilityContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#edge_capability}.
	 * @param ctx the parse tree
	 */
	void exitEdge_capability(GrammarParser.Edge_capabilityContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#fixpoint}.
	 * @param ctx the parse tree
	 */
	void enterFixpoint(GrammarParser.FixpointContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#fixpoint}.
	 * @param ctx the parse tree
	 */
	void exitFixpoint(GrammarParser.FixpointContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#function_inner}.
	 * @param ctx the parse tree
	 */
	void enterFunction_inner(GrammarParser.Function_innerContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#function_inner}.
	 * @param ctx the parse tree
	 */
	void exitFunction_inner(GrammarParser.Function_innerContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#edge_function}.
	 * @param ctx the parse tree
	 */
	void enterEdge_function(GrammarParser.Edge_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#edge_function}.
	 * @param ctx the parse tree
	 */
	void exitEdge_function(GrammarParser.Edge_functionContext ctx);
}