// Generated from C:/Users/martin/Documents/Git/master-project/Smuc Language\Grammar.g4 by ANTLR 4.5.1
package smucparser.grammar.generated;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GrammarParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, WS=9, 
		END=10, VARNAME=11, INTEGER=12, DOUBLE=13, PLUS=14, DASH=15, SLASH=16, 
		STAR=17, UNDERSCORE=18, DOT=19, UNDIRECTED_EDGE_START=20, UNDIRECTED_EDGE_END=21, 
		INGOING_EDGE_START=22, OUTGOING_EDGE_START=23;
	public static final int
		RULE_start_rule = 0, RULE_psi = 1, RULE_value = 2, RULE_variable = 3, 
		RULE_string = 4, RULE_number = 5, RULE_function = 6, RULE_aggregate = 7, 
		RULE_func_name = 8, RULE_edge_direction = 9, RULE_edge_capability = 10, 
		RULE_fixpoint = 11, RULE_function_inner = 12, RULE_edge_function = 13;
	public static final String[] ruleNames = {
		"start_rule", "psi", "value", "variable", "string", "number", "function", 
		"aggregate", "func_name", "edge_direction", "edge_capability", "fixpoint", 
		"function_inner", "edge_function"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "')'", "'\"'", "'\\\"'", "'lfp'", "'gfp'", "','", "':='", 
		null, null, null, null, null, "'+'", "'-'", "'/'", "'*'", "'_'", "'.'", 
		"'['", "']'", "'>'", "'<'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, "WS", "END", "VARNAME", 
		"INTEGER", "DOUBLE", "PLUS", "DASH", "SLASH", "STAR", "UNDERSCORE", "DOT", 
		"UNDIRECTED_EDGE_START", "UNDIRECTED_EDGE_END", "INGOING_EDGE_START", 
		"OUTGOING_EDGE_START"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Grammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public GrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class Start_ruleContext extends ParserRuleContext {
		public PsiContext psi() {
			return getRuleContext(PsiContext.class,0);
		}
		public TerminalNode EOF() { return getToken(GrammarParser.EOF, 0); }
		public Start_ruleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start_rule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterStart_rule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitStart_rule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitStart_rule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Start_ruleContext start_rule() throws RecognitionException {
		Start_ruleContext _localctx = new Start_ruleContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start_rule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28);
			psi(0);
			setState(29);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PsiContext extends ParserRuleContext {
		public PsiContext left;
		public PsiContext psi_inner;
		public Token op;
		public PsiContext right;
		public List<PsiContext> psi() {
			return getRuleContexts(PsiContext.class);
		}
		public PsiContext psi(int i) {
			return getRuleContext(PsiContext.class,i);
		}
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public AggregateContext aggregate() {
			return getRuleContext(AggregateContext.class,0);
		}
		public FixpointContext fixpoint() {
			return getRuleContext(FixpointContext.class,0);
		}
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public TerminalNode STAR() { return getToken(GrammarParser.STAR, 0); }
		public TerminalNode SLASH() { return getToken(GrammarParser.SLASH, 0); }
		public TerminalNode PLUS() { return getToken(GrammarParser.PLUS, 0); }
		public TerminalNode DASH() { return getToken(GrammarParser.DASH, 0); }
		public PsiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_psi; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterPsi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitPsi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitPsi(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PsiContext psi() throws RecognitionException {
		return psi(0);
	}

	private PsiContext psi(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PsiContext _localctx = new PsiContext(_ctx, _parentState);
		PsiContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_psi, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(40);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				{
				setState(32);
				match(T__0);
				setState(33);
				((PsiContext)_localctx).psi_inner = psi(0);
				setState(34);
				match(T__1);
				}
				break;
			case 2:
				{
				setState(36);
				function();
				}
				break;
			case 3:
				{
				setState(37);
				aggregate();
				}
				break;
			case 4:
				{
				setState(38);
				fixpoint();
				}
				break;
			case 5:
				{
				setState(39);
				value();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(50);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(48);
					switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
					case 1:
						{
						_localctx = new PsiContext(_parentctx, _parentState);
						_localctx.left = _prevctx;
						_localctx.left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_psi);
						setState(42);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(43);
						((PsiContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==SLASH || _la==STAR) ) {
							((PsiContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(44);
						((PsiContext)_localctx).right = psi(5);
						}
						break;
					case 2:
						{
						_localctx = new PsiContext(_parentctx, _parentState);
						_localctx.left = _prevctx;
						_localctx.left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_psi);
						setState(45);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(46);
						((PsiContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==DASH) ) {
							((PsiContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(47);
						((PsiContext)_localctx).right = psi(4);
						}
						break;
					}
					} 
				}
				setState(52);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_value);
		try {
			setState(56);
			switch (_input.LA(1)) {
			case INTEGER:
			case DOUBLE:
			case DASH:
				enterOuterAlt(_localctx, 1);
				{
				setState(53);
				number();
				}
				break;
			case VARNAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(54);
				variable();
				}
				break;
			case T__2:
				enterOuterAlt(_localctx, 3);
				{
				setState(55);
				string();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public TerminalNode VARNAME() { return getToken(GrammarParser.VARNAME, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58);
			match(VARNAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public List<TerminalNode> WS() { return getTokens(GrammarParser.WS); }
		public TerminalNode WS(int i) {
			return getToken(GrammarParser.WS, i);
		}
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitString(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitString(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_string);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(60);
			match(T__2);
			setState(66);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << WS) | (1L << END) | (1L << VARNAME) | (1L << INTEGER) | (1L << DOUBLE) | (1L << PLUS) | (1L << DASH) | (1L << SLASH) | (1L << STAR) | (1L << UNDERSCORE) | (1L << DOT) | (1L << UNDIRECTED_EDGE_START) | (1L << UNDIRECTED_EDGE_END) | (1L << INGOING_EDGE_START) | (1L << OUTGOING_EDGE_START))) != 0)) {
				{
				setState(64);
				switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
				case 1:
					{
					setState(61);
					_la = _input.LA(1);
					if ( _la <= 0 || (_la==T__2) ) {
					_errHandler.recoverInline(this);
					} else {
						consume();
					}
					}
					break;
				case 2:
					{
					setState(62);
					match(WS);
					}
					break;
				case 3:
					{
					setState(63);
					match(T__3);
					}
					break;
				}
				}
				setState(68);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(69);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public Token num;
		public TerminalNode DASH() { return getToken(GrammarParser.DASH, 0); }
		public TerminalNode INTEGER() { return getToken(GrammarParser.INTEGER, 0); }
		public TerminalNode DOUBLE() { return getToken(GrammarParser.DOUBLE, 0); }
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitNumber(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitNumber(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_number);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(72);
			_la = _input.LA(1);
			if (_la==DASH) {
				{
				setState(71);
				match(DASH);
				}
			}

			setState(76);
			switch (_input.LA(1)) {
			case INTEGER:
				{
				setState(74);
				((NumberContext)_localctx).num = match(INTEGER);
				}
				break;
			case DOUBLE:
				{
				setState(75);
				((NumberContext)_localctx).num = match(DOUBLE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public Func_nameContext op;
		public Function_innerContext inner;
		public Func_nameContext func_name() {
			return getRuleContext(Func_nameContext.class,0);
		}
		public Function_innerContext function_inner() {
			return getRuleContext(Function_innerContext.class,0);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			((FunctionContext)_localctx).op = func_name();
			setState(79);
			match(T__0);
			setState(80);
			((FunctionContext)_localctx).inner = function_inner();
			setState(81);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AggregateContext extends ParserRuleContext {
		public Func_nameContext op;
		public Edge_directionContext direction;
		public PsiContext right;
		public Func_nameContext func_name() {
			return getRuleContext(Func_nameContext.class,0);
		}
		public Edge_directionContext edge_direction() {
			return getRuleContext(Edge_directionContext.class,0);
		}
		public PsiContext psi() {
			return getRuleContext(PsiContext.class,0);
		}
		public AggregateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterAggregate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitAggregate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitAggregate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AggregateContext aggregate() throws RecognitionException {
		AggregateContext _localctx = new AggregateContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_aggregate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(83);
			((AggregateContext)_localctx).op = func_name();
			setState(84);
			((AggregateContext)_localctx).direction = edge_direction();
			setState(85);
			((AggregateContext)_localctx).right = psi(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_nameContext extends ParserRuleContext {
		public TerminalNode VARNAME() { return getToken(GrammarParser.VARNAME, 0); }
		public TerminalNode PLUS() { return getToken(GrammarParser.PLUS, 0); }
		public TerminalNode DASH() { return getToken(GrammarParser.DASH, 0); }
		public TerminalNode SLASH() { return getToken(GrammarParser.SLASH, 0); }
		public TerminalNode STAR() { return getToken(GrammarParser.STAR, 0); }
		public Func_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterFunc_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitFunc_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFunc_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Func_nameContext func_name() throws RecognitionException {
		Func_nameContext _localctx = new Func_nameContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_func_name);
		int _la;
		try {
			setState(89);
			switch (_input.LA(1)) {
			case VARNAME:
				enterOuterAlt(_localctx, 1);
				{
				setState(87);
				match(VARNAME);
				}
				break;
			case PLUS:
			case DASH:
			case SLASH:
			case STAR:
				enterOuterAlt(_localctx, 2);
				{
				setState(88);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << DASH) | (1L << SLASH) | (1L << STAR))) != 0)) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Edge_directionContext extends ParserRuleContext {
		public Token dir;
		public Edge_capabilityContext capability;
		public TerminalNode UNDIRECTED_EDGE_END() { return getToken(GrammarParser.UNDIRECTED_EDGE_END, 0); }
		public TerminalNode UNDIRECTED_EDGE_START() { return getToken(GrammarParser.UNDIRECTED_EDGE_START, 0); }
		public Edge_capabilityContext edge_capability() {
			return getRuleContext(Edge_capabilityContext.class,0);
		}
		public TerminalNode INGOING_EDGE_START() { return getToken(GrammarParser.INGOING_EDGE_START, 0); }
		public TerminalNode OUTGOING_EDGE_START() { return getToken(GrammarParser.OUTGOING_EDGE_START, 0); }
		public Edge_directionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_edge_direction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterEdge_direction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitEdge_direction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitEdge_direction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Edge_directionContext edge_direction() throws RecognitionException {
		Edge_directionContext _localctx = new Edge_directionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_edge_direction);
		int _la;
		try {
			setState(106);
			switch (_input.LA(1)) {
			case UNDIRECTED_EDGE_START:
				enterOuterAlt(_localctx, 1);
				{
				setState(91);
				((Edge_directionContext)_localctx).dir = match(UNDIRECTED_EDGE_START);
				setState(93);
				_la = _input.LA(1);
				if (_la==VARNAME) {
					{
					setState(92);
					((Edge_directionContext)_localctx).capability = edge_capability();
					}
				}

				setState(95);
				match(UNDIRECTED_EDGE_END);
				}
				break;
			case OUTGOING_EDGE_START:
				enterOuterAlt(_localctx, 2);
				{
				setState(96);
				((Edge_directionContext)_localctx).dir = match(OUTGOING_EDGE_START);
				setState(98);
				_la = _input.LA(1);
				if (_la==VARNAME) {
					{
					setState(97);
					((Edge_directionContext)_localctx).capability = edge_capability();
					}
				}

				setState(100);
				match(INGOING_EDGE_START);
				}
				break;
			case INGOING_EDGE_START:
				enterOuterAlt(_localctx, 3);
				{
				setState(101);
				((Edge_directionContext)_localctx).dir = match(INGOING_EDGE_START);
				setState(103);
				_la = _input.LA(1);
				if (_la==VARNAME) {
					{
					setState(102);
					((Edge_directionContext)_localctx).capability = edge_capability();
					}
				}

				setState(105);
				match(OUTGOING_EDGE_START);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Edge_capabilityContext extends ParserRuleContext {
		public Token capability;
		public TerminalNode VARNAME() { return getToken(GrammarParser.VARNAME, 0); }
		public Edge_capabilityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_edge_capability; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterEdge_capability(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitEdge_capability(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitEdge_capability(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Edge_capabilityContext edge_capability() throws RecognitionException {
		Edge_capabilityContext _localctx = new Edge_capabilityContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_edge_capability);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(108);
			((Edge_capabilityContext)_localctx).capability = match(VARNAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FixpointContext extends ParserRuleContext {
		public Token fp;
		public Token op;
		public PsiContext right;
		public TerminalNode DOT() { return getToken(GrammarParser.DOT, 0); }
		public TerminalNode VARNAME() { return getToken(GrammarParser.VARNAME, 0); }
		public PsiContext psi() {
			return getRuleContext(PsiContext.class,0);
		}
		public FixpointContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fixpoint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterFixpoint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitFixpoint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFixpoint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FixpointContext fixpoint() throws RecognitionException {
		FixpointContext _localctx = new FixpointContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_fixpoint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110);
			((FixpointContext)_localctx).fp = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==T__4 || _la==T__5) ) {
				((FixpointContext)_localctx).fp = (Token)_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(111);
			((FixpointContext)_localctx).op = match(VARNAME);
			setState(112);
			match(DOT);
			setState(113);
			((FixpointContext)_localctx).right = psi(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_innerContext extends ParserRuleContext {
		public List<PsiContext> psi() {
			return getRuleContexts(PsiContext.class);
		}
		public PsiContext psi(int i) {
			return getRuleContext(PsiContext.class,i);
		}
		public Function_innerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_inner; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterFunction_inner(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitFunction_inner(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFunction_inner(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_innerContext function_inner() throws RecognitionException {
		Function_innerContext _localctx = new Function_innerContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_function_inner);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(120);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(115);
					psi(0);
					setState(116);
					match(T__6);
					}
					} 
				}
				setState(122);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			}
			setState(123);
			psi(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Edge_functionContext extends ParserRuleContext {
		public Token op;
		public Token parameter;
		public Token source;
		public Token target;
		public PsiContext psi() {
			return getRuleContext(PsiContext.class,0);
		}
		public TerminalNode EOF() { return getToken(GrammarParser.EOF, 0); }
		public List<TerminalNode> VARNAME() { return getTokens(GrammarParser.VARNAME); }
		public TerminalNode VARNAME(int i) {
			return getToken(GrammarParser.VARNAME, i);
		}
		public Edge_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_edge_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterEdge_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitEdge_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitEdge_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Edge_functionContext edge_function() throws RecognitionException {
		Edge_functionContext _localctx = new Edge_functionContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_edge_function);
		try {
			setState(150);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(125);
				((Edge_functionContext)_localctx).op = match(VARNAME);
				setState(126);
				match(T__0);
				setState(127);
				((Edge_functionContext)_localctx).parameter = match(VARNAME);
				setState(128);
				match(T__6);
				setState(129);
				((Edge_functionContext)_localctx).source = match(VARNAME);
				setState(130);
				match(T__6);
				setState(131);
				((Edge_functionContext)_localctx).target = match(VARNAME);
				setState(132);
				match(T__1);
				setState(133);
				match(T__7);
				setState(134);
				psi(0);
				setState(135);
				match(EOF);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(137);
				((Edge_functionContext)_localctx).op = match(VARNAME);
				setState(138);
				match(T__0);
				setState(139);
				((Edge_functionContext)_localctx).parameter = match(VARNAME);
				setState(140);
				match(T__1);
				setState(141);
				match(T__0);
				setState(142);
				((Edge_functionContext)_localctx).source = match(VARNAME);
				setState(143);
				match(T__6);
				setState(144);
				((Edge_functionContext)_localctx).target = match(VARNAME);
				setState(145);
				match(T__1);
				setState(146);
				match(T__7);
				setState(147);
				psi(0);
				setState(148);
				match(EOF);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return psi_sempred((PsiContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean psi_sempred(PsiContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 4);
		case 1:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\31\u009b\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\3\2\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\5\3+\n\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3\63\n\3\f\3\16"+
		"\3\66\13\3\3\4\3\4\3\4\5\4;\n\4\3\5\3\5\3\6\3\6\3\6\3\6\7\6C\n\6\f\6\16"+
		"\6F\13\6\3\6\3\6\3\7\5\7K\n\7\3\7\3\7\5\7O\n\7\3\b\3\b\3\b\3\b\3\b\3\t"+
		"\3\t\3\t\3\t\3\n\3\n\5\n\\\n\n\3\13\3\13\5\13`\n\13\3\13\3\13\3\13\5\13"+
		"e\n\13\3\13\3\13\3\13\5\13j\n\13\3\13\5\13m\n\13\3\f\3\f\3\r\3\r\3\r\3"+
		"\r\3\r\3\16\3\16\3\16\7\16y\n\16\f\16\16\16|\13\16\3\16\3\16\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u0099\n\17\3\17\2\3"+
		"\4\20\2\4\6\b\n\f\16\20\22\24\26\30\32\34\2\7\3\2\22\23\3\2\20\21\3\2"+
		"\5\5\3\2\20\23\3\2\7\b\u00a1\2\36\3\2\2\2\4*\3\2\2\2\6:\3\2\2\2\b<\3\2"+
		"\2\2\n>\3\2\2\2\fJ\3\2\2\2\16P\3\2\2\2\20U\3\2\2\2\22[\3\2\2\2\24l\3\2"+
		"\2\2\26n\3\2\2\2\30p\3\2\2\2\32z\3\2\2\2\34\u0098\3\2\2\2\36\37\5\4\3"+
		"\2\37 \7\2\2\3 \3\3\2\2\2!\"\b\3\1\2\"#\7\3\2\2#$\5\4\3\2$%\7\4\2\2%+"+
		"\3\2\2\2&+\5\16\b\2\'+\5\20\t\2(+\5\30\r\2)+\5\6\4\2*!\3\2\2\2*&\3\2\2"+
		"\2*\'\3\2\2\2*(\3\2\2\2*)\3\2\2\2+\64\3\2\2\2,-\f\6\2\2-.\t\2\2\2.\63"+
		"\5\4\3\7/\60\f\5\2\2\60\61\t\3\2\2\61\63\5\4\3\6\62,\3\2\2\2\62/\3\2\2"+
		"\2\63\66\3\2\2\2\64\62\3\2\2\2\64\65\3\2\2\2\65\5\3\2\2\2\66\64\3\2\2"+
		"\2\67;\5\f\7\28;\5\b\5\29;\5\n\6\2:\67\3\2\2\2:8\3\2\2\2:9\3\2\2\2;\7"+
		"\3\2\2\2<=\7\r\2\2=\t\3\2\2\2>D\7\5\2\2?C\n\4\2\2@C\7\13\2\2AC\7\6\2\2"+
		"B?\3\2\2\2B@\3\2\2\2BA\3\2\2\2CF\3\2\2\2DB\3\2\2\2DE\3\2\2\2EG\3\2\2\2"+
		"FD\3\2\2\2GH\7\5\2\2H\13\3\2\2\2IK\7\21\2\2JI\3\2\2\2JK\3\2\2\2KN\3\2"+
		"\2\2LO\7\16\2\2MO\7\17\2\2NL\3\2\2\2NM\3\2\2\2O\r\3\2\2\2PQ\5\22\n\2Q"+
		"R\7\3\2\2RS\5\32\16\2ST\7\4\2\2T\17\3\2\2\2UV\5\22\n\2VW\5\24\13\2WX\5"+
		"\4\3\2X\21\3\2\2\2Y\\\7\r\2\2Z\\\t\5\2\2[Y\3\2\2\2[Z\3\2\2\2\\\23\3\2"+
		"\2\2]_\7\26\2\2^`\5\26\f\2_^\3\2\2\2_`\3\2\2\2`a\3\2\2\2am\7\27\2\2bd"+
		"\7\31\2\2ce\5\26\f\2dc\3\2\2\2de\3\2\2\2ef\3\2\2\2fm\7\30\2\2gi\7\30\2"+
		"\2hj\5\26\f\2ih\3\2\2\2ij\3\2\2\2jk\3\2\2\2km\7\31\2\2l]\3\2\2\2lb\3\2"+
		"\2\2lg\3\2\2\2m\25\3\2\2\2no\7\r\2\2o\27\3\2\2\2pq\t\6\2\2qr\7\r\2\2r"+
		"s\7\25\2\2st\5\4\3\2t\31\3\2\2\2uv\5\4\3\2vw\7\t\2\2wy\3\2\2\2xu\3\2\2"+
		"\2y|\3\2\2\2zx\3\2\2\2z{\3\2\2\2{}\3\2\2\2|z\3\2\2\2}~\5\4\3\2~\33\3\2"+
		"\2\2\177\u0080\7\r\2\2\u0080\u0081\7\3\2\2\u0081\u0082\7\r\2\2\u0082\u0083"+
		"\7\t\2\2\u0083\u0084\7\r\2\2\u0084\u0085\7\t\2\2\u0085\u0086\7\r\2\2\u0086"+
		"\u0087\7\4\2\2\u0087\u0088\7\n\2\2\u0088\u0089\5\4\3\2\u0089\u008a\7\2"+
		"\2\3\u008a\u0099\3\2\2\2\u008b\u008c\7\r\2\2\u008c\u008d\7\3\2\2\u008d"+
		"\u008e\7\r\2\2\u008e\u008f\7\4\2\2\u008f\u0090\7\3\2\2\u0090\u0091\7\r"+
		"\2\2\u0091\u0092\7\t\2\2\u0092\u0093\7\r\2\2\u0093\u0094\7\4\2\2\u0094"+
		"\u0095\7\n\2\2\u0095\u0096\5\4\3\2\u0096\u0097\7\2\2\3\u0097\u0099\3\2"+
		"\2\2\u0098\177\3\2\2\2\u0098\u008b\3\2\2\2\u0099\35\3\2\2\2\21*\62\64"+
		":BDJN[_dilz\u0098";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}