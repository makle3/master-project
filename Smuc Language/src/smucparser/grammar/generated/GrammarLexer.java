// Generated from C:/Users/martin/Documents/Git/master-project/Smuc Language\Grammar.g4 by ANTLR 4.5.1
package smucparser.grammar.generated;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GrammarLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, WS=9, 
		END=10, VARNAME=11, INTEGER=12, DOUBLE=13, PLUS=14, DASH=15, SLASH=16, 
		STAR=17, UNDERSCORE=18, DOT=19, UNDIRECTED_EDGE_START=20, UNDIRECTED_EDGE_END=21, 
		INGOING_EDGE_START=22, OUTGOING_EDGE_START=23;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "WS", 
		"END", "VARNAME", "INTEGER", "DOUBLE", "PLUS", "DASH", "SLASH", "STAR", 
		"UNDERSCORE", "DOT", "UNDIRECTED_EDGE_START", "UNDIRECTED_EDGE_END", "INGOING_EDGE_START", 
		"OUTGOING_EDGE_START"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "')'", "'\"'", "'\\\"'", "'lfp'", "'gfp'", "','", "':='", 
		null, null, null, null, null, "'+'", "'-'", "'/'", "'*'", "'_'", "'.'", 
		"'['", "']'", "'>'", "'<'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, "WS", "END", "VARNAME", 
		"INTEGER", "DOUBLE", "PLUS", "DASH", "SLASH", "STAR", "UNDERSCORE", "DOT", 
		"UNDIRECTED_EDGE_START", "UNDIRECTED_EDGE_END", "INGOING_EDGE_START", 
		"OUTGOING_EDGE_START"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public GrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Grammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\31x\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2\3\2\3"+
		"\3\3\3\3\4\3\4\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\t"+
		"\3\t\3\t\3\n\6\nI\n\n\r\n\16\nJ\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3"+
		"\f\3\f\7\fW\n\f\f\f\16\fZ\13\f\3\r\6\r]\n\r\r\r\16\r^\3\16\3\16\3\16\3"+
		"\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3"+
		"\25\3\26\3\26\3\27\3\27\3\30\3\30\2\2\31\3\3\5\4\7\5\t\6\13\7\r\b\17\t"+
		"\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27"+
		"-\30/\31\3\2\5\5\2\13\f\17\17\"\"\4\2C\\c|\3\2\62;|\2\3\3\2\2\2\2\5\3"+
		"\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2"+
		"\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3"+
		"\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'"+
		"\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\3\61\3\2\2\2\5\63"+
		"\3\2\2\2\7\65\3\2\2\2\t\67\3\2\2\2\13:\3\2\2\2\r>\3\2\2\2\17B\3\2\2\2"+
		"\21D\3\2\2\2\23H\3\2\2\2\25N\3\2\2\2\27R\3\2\2\2\31\\\3\2\2\2\33`\3\2"+
		"\2\2\35d\3\2\2\2\37f\3\2\2\2!h\3\2\2\2#j\3\2\2\2%l\3\2\2\2\'n\3\2\2\2"+
		")p\3\2\2\2+r\3\2\2\2-t\3\2\2\2/v\3\2\2\2\61\62\7*\2\2\62\4\3\2\2\2\63"+
		"\64\7+\2\2\64\6\3\2\2\2\65\66\7$\2\2\66\b\3\2\2\2\678\7^\2\289\7$\2\2"+
		"9\n\3\2\2\2:;\7n\2\2;<\7h\2\2<=\7r\2\2=\f\3\2\2\2>?\7i\2\2?@\7h\2\2@A"+
		"\7r\2\2A\16\3\2\2\2BC\7.\2\2C\20\3\2\2\2DE\7<\2\2EF\7?\2\2F\22\3\2\2\2"+
		"GI\t\2\2\2HG\3\2\2\2IJ\3\2\2\2JH\3\2\2\2JK\3\2\2\2KL\3\2\2\2LM\b\n\2\2"+
		"M\24\3\2\2\2NO\7\2\2\3OP\3\2\2\2PQ\b\13\2\2Q\26\3\2\2\2RX\t\3\2\2SW\t"+
		"\3\2\2TW\5%\23\2UW\5\31\r\2VS\3\2\2\2VT\3\2\2\2VU\3\2\2\2WZ\3\2\2\2XV"+
		"\3\2\2\2XY\3\2\2\2Y\30\3\2\2\2ZX\3\2\2\2[]\t\4\2\2\\[\3\2\2\2]^\3\2\2"+
		"\2^\\\3\2\2\2^_\3\2\2\2_\32\3\2\2\2`a\5\31\r\2ab\5\'\24\2bc\5\31\r\2c"+
		"\34\3\2\2\2de\7-\2\2e\36\3\2\2\2fg\7/\2\2g \3\2\2\2hi\7\61\2\2i\"\3\2"+
		"\2\2jk\7,\2\2k$\3\2\2\2lm\7a\2\2m&\3\2\2\2no\7\60\2\2o(\3\2\2\2pq\7]\2"+
		"\2q*\3\2\2\2rs\7_\2\2s,\3\2\2\2tu\7@\2\2u.\3\2\2\2vw\7>\2\2w\60\3\2\2"+
		"\2\7\2JVX^\3\2\3\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}