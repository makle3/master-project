// Generated from C:/Users/martin/Documents/Git/master-project/Smuc Language\Grammar.g4 by ANTLR 4.5.1
package smucparser.grammar.generated;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GrammarParser#start_rule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart_rule(GrammarParser.Start_ruleContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#psi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPsi(GrammarParser.PsiContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(GrammarParser.ValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(GrammarParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#string}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(GrammarParser.StringContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(GrammarParser.NumberContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(GrammarParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#aggregate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggregate(GrammarParser.AggregateContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#func_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_name(GrammarParser.Func_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#edge_direction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEdge_direction(GrammarParser.Edge_directionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#edge_capability}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEdge_capability(GrammarParser.Edge_capabilityContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#fixpoint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFixpoint(GrammarParser.FixpointContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#function_inner}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_inner(GrammarParser.Function_innerContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#edge_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEdge_function(GrammarParser.Edge_functionContext ctx);
}