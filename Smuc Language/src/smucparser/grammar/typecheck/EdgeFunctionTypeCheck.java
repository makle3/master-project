package smucparser.grammar.typecheck;

import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.simple.specialized.VertexDataType;
import smucparser.grammar.TypeValueAndResult;
import smucparser.grammar.generated.GrammarParser;

import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Logger;

/**
 * Created by Martin on 23-03-2016.
 */
public class EdgeFunctionTypeCheck extends TypeCheck {
    private final HashSet<String> edgeCapabilitiesUsed;
    private String edgeFunctionParameter;
    private Class<? extends AbstractDataType> edgeFunctionResultType;
    private String edgeFunctionTargetName;
    private String edgeFunctionSourceName;
    private Class<? extends AbstractDataType> vertexType;

    public EdgeFunctionTypeCheck(Class<? extends AbstractDataType> dataType, HashSet<String> edgeCapabilitiesUsed) {
        super(dataType);
        this.edgeCapabilitiesUsed = edgeCapabilitiesUsed;
    }

    /**
     * Check the types defined for the given variable, and return it. If a type has not been defined for it,
     * then try to infer it.
     */
    @Override
    public Object visitVariable(GrammarParser.VariableContext ctx) {
        String varname = ctx.getText();

        if(varname.equals(this.edgeFunctionParameter)){
            typesProperty.put(ctx, new TypeValueAndResult(this.edgeFunctionResultType));
            return this.edgeFunctionResultType;
        }
        if(varname.equals(this.edgeFunctionSourceName) || varname.equals(this.edgeFunctionTargetName)){
            typesProperty.put(ctx, new TypeValueAndResult(this.vertexType));
            return this.vertexType;
        }


        return super.visitVariable(ctx);
    }

    @Override
    public Object visitEdge_function(GrammarParser.Edge_functionContext ctx) {
        String functionName = ctx.op.getText();

        if(!edgeCapabilitiesUsed.contains(functionName)){
            return null;
        }

        Class<? extends AbstractDataType> type = types.get(functionName);

        this.edgeFunctionParameter = ctx.parameter.getText();
        this.edgeFunctionResultType = type;
        this.edgeFunctionSourceName = ctx.source.getText();
        this.edgeFunctionTargetName = ctx.target.getText();
        this.vertexType = VertexDataType.class;
        Object ret = super.visit(ctx.psi());

        typesProperty.put(ctx, new TypeValueAndResult((Class<? extends AbstractDataType>) ret));
        return ret;
    }

    public void setTypes(HashMap<String, Class<? extends AbstractDataType>> types) {
        this.types = types;
    }
}
