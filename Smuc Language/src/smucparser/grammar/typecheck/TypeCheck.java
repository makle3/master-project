package smucparser.grammar.typecheck;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.datatypes.simple.specialized.VertexDataType;
import smucparser.grammar.TypeValueAndResult;
import smucparser.grammar.generated.GrammarBaseVisitor;
import smucparser.grammar.generated.GrammarParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Martin on 23-03-2016.
 */
public class TypeCheck extends GrammarBaseVisitor {
    private static final Logger log = Logger.getLogger( TypeCheck.class.getName() );

    static ParseTreeProperty<TypeValueAndResult> typesProperty = new ParseTreeProperty<>();
    private final Class<? extends AbstractDataType> defaultType;
    HashMap<String, Class<? extends AbstractDataType>> types = new HashMap<>();
    private Class<? extends AbstractDataType> inferAs = null;
    private List<Integer> functionDepth = new ArrayList<>();
    private Integer functionDepthIndex = -1;

    public TypeCheck(Class<? extends AbstractDataType> dataType) {
        this.defaultType = dataType;
    }


    /**
     * Used to define types of elements, such as variable names
     * @param name name of thar variable.
     * @param type the type of the variable, extending {@link AbstractDataType}.
     */
    public void addType(String name, Class<? extends AbstractDataType> type){
        if(types.containsKey(name)){
            log.log(Level.INFO, String.format("Replacing type %s with %s (previously %s).", name, SMuC.getTypeName(type), SMuC.getTypeName(types.get(name))));
        }
        types.put(name, type);
    }


    public ParseTreeProperty<TypeValueAndResult> getTypesProperty() {
        return typesProperty;
    }


    @Override
    @SuppressWarnings("unchecked")
    public Object visitStart_rule(GrammarParser.Start_ruleContext ctx) {
        Object type = this.visit(ctx.psi());
        typesProperty.put(ctx, new TypeValueAndResult((Class<? extends AbstractDataType>) type));
        return type;
    }
    @Override
    @SuppressWarnings("unchecked")
    public Object visitPsi(GrammarParser.PsiContext ctx) {
        //If psi_inner exists then we have a parenthesis at hand
        if(ctx.psi_inner!= null){
            Class<? extends AbstractDataType> psiType = (Class<? extends AbstractDataType>) this.visit(ctx.psi_inner);
            typesProperty.put(ctx, new TypeValueAndResult(psiType));

            return psiType;
        }

        //If there's an op (+-*/) then do it manually.
        if(ctx.op != null){
            Class<? extends AbstractDataType> left = (Class<? extends AbstractDataType>) this.visit(ctx.left);
            Class<? extends AbstractDataType> right = (Class<? extends AbstractDataType>) this.visit(ctx.right);
            if(left == null && right == null){
                return null;
            }

            Class<? extends AbstractDataType> prevInferAs = inferAs;

            if(left == null){
                inferAs = right;
                left = (Class<? extends AbstractDataType>) this.visit(ctx.left);
            }
            if(right == null){
                inferAs = left;
                right = (Class<? extends AbstractDataType>) this.visit(ctx.right);
            }
            typesProperty.put(ctx, new TypeValueAndResult(left));

            inferAs = prevInferAs;
            // When using +-/* return the same type for simplicity
            if(left == right){
                return left;
            }
            else {
                throw new ParseCancellationException(String.format("Left and right hand side of %s are different types (%s!=%s)",
                        ctx.getText(),
                        left == null ?"null" : SMuC.getTypeName(left),
                        right == null?"null" : SMuC.getTypeName(right)));
            }
        }
        Class<? extends AbstractDataType> psiType = (Class<? extends AbstractDataType>) super.visitPsi(ctx);

        typesProperty.put(ctx, new TypeValueAndResult(psiType));
        return psiType;
    }

    @Override
    public Object visitFunction(GrammarParser.FunctionContext ctx) {
        functionDepthIndex += 1;
        functionDepth.add(functionDepthIndex, 1);
        @SuppressWarnings("unchecked")
        Class<? extends AbstractDataType>[] inner = (Class<? extends AbstractDataType>[]) this.visit(ctx.function_inner());

        //Check if any of the parameters of a function has a different type than the others (except null)
        Class<? extends AbstractDataType> innerType = null;
        for (int i = 0; i < inner.length; i++) {
            if(inner[i] != null){
                if(innerType==null){
                    innerType = inner[i];
                }
                if(innerType != inner[i]){
                    throw new ParseCancellationException(String.format("Type mismatch in %s where %s(%s) is not of type %s",
                            ctx.getParent().getText(),
                            ctx.inner.psi(i).getText(),
                            SMuC.getTypeName(inner[i]),
                            SMuC.getTypeName(innerType)));
                }
            }
        }
        // Register the type of the inner part of the function
        typesProperty.put(ctx.inner, new TypeValueAndResult(innerType));

        //Iterate over each type of the parameters, and if the type is null, then visit it again, but this time
        // with the global parameter inferAs set to the type of the function.
        Class<? extends AbstractDataType> prevInferAs = inferAs;
        inferAs = innerType;
        functionDepth.set(functionDepthIndex, 2);
        for (int i = 0; i < inner.length; i++) {
            if(inner[i] == null){
                inner[i] = (Class<? extends AbstractDataType>) this.visit(ctx.inner.psi(i));
            }
        }
        inferAs = prevInferAs;


        if(innerType == null){
            log.log(Level.INFO, "visitfunction returned null, going again.");
            functionDepthIndex -= 1;
            return null;
        }

        // Check if the innerType has a function with the given name, and use the returntype of this function as return.
        // If there is no such function, then check the datatype of the outer function (if one exists) for the function.
        Class ret = AbstractDataType.getInstance(innerType).getMethodReturnType(ctx.func_name().getText());
        if(ret == null && prevInferAs != null){
            ret = AbstractDataType.getInstance(prevInferAs).getMethodReturnType(ctx.func_name().getText());
            if(ret == null) {
                throw new ParseCancellationException(String.format("Method %s has not been defined for %s or %s",
                        ctx.func_name().getText(),
                        SMuC.getTypeName(innerType),
                        SMuC.getTypeName(inferAs)));
            }
        }
        else {
            //Check whether the parent function has tried to infer its type yet, if not, and the function is unable  to
            //infer itself, then return null and wait for the parent to try and infer its type.
            if (prevInferAs == null && ret == null) {
                if(functionDepthIndex > 0 && functionDepth.get(functionDepthIndex - 1) == 1) {
                    functionDepthIndex -= 1;
                    return null;
                }
                else {
                    throw new ParseCancellationException(String.format("Method %s has not been defined for %s",
                            ctx.func_name().getText(),
                            SMuC.getTypeName(innerType)));
                }
            }
        }
        typesProperty.put(ctx, new TypeValueAndResult(innerType, ret));

        functionDepthIndex -= 1;
        return ret;
    }



    /**
     * Check if the types of the functions are the same.
     */
    @Override
    @SuppressWarnings("unchecked")
    public Object visitFunction_inner(GrammarParser.Function_innerContext ctx) {
        Class[] types = new Class[ctx.psi().size()];
        int i = 0;
        for (GrammarParser.PsiContext p : ctx.psi()) {
            types[i++] = (Class<? extends AbstractDataType>) this.visit(p);
        }
        return types;
    }


    /**
     * Check the types defined for the given variable, and return it. If a type has not been defined for it,
     * then try to infer it.
     */
    @Override
    public Object visitVariable(GrammarParser.VariableContext ctx) {
        String varname = ctx.getText();

        if(types.size() == 0){
            typesProperty.put(ctx, new TypeValueAndResult(defaultType));
            return defaultType;
        }
        if(types.containsKey(varname)){
            typesProperty.put(ctx, new TypeValueAndResult(types.get(varname)));
            return types.get(varname);
        }
        if(inferAs != null){
            //log.log(Level.INFO, String.format("Guessing %s is of type %s.", varname, SMuC.getTypeName(inferAs)));
            types.put(varname, inferAs);
            typesProperty.put(ctx, new TypeValueAndResult(inferAs));
            return inferAs;
        }
        return null;
    }

    @Override
    public Object visitNumber(GrammarParser.NumberContext ctx) {
        return getLiteralType(ctx);
    }

    @Override
    public Object visitString(GrammarParser.StringContext ctx) {
        return getLiteralType(ctx);
    }

    private Object getLiteralType(ParseTree ctx) {
        if(inferAs != null){
            log.log(Level.INFO, String.format("Guessing %s is of type %s.", ctx.getText(), SMuC.getTypeName(inferAs)));
            typesProperty.put(ctx, new TypeValueAndResult(inferAs));
            return inferAs;
        }
        typesProperty.put(ctx, new TypeValueAndResult(defaultType));
        return defaultType;
    }


    @Override
    public Object visitAggregate(GrammarParser.AggregateContext ctx) {
        @SuppressWarnings("unchecked")
        Class<? extends AbstractDataType> ret = (Class<? extends AbstractDataType>) this.visit(ctx.psi());

        //Set the type of the edge capability
        if(ctx.edge_direction().edge_capability() != null) {
            typesProperty.put(ctx.edge_direction().edge_capability(), new TypeValueAndResult(ret));
            addType(ctx.edge_direction().edge_capability().getText(), ret);
        }
        if(ret != null){
            Class a = AbstractDataType.getInstance(ret).getMethodReturnType(ctx.func_name().getText());

            if(a  != null){
                typesProperty.put(ctx, new TypeValueAndResult(ret,a));
            }
            else{
                typesProperty.put(ctx, new TypeValueAndResult(ret));
            }

            //if(a != ret){//FixMe add exception
            //    log.log(Level.SEVERE, String.format("The type of the input, and the output type of aggregate function %s do not match.", ctx.func_name().getText()));
            //}
        }
        return ret;
    }


    @Override
    public Object visitFixpoint(GrammarParser.FixpointContext ctx) {
        return getFixpointType(ctx, ctx.op.getText(), ctx.psi());
    }



    @SuppressWarnings("unchecked")
    public Object getFixpointType(ParserRuleContext ctx, String op, GrammarParser.PsiContext psi) {
        Class<? extends AbstractDataType> psiType = (Class<? extends AbstractDataType>) this.visit(psi);

        if(psiType == null){
            Class<? extends AbstractDataType> prevInfer = inferAs;
            inferAs = defaultType;
            psiType = (Class<? extends AbstractDataType>) this.visit(psi);
            inferAs = prevInfer;
        }

        checkFormulaVariableType(psiType, op, op, types.get(op), SMuC.getTypeName(psiType));
        typesProperty.put(ctx, new TypeValueAndResult(psiType));
        return psiType;
    }


    private void checkFormulaVariableType(Class<? extends AbstractDataType> psiType, String text, Object... args) {
        if (types.containsKey(text)) {
            if (psiType != types.get(text)) {
                log.log(Level.SEVERE, String.format("The type of formula variable %s has been defined as %s but the resulting value is of type %s", args));
                throw new ParseCancellationException();
            }
        } else {
            types.put(text, psiType);
        }
    }

    public HashMap<String, Class<? extends AbstractDataType>> getTypes() {
        return types;
    }
}
