package smucparser.grammar.visitor;

/**
 * Created by Martin on 18-03-2016.
 */
class FormulaVarName{
    private final String name;

    FormulaVarName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
