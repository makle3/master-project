package smucparser.grammar.visitor;

import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.alg.DirectedNeighborIndex;
import smucparser.TargetOption;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.DataTypeDifferenceNotDefinedException;
import smucparser.grammar.generated.GrammarLexer;
import smucparser.grammar.generated.GrammarParser;
import smucparser.grammar.semantics.VariableType;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by martin on 02-02-2016.
 */
public class SmucVisitor extends AbstractSmucVisitor {
    private static final Logger log = Logger.getLogger( SmucVisitor.class.getName() );
    private final ParseTreeProperty<VariableType> varnames;
    private final HashMap<String, FixedPoint> formulaVarTypes = new HashMap<>();
    private final Graph graph;
    private HashMap<String, Integer> iterations = new HashMap<>();
    private final DirectedNeighborIndex<MyVertex, AbstractDataType> neighborIndex;
    private Long maxIterations = 0L;
    private TargetOption targetOption = TargetOption.EQUALS;
    private Double epsilon;
    private ParseTreeProperty<Integer> nestedLevels;

    public Graph getGraph() {
        return graph;
    }

    public void setMaxIterations(Long maxIterations) {
        this.maxIterations = maxIterations;
    }

    public void setTargetOption(TargetOption targetOption) {
        this.targetOption = targetOption;
    }

    public void setEpsilon(Double epsilon) {
        this.epsilon = epsilon;
    }

    public void setNestedLevel(ParseTreeProperty<Integer> nestedParent) {
        this.nestedLevels = nestedParent;
    }


    private enum FixedPoint{
        GREATEST_FIXED_POINT, LEAST_FIXED_POINT
    }

    public SmucVisitor(AbstractDataType dataType, Graph g, ParseTreeProperty<VariableType> varnames){
        super(dataType, (MyVertex) g.vertexSet().iterator().next());
        this.graph = g;
        this.neighborIndex = new DirectedNeighborIndex((DirectedGraph) g);
        this.varnames = varnames;
    }

    /**
     * Perform a function on the results of the right hand side of the query of neighboring nodes
     * (ingoing/outgoing/both depending on query)
     * It is assumed that the function used here is associative and commutative
     */
    @Override
    public AbstractDataType visitAggregate(GrammarParser.AggregateContext ctx) {
        int edgeDirection = ctx.edge_direction().dir.getType();
        //Gets the nodes which we want to calculate on
        List<MyVertex> neighborList = (List<MyVertex>) this.visit(ctx.direction);


        //Remember the node we started on
        MyVertex startVertex = this.getCurrentVertex();
        List<AbstractDataType> nodeResults = new ArrayList<>();

        //Iterate through the neighbors and evaluate the results of the right hand side of the query
        for (MyVertex neighbor : neighborList) {
            //First we want to evaluate the right hand side of the aggregate rule, from the perspective of the given node
            this.setCurrentVertex(neighbor);
            AbstractDataType psiResult = visitPsi(ctx.right);

            MyEdge edge = null;
            MyEdge edge2 = null;
            switch (edgeDirection){
                case GrammarLexer.INGOING_EDGE_START:
                    edge = (MyEdge) getGraph().getEdge(neighbor, startVertex);
                    break;
                case GrammarLexer.OUTGOING_EDGE_START:
                    edge = (MyEdge) getGraph().getEdge(startVertex, neighbor);
                    break;
                case GrammarLexer.UNDIRECTED_EDGE_START:
                    edge = (MyEdge) getGraph().getEdge(neighbor, startVertex);
                    edge2 = (MyEdge) getGraph().getEdge(startVertex, neighbor);
                    break;
            }

            if(edge != null) {
                AbstractDataType edgeResult = edge.doCapability(getDataType(ctx), ctx.edge_direction().edge_capability(), psiResult, types);
                nodeResults.add(edgeResult);
            }

            if(edge2 != null){
                AbstractDataType edgeResult2 = edge2.doCapability(getDataType(ctx), ctx.edge_direction().edge_capability(), psiResult, types);
                nodeResults.add(edgeResult2);
            }
        }
        this.setCurrentVertex(startVertex);

        return this.performFunc(ctx.op.getText(), nodeResults, types.get(ctx).getAbstractValueType(), types.get(ctx).getAbstractResultType());
    }


    @Override
    public List<MyVertex> visitEdge_direction(GrammarParser.Edge_directionContext ctx) {
        if(ctx.dir.getType() == GrammarLexer.INGOING_EDGE_START){
            return neighborIndex.predecessorListOf(this.getCurrentVertex());
        }
        if(ctx.dir.getType() == GrammarLexer.OUTGOING_EDGE_START) {
            return neighborIndex.successorListOf(this.getCurrentVertex());
        }
        Set<MyVertex> set = new HashSet<>();
        List<MyVertex> neighbors1 = neighborIndex.predecessorListOf(this.getCurrentVertex());
        List<MyVertex> neighbors2 = neighborIndex.successorListOf(this.getCurrentVertex());

        set.addAll(neighbors1);
        set.addAll(neighbors2);

        return new ArrayList<>(set);
    }
    private HashMap<String, Boolean> doneNestedFixpoint = new HashMap<>();
    @Override
    public Object visitFixpoint(GrammarParser.FixpointContext ctx) {
        String formulaVar = ctx.op.getText();
        FixedPoint fp = FixedPoint.GREATEST_FIXED_POINT;
        if(Objects.equals(ctx.fp.getText(), "lfp")){
            fp = FixedPoint.LEAST_FIXED_POINT;
        }
        formulaVarTypes.put(formulaVar, fp);
        try {
            findFixedPoint(formulaVar, fp, ctx);
        } catch (DataTypeDifferenceNotDefinedException e) {
            e.printStackTrace();
        }
        return new FormulaVarName(formulaVar);
    }

    @Override
    public AbstractDataType visitPsi(GrammarParser.PsiContext ctx){
        Object value = super.visitPsi(ctx);
        if(value instanceof FormulaVarName){
            value = getCurrentVertex().getFormulaVariable(((FormulaVarName) value).getName());
        }
        return (AbstractDataType) value;
    }


    /**
     * Find a fixpoint of a function on the graph, ie. a point where applying the function another iteration
     * does not change the graph.
     */
    private void findFixedPoint(String formulaVar, FixedPoint fixedPoint, GrammarParser.FixpointContext ctx) throws DataTypeDifferenceNotDefinedException {
        MyVertex startVertex = this.getCurrentVertex();

        boolean changed = true;
        if(!iterations.containsKey(formulaVar)){
            iterations.put(formulaVar, 0);
            if(this.getCurrentVertex().getFormulaVariable(formulaVar) == null) {
                for(Object o : getGraph().vertexSet()) {
                    MyVertex v = (MyVertex) o;
                    AbstractDataType type = getDataType(ctx);
                    AbstractDataType val = type.newInstance();
                    if (fixedPoint == FixedPoint.GREATEST_FIXED_POINT) {
                        val.setValue(type.getTop());
                    } else {
                        val.setValue(type.getBottom());
                    }
                    v.addFormulaVariableIteration(formulaVar, 0, val);

                }
            }
        }


        // Continue until there are no more changes in the graph.
        while(changed && (iterations.get(formulaVar) < this.maxIterations || this.maxIterations == 0)) {
            if(this.nestedLevels != null) {
                if (this.nestedLevels.get(ctx) > 0) {
                    if (this.doneNestedFixpoint.containsKey(formulaVar)) {
                        if (this.doneNestedFixpoint.get(formulaVar)) {
                            return;
                        }
                    }
                } else {
                    for (Map.Entry<String, Boolean> e : doneNestedFixpoint.entrySet()) {
                        e.setValue(false);
                    }
                }
            }

            int iteration = iterations.get(formulaVar);

            //System.out.printf("Doing iteration %d for %s.%n", iteration, formulaVar);
            changed = false;
            //Iterate over the graph
            for (Object o : getGraph().vertexSet()) {
                MyVertex v = (MyVertex)o;

                this.setCurrentVertex(v);
                AbstractDataType value = visitPsi(ctx.psi());
                v.addFormulaVariableIteration(formulaVar, iteration + 1, value);

                if (this.targetOption == TargetOption.EQUALS) {
                    if (v.hasChanged(formulaVar)) {
                        changed = true;
                    }
                } else if (this.targetOption == TargetOption.EPSILON) {
                    if (v.difference(formulaVar, this.epsilon)) {
                        changed = true;
                    }
                }
            }
            iterations.put(formulaVar, iteration+1);
            if(!changed){
                this.doneNestedFixpoint.put(formulaVar, true);
            }
        }
        this.setCurrentVertex(startVertex);
    }

    @Override
    public AbstractDataType visitVariable(GrammarParser.VariableContext ctx){
        String nodeName = ctx.getText();
        VariableType type = varnames.get(ctx);
        switch (type){
            case NODE_ATTRIBUTE:
                return this.getCurrentVertex().getAttribute(nodeName);
            case FORMULA_VARIABLE:
                AbstractDataType value = this.getCurrentVertex().getFormulaVariable(nodeName,this.iterations.get(nodeName));
                if(value != null){
                    return value;
                }
                else{
                    log.log(Level.WARNING,"No formula variable found for {0}.", nodeName);
                }
                break;
            case FORMULA_VARIABLE_PREVIOUS_QUERY:
                return this.getCurrentVertex().getFormulaVariable(nodeName);
        }
        return null;
    }


    @Override
    public AbstractDataType visitTerminal(TerminalNode node) {
        return null;
    }


}
