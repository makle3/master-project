package smucparser.grammar.visitor;

import org.antlr.v4.runtime.tree.ParseTreeProperty;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.datatypes.simple.specialized.VertexDataType;
import smucparser.grammar.generated.GrammarParser;
import smucparser.graph.MyVertex;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Martin on 27-02-2016.
 */
public class EdgeFunctionVisitor extends AbstractSmucVisitor {
    private MyVertex source;
    private MyVertex target;
    private final AbstractDataType psiInput;
    private final Map<String, AbstractDataType> edgeItems;
    private String parameter;
    private String targetName;
    private String sourceName;
    private VertexDataType vertexDataType = (VertexDataType) AbstractDataType.getInstance(VertexDataType.class);

    public EdgeFunctionVisitor(AbstractDataType dataType, MyVertex source, MyVertex target, Map<String, AbstractDataType> edgeItems, AbstractDataType psiResult) {
        super(dataType,source);
        this.source = source;
        this.target = target;
        this.psiInput = psiResult;
        this.edgeItems = edgeItems;
    }
    @Override
    public AbstractDataType visitEdge_function(GrammarParser.Edge_functionContext ctx) {
        this.parameter = ctx.parameter.getText();
        sourceName = ctx.source.getText();
        targetName = ctx.target.getText();
        Object test = this.visit(ctx.psi());
        return (AbstractDataType) test;
    }

    @Override
    public AbstractDataType visitVariable(GrammarParser.VariableContext ctx) {
        // We want to have access to the vertex in edge functions. But not items in the vertex
        // Vertex ID is stored as a long
        AbstractDataType dt = this.vertexDataType.newInstance();
        if(ctx.VARNAME().getText().equals(sourceName)){
            dt.setValue(source.getId());
            return dt;
        }
        else if(ctx.VARNAME().getText().equals(targetName)){
            dt.setValue(target.getId());
            return dt;
        }

        if(ctx.VARNAME().getText().equals(parameter)){
            return psiInput;
        }
        if(edgeItems.containsKey(ctx.VARNAME().getText())){
            return edgeItems.get(ctx.VARNAME().getText());
        }
        throw new RuntimeException(String.format("%s is not defined as either a parameter of the edge function, or a value of the edge.", ctx.VARNAME().getText()));
    }
}
