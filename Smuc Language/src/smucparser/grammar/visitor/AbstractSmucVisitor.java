package smucparser.grammar.visitor;

import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.grammar.TypeValueAndResult;
import smucparser.grammar.generated.GrammarBaseVisitor;
import smucparser.grammar.generated.GrammarParser;
import smucparser.graph.MyVertex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by martin on 02-02-2016.
 */
public abstract class AbstractSmucVisitor extends GrammarBaseVisitor {
    private AbstractDataType overallType;
    protected ParseTreeProperty<TypeValueAndResult> types = new ParseTreeProperty<>();
    private MyVertex currentVertex;

    public void setTypes(ParseTreeProperty<TypeValueAndResult> t){
        this.types = t;
    }

    public AbstractSmucVisitor(AbstractDataType dataType, MyVertex v) {
        currentVertex = v;
        overallType = dataType;
    }

    @Override
    public AbstractDataType visitStart_rule(GrammarParser.Start_ruleContext ctx) {
        return (AbstractDataType) visitPsi(ctx.psi());
    }

    @Override
    public AbstractDataType visitFunction(GrammarParser.FunctionContext ctx) {
        List<AbstractDataType> children = this.visitFunction_inner(ctx.inner);
        return performFunc(ctx.op.getText(), children, types.get(ctx).getAbstractValueType(), types.get(ctx).getAbstractResultType());
    }

    /**
     * Perform the function defined in the DataType on the list of nodes given.
     * @param functionName Name of the function as a string.
     * @param nodes The nodes to perform function on.
     * @return The result of the function in the current node.
     */
    AbstractDataType performFunc(String functionName, List<AbstractDataType> nodes,
                                 Class<? extends AbstractDataType> dataType,
                                 Class<? extends AbstractDataType> resultType) {
        AbstractDataType type = AbstractDataType.getInstance(dataType);
        AbstractDataType resType = AbstractDataType.getInstance(resultType);


        Object[] values = new Object[nodes.size()];
        for (int i = 0; i < nodes.size(); i++) {
            values[i] = nodes.get(i).getValue();
        }
        //todo Add more checking of functions when checking that they exists. Like method signature and stuff
        Object value;
        if(type.checkForFunction(functionName)) {
            value = type.function(functionName, getCurrentVertex(), values);
        }
        else{
            value = resType.function(functionName, getCurrentVertex(), values);
        }
        AbstractDataType dc = resType.newInstance();
        dc.setValue(value);
        return dc;
    }


    @Override
    public Object visitPsi(GrammarParser.PsiContext ctx){
        if(ctx.op != null){
            AbstractDataType left = (AbstractDataType) this.visitPsi(ctx.left);
            AbstractDataType right = (AbstractDataType) this.visitPsi(ctx.right);
            //Throw error if left and right are different classes
            if(!left.getClass().isAssignableFrom(right.getClass())){
                throw new ParseCancellationException("Left and right hand side of " + ctx.op.getText() + " needs to be of the same type.");//FixMe Add more information
            }

            AbstractDataType dc = getDataType(ctx);
            dc.setValue(dc.function(ctx.op.getText(), getCurrentVertex(), left.getValue(), right.getValue()));
            return  dc;
        }
        else if(ctx.psi_inner != null){
            return this.visitPsi(ctx.psi_inner);
        }
        return  visitChildren(ctx);
    }

    @Override
    public List<AbstractDataType> visitFunction_inner(GrammarParser.Function_innerContext ctx) {
        List<AbstractDataType> result = new ArrayList<>();
        for(GrammarParser.PsiContext p : ctx.psi()){
            result.add((AbstractDataType) p.accept(this));
        }
        return result;
    }

    @Override
    public AbstractDataType visitNumber(GrammarParser.NumberContext ctx) {
        AbstractDataType type = getDataType(ctx);

        type.setValue(type.interpret(ctx.DASH() != null ? ctx.DASH().getText()+ctx.num.getText():ctx.num.getText(), getCurrentVertex().getId()));
        return type;
    }

    @Override
    public Object visitString(GrammarParser.StringContext ctx) {
        String s = ctx.getStart().getTokenSource().getInputStream().getText(new Interval(ctx.getStart().getStartIndex(), ctx.getStop().getStopIndex()));
        s = s.substring(1, s.length()-1); //Remove ""
        AbstractDataType type = getDataType(ctx);

        type.setValue(type.interpret(s, getCurrentVertex().getId()));
        return type;
    }

    public MyVertex getCurrentVertex() {
        return currentVertex;
    }

    public void setCurrentVertex(MyVertex currentVertex) {
        this.currentVertex = currentVertex;
    }



    AbstractDataType getDataType(ParseTree ctx) {
        AbstractDataType type = null;
        try {
            TypeValueAndResult dt = types.get(ctx);
            if(dt != null) {
                type = AbstractDataType.newInstance(dt.getAbstractValueType());
            }
        } catch (DataTypeException e) {
            throw new ParseCancellationException(e);
        }
        if(type == null){
            type = overallType.newInstance();
        }
        return type;
    }
}
