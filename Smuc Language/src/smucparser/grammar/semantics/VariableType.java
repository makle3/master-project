package smucparser.grammar.semantics;

/**
 * Created by martin on 01-06-2016.
 */
public enum VariableType {
    NODE_ATTRIBUTE, FORMULA_VARIABLE, EDGE_CAPABILITY, FORMULA_VARIABLE_PREVIOUS_QUERY
}