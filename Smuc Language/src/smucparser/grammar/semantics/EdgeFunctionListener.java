package smucparser.grammar.semantics;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import smucparser.grammar.generated.GrammarBaseListener;
import smucparser.grammar.generated.GrammarParser;

import java.util.HashMap;

/**
 * Created by Martin on 27-02-2016.
 */
public class EdgeFunctionListener extends GrammarBaseListener  {
    private enum varTypes{
        VARIABLE, PARAMETER, FUNCTION_NAME
    }

    public String functionName;
    private String parname;
    private HashMap variables = new HashMap();

    @Override
    public void enterStart_rule(GrammarParser.Start_ruleContext ctx) { throw new ParseCancellationException(); }

    @Override
    public void enterPsi(GrammarParser.PsiContext ctx) { }
    @Override
    public void enterVariable(GrammarParser.VariableContext ctx) {
        String name = ctx.VARNAME().getText();


        if(!variables.containsKey(name)){
            variables.put(name, varTypes.VARIABLE);
        }
        else{
            if(        variables.get(name) != varTypes.VARIABLE
                    && variables.get(name) != varTypes.PARAMETER){
                throw new ParseCancellationException();//varname is not a variable
            }
        }
    }

    @Override
    public void enterNumber(GrammarParser.NumberContext ctx) { }
    @Override
    public void enterFunction(GrammarParser.FunctionContext ctx) {
        String fName = ctx.func_name().getText();
        if(fName.equals(functionName)){
            throw new ParseCancellationException();//No recursion
        }
        if(variables.containsKey(fName)){
            if(variables.get(fName) != varTypes.FUNCTION_NAME){
                throw new ParseCancellationException(); //function name already used by something that is not a function
            }
        }
        variables.put(fName, varTypes.FUNCTION_NAME);
    }
    @Override
    public void enterAggregate(GrammarParser.AggregateContext ctx) { throw new ParseCancellationException(); }
    @Override
    public void enterEdge_direction(GrammarParser.Edge_directionContext ctx) { throw new ParseCancellationException(); }
    @Override
    public void enterEdge_capability(GrammarParser.Edge_capabilityContext ctx) { throw new ParseCancellationException(); }

    @Override
    public void enterFixpoint(GrammarParser.FixpointContext ctx) {
        throw new ParseCancellationException("Fixed points can not be used in edge capabilities.");
    }

    @Override
    public void enterFunction_inner(GrammarParser.Function_innerContext ctx) { }
    @Override
    public void enterEdge_function(GrammarParser.Edge_functionContext ctx) {
        functionName = ctx.op.getText();
        parname = ctx.parameter.getText();
        variables.put(parname, varTypes.PARAMETER);
    }
    @Override
    public void exitEdge_function(GrammarParser.Edge_functionContext ctx) {}


    @Override
    public void enterEveryRule(ParserRuleContext ctx) { }
    @Override
    public void visitTerminal(TerminalNode node) { }
    @Override
    public void visitErrorNode(ErrorNode node) { }

}
