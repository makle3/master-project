package smucparser.grammar.semantics;

import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.jgrapht.Graph;
import smucparser.grammar.generated.GrammarBaseListener;
import smucparser.grammar.generated.GrammarParser;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import static smucparser.grammar.semantics.VariableType.*;

/**
 * Created by Martin on 01-02-2016.
 */
public class SmucListener extends GrammarBaseListener {
    private static ParseTreeProperty<Integer> nestedLevelProperty = new ParseTreeProperty<>();


    private static final Logger log = Logger.getLogger(SmucListener.class.getName());
    private Integer nestedLevel = 0;
    private HashSet<String> edgeCapabilitiesUsed = new HashSet<>();
    private final Graph graph;
    private ParseTreeProperty<VariableType> varnames = new ParseTreeProperty<>();
    private HashMap<String, ParseTree> variables = new HashMap();
    private HashSet<ParseTree> contextSet = new HashSet<>();


    public SmucListener(Graph g) {
        this.graph = g;
    }

    public HashSet<String> getEdgeCapabilitiesUsed() {
        return edgeCapabilitiesUsed;
    }

    public ParseTreeProperty<VariableType> getVarnames() {
        return varnames;
    }


    /**
     * All the tokens registered in the other methods are iterated over,
     */
    @Override
    public void exitStart_rule(GrammarParser.Start_ruleContext ctx) {
        HashSet<ParseTree> variables = new HashSet<>();
        for (ParseTree tree : contextSet) {
            if (varnames.get(tree) == FORMULA_VARIABLE || varnames.get(tree) == NODE_ATTRIBUTE) {
                variables.add(tree);
            }
            else if (varnames.get(tree) == EDGE_CAPABILITY) {
                checkEdgeCapability(tree.getText());
            }
        }

        try {
            checkNodeAtrributesAndVariables(variables);
        } catch (ParseCancellationException ex) {
            log.log(Level.SEVERE, ex.toString());
            throw ex;
        }

        //System.out.println("Ending Start Rule");
    }

    /**
     * Checks that each edge in the graph contains the required edge capability.
     * @param edgeCapability String name of the edge capability.
     * @throws ParseCancellationException If an edge is missing an edge capability.
     */
    private void checkEdgeCapability(String edgeCapability) throws ParseCancellationException {
        if (edgeCapability != null) {
            int numOfEdges = this.graph.edgeSet().size();
            int edgesWithCapability = numOfEdges;
            for (Object e : this.graph.edgeSet()) {
                MyEdge edge = (MyEdge) e;

                // In the case that the edge capability is not defined for an edge, we will use the identity capability
                if (!edge.hasEdgeCapability(edgeCapability)) {
                    edge.addIdentityCapability(edgeCapability);
                    edgesWithCapability--;
                }
            }
            if(edgesWithCapability == 0){
                MessageFormat fmt = new MessageFormat("No edges have edge capability {0} defined.");
                throw new ParseCancellationException(fmt.format(new Object[]{edgeCapability}));
            }
            if(edgesWithCapability != numOfEdges){
                MessageFormat fmt = new MessageFormat("Some edges do not have edge capability {0} defined ({1} of {2} have), those are using identity instead.");
                log.log(Level.INFO, fmt.format(new Object[]{edgeCapability, edgesWithCapability, numOfEdges}));
            }
        }
    }

    /**
     * Iterates over the graph the object is initialized with, and checks that all nodes contain the elements
     * used in the query.
     * @param variables A set containing the variables used in the query.
     * @throws ParseCancellationException If a vertex is missing a variable, or a variable is used as a
     * formula variable.
     */
    private void checkNodeAtrributesAndVariables(HashSet<ParseTree> variables) throws ParseCancellationException {
        for (Object o : graph.vertexSet()) {
            MyVertex v = (MyVertex) o;
            for (ParseTree tree : variables) {
                String tokenText = tree.getText();

                if (varnames.get(tree) == FORMULA_VARIABLE) {
                    if (v.getAttributeValue(tokenText) != null) {
                        //Make sure that the node doesn't have an item with the same name as the formula variable
                        //v.addFormulaVariableIteration(tokenText, 0, v.getAttributeValue(tokenText));
                        v.addFormulaVariableAlias(tokenText);

                        log.log(Level.WARNING, "Vertex {0} contains {1} \"{2}\" while it is used as a formula variable.",
                                new Object[]{v.getId(), varnames.get(tree), tokenText});
                    }
                } else if (varnames.get(tree) == NODE_ATTRIBUTE) {
                    if (v.getAttributeValue(tokenText) == null) {
                        if (v.getFormulaVariable(tokenText) != null) {
                            varnames.put(tree, FORMULA_VARIABLE_PREVIOUS_QUERY);
                        } else {
                            MessageFormat fmt = new MessageFormat("Vertex {0} is missing {1} \"{2}\".");
                            throw new ParseCancellationException(fmt.format(new Object[]{v.getId(), varnames.get(tree), tokenText}));
                        }
                    }
                }
            }
        }
    }

    /**
     * Register token as a fixpoint.
     * As fixpoints can be nested, it is required to know at what level the nested fixpoint is. This is used during
     * execution of the query in the {@link smucparser.grammar.visitor.SmucVisitor}.
     */
    @Override
    public void enterFixpoint(GrammarParser.FixpointContext ctx) {
        registerContext(ctx, FORMULA_VARIABLE);
        nestedLevelProperty.put(ctx, this.nestedLevel);
        variables.put(ctx.op.getText(), ctx);
        this.nestedLevel += 1;
    }

    @Override
    public void exitFixpoint(GrammarParser.FixpointContext ctx) {
        this.nestedLevel -= 1;
    }

    /**
     * Register token as a node label, unless it has already been defined, then it is set to be the type as defined.
     */
    @Override
    public void enterVariable(GrammarParser.VariableContext ctx) {
        if (varnames.get(ctx) == null) {
            if(variables.containsKey(ctx.getText())){
                registerContext(ctx, varnames.get(variables.get(ctx.getText())));
            }
            else{
                registerContext(ctx, NODE_ATTRIBUTE);
                variables.put(ctx.getText(), ctx);
            }
        }
        registerContext(ctx, varnames.get(variables.get(ctx.getText())));
    }

    /**
     * Register token as an edge capability.
     */
    @Override
    public void enterEdge_capability(GrammarParser.Edge_capabilityContext ctx) {
        if (variables.containsKey(ctx.getText())) {
            if (varnames.get(variables.get(ctx.getText())) != EDGE_CAPABILITY) {
                throw new ParseCancellationException(ctx.getText() + " has already been defined as " + varnames.get(ctx));
            }
        }
        registerContext(ctx, EDGE_CAPABILITY);
        edgeCapabilitiesUsed.add(ctx.capability.getText());
    }

    @Override
    public void visitErrorNode(ErrorNode errorNode) {
        log.log(Level.SEVERE, errorNode.toString());
        throw new ParseCancellationException(errorNode.toString());
    }

    private void registerContext(ParseTree ctx, VariableType type) {
        varnames.put(ctx, type);
        contextSet.add(ctx);
    }

    public static ParseTreeProperty<Integer> getNestedLevelProperty() {
        return nestedLevelProperty;
    }
}
