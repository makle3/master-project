package smucparser.graph;

import com.google.gson.annotations.SerializedName;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.DataTypeDifferenceNotDefinedException;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Martin on 03-02-2016.
 */
public class MyVertex{
    @SerializedName("id")
    private Long id;
    @SerializedName("attribute")
    private TreeMap<String, AbstractDataType> attribute = new TreeMap<>();
    @SerializedName("formulaVariables")
    private HashMap<String, AbstractDataType> formulaVariables = new HashMap<>();
    private transient HashMap<String, AbstractDataType> prevFormulaVariables = new HashMap<>();
    private transient HashMap<String, Integer> iterations = new HashMap<>();
    @SerializedName("formulaVariableAlias")
    private HashMap<String, String> formulaVariableAlias = new HashMap<>();
    private static Long counter = 0L;
    private static transient final Logger log = Logger.getLogger( MyVertex.class.getName() );

    public List<String> getDoNotPrint() {
        return doNotPrint;
    }

    private transient final List<String> doNotPrint = new ArrayList<>();


    public MyVertex(){
        this.id = counter++;
    }

    public MyVertex(Long id){
        this.id = id;
        if(counter <= id){
            counter = id+1;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
        if(id > counter){
            counter = id + 1;
        }
        else {
            counter++;
        }
    }

    public AbstractDataType addAttribute(String key, AbstractDataType value){
        return this.attribute.put(key,value);
    }

    void addAttributeMap(Map<String, AbstractDataType> map){
        attribute.putAll(map);
    }

    public Object getAttributeValue(String key){
        AbstractDataType attribute = this.getAttribute(key);
        return attribute==null ? null : attribute.getValue();
    }
    public AbstractDataType getAttribute(String key){
        return attribute.get(key);
    }

    public Map<String,AbstractDataType> getAttributes(){
        return this.attribute;
    }

    public void addFormulaVariableAlias(String tokenText) {
        int addition = 0;
        while(this.attribute.containsKey(tokenText + addition) || this.formulaVariableAlias.containsKey(tokenText + addition)){
            addition++;
        }
        formulaVariableAlias.put(tokenText, tokenText + addition);
    }

    public void addFormulaVariableIteration(String variable, int iteration, AbstractDataType value){
        if(formulaVariableAlias.containsKey(variable)){
            variable = formulaVariableAlias.get(variable);
        }
        iterations.put(variable, iteration);
        if(formulaVariables.containsKey(variable)){
            prevFormulaVariables.put(variable,formulaVariables.put(variable,value));
        }
        else{
            formulaVariables.put(variable,value);
            prevFormulaVariables.put(variable, value);
        }
    }


    public AbstractDataType getFormulaVariable(String variable, int iteration){
        if(formulaVariableAlias.containsKey(variable)){
            variable = formulaVariableAlias.get(variable);
        }
        if(!formulaVariables.containsKey(variable)){
            return null;
        }
        if(iteration == iterations.get(variable)) {
            return formulaVariables.get(variable);
        }
        else{
            return prevFormulaVariables.get(variable);
        }
    }

    public AbstractDataType getFormulaVariable(String variable){
        if(formulaVariableAlias.containsKey(variable)){
            variable = formulaVariableAlias.get(variable);
        }
        if(!formulaVariables.containsKey(variable)){
            return null;
        }
        return formulaVariables.get(variable);
    }

    public Map<String, AbstractDataType> getFormulaVariableTypes(){
        Map<String, AbstractDataType> ret = new HashMap<>();
        for(String key: formulaVariables.keySet()){
            ret.put(key, this.getFormulaVariable(key));
        }
        return ret;
    }



    /**
     * Check if a given formula variable has changed in the last two iterations.

     * @param formulaVar Name of the formula variable to check.
     * @return True if the value has changed, or there is no value.
     */
    public boolean hasChanged(String formulaVar) {
        if (formulaVariableAlias.containsKey(formulaVar)) {
            formulaVar = formulaVariableAlias.get(formulaVar);
        }

        AbstractDataType first = formulaVariables.get(formulaVar);
        if (!prevFormulaVariables.containsKey(formulaVar)) {
            return true;
        }
        AbstractDataType second = prevFormulaVariables.get(formulaVar);

        if(first == null || second == null || first.getValue() == null || second.getValue() == null){
            return false;
        }

        return !first.equals(second) || first.getValue().getClass() != second.getValue().getClass();

    }

    public boolean difference(String formulaVar, Double epsilon) throws DataTypeDifferenceNotDefinedException {
        if(formulaVariableAlias.containsKey(formulaVar)){
            formulaVar = formulaVariableAlias.get(formulaVar);
        }

        AbstractDataType first = formulaVariables.get(formulaVar);
        AbstractDataType second = prevFormulaVariables.get(formulaVar);

        // In case we use GFP/LFP with a different default class than expected.
        if(!first.getValue().getClass().equals(second.getValue().getClass())){
            second = first;
            prevFormulaVariables.put(formulaVar, first);
        }

        double difference = Math.abs(first.getDifference(first.getValue(), second.getValue()));
        return difference > epsilon;
    }

    //@Override
    public String toDot(){
        return toDot(true, true, true);
    }

    public String toDot(boolean printAttributes, boolean printFormulaVariables){
        return toDot(true, printAttributes, printFormulaVariables);
    }

    public String toDot(boolean compact, boolean printAttributes, boolean printFormulaVariables){
        StringBuilder sb = new StringBuilder();
        String name = "" + this.getId();
        sb.append("Node: ").append(name).append("\\n");
        //public HashMap<String,T>
        if(printAttributes) {
            for (Map.Entry<String, AbstractDataType> entry : attribute.entrySet()) {
                if(doNotPrint.contains(entry.getKey())){
                    continue;
                }
                sb.append(entry.getKey())
                        .append(": ")
                        .append(printT(entry.getValue()))
                        .append("\\n");

            }
        }
        if(printFormulaVariables) {
            if (compact) {
                for (Map.Entry<String, AbstractDataType> entry : formulaVariables.entrySet()) {
                    if(doNotPrint.contains(entry.getKey())){
                        continue;
                    }
                    AbstractDataType value = getFormulaVariable(entry.getKey());
                    sb.append(entry.getKey())
                            .append(": ")
                            .append(printT(value))
                            .append("\\n");
                }
            } else {
                for (Map.Entry<String, AbstractDataType> entry : formulaVariables.entrySet()) {
                    if(doNotPrint.contains(entry.getKey())){
                        continue;
                    }
                    sb.append(entry.getKey())
                            .append(": ")
                            .append("")
                            .append("\\n");
                }
            }
        }
        return sb.toString();
    }

    private String printT(AbstractDataType value){
        if(value.getValue() == null){
            return "null";
        }
        //if(value.equals(value.getTop())){
        //    return value.toString();//"Top";
        //}
        //else if(value.equals(value.getBottom())){
        //    return value.toString();//"Bot";
        //}
        else {
            return value.getValue().toString();
        }
    }

}
