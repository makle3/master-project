package smucparser.graph;

import org.jgrapht.Graph;
import org.jgrapht.VertexFactory;
import org.jgrapht.generate.GraphGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Martin on 16-02-2016.
 */
public class MyGraphGenerator<V, E, T> implements GraphGenerator<V, E, T> {
    private int init_num_nodes;
    private int edgesToAdd;
    private int numNodes;
    private Random random = new Random();

    private Map defaultNode;


    public MyGraphGenerator(int init_num_nodes, int edgesToAdd, int numNodes, Map defaultNode){
        this.init_num_nodes = init_num_nodes;
        this.edgesToAdd = edgesToAdd;
        this.numNodes = numNodes;
        this.defaultNode = defaultNode;
    }

    @Override
    public void generateGraph(Graph<V, E> graph, VertexFactory<V> vertexFactory, Map<String, T> map) {
        // Create N nodes
        List<V> nodes = new ArrayList<>();

        //Keep track of the degree of each node
        int degrees[] = new int[numNodes];

        // For each node
        for (int i = 0; i < numNodes; i++)
        {
            // Save node in array
            nodes.add(vertexFactory.createVertex());
            if(nodes.get(i) instanceof MyVertex & map != null) {
                ((MyVertex) nodes.get(i)).addAttributeMap(defaultNode);
            }
            graph.addVertex(nodes.get(i));
        }

        //set the number of edges to zero
        int numEdges = 0;

        //Set up the initial  complete seed network
        for (int i = 0; i < init_num_nodes; i++)
        {
            for (int j = (i + 1); j < init_num_nodes; j++)
            {

                //Create the new edge
                graph.addEdge(nodes.get(i),nodes.get(j));

                //increment the degrees for each nodes
                degrees[i]++;
                degrees[j]++;

                //Increment the edges
                numEdges++;
            }
        }


        //Add each node one at a time
        for (int i = init_num_nodes; i < numNodes; i++)
        {
            int added = 0;
            double degreeIgnore = 0;
            //Add the appropriate number of edges
            for (int m = 0; m < edgesToAdd; m++)
            {
                //keep a running talley of the probability
                double prob = 0;
                //Choose a random number
                double randNum = random.nextDouble();



                //Try to add this node to every existing node
                for (int j = 0; j < i; j++)
                {
                    //Check for an existing connection between these two nodes
                    if(!graph.containsEdge(nodes.get(i),nodes.get(j)))
                    {
                        //Increment the talley by the jth node's probability
                        prob += degrees[j]
                                / (2.0d * numEdges - degreeIgnore);
                    }

                    //System.out.println(m + "\t"  + j +"\t" + prob + " < " + randNum  + "-" + degreeIgnore );


                    //If this pushes us past the the probability
                    if (randNum <= prob)
                    {
                        // Create an edge between node i and node j
                        graph.addEdge(nodes.get(i),nodes.get(j));

                        degreeIgnore += degrees[j];

                        //increment the number of edges
                        added++;
                        //increment the degrees of each node
                        degrees[i]++;
                        degrees[j]++;



                        //Stop iterating for this probability, once we have found a single edge
                        break;
                    }
                }
            }
            numEdges += added;
        }
    }
}
