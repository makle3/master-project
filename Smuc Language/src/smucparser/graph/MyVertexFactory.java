package smucparser.graph;

import org.jgrapht.VertexFactory;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.datatypes.simple.IntDataType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Martin on 09-02-2016.
 */
public class MyVertexFactory implements VertexFactory<MyVertex> {
    private HashMap<Long, MyVertex> vertexes = new HashMap<>();
    private Class <? extends AbstractDataType> defaultDataType;
    private Map<String, Class <? extends AbstractDataType>> types = new HashMap<>();

    public MyVertexFactory(Class <? extends AbstractDataType> dataType){
        this.defaultDataType = dataType;
    }
    public MyVertexFactory(){
        this.defaultDataType = IntDataType.class;
    }



    public MyVertex createVertex(HashMap<String, AbstractDataType> items){
        MyVertex vertexType = createVertex();
        if (vertexType != null) {
            vertexType.addAttributeMap(items);
        }
        return vertexType;
    }

    @Override
    public MyVertex createVertex() {
        return new MyVertex();
    }

    public MyVertex createVertex(Long id) {
        MyVertex vertex = new MyVertex();
        vertex.setId(id);
        this.vertexes.put(vertex.getId(),vertex);
        return vertex;
    }

    public MyVertex createVertex(Long id, HashMap<String, String> items) throws DataTypeException {
        MyVertex vertex = this.createVertex();
        vertex.setId(id);

        for(Map.Entry<String,String> entry : items.entrySet()){
            Class<? extends AbstractDataType> type = types.containsKey(entry.getKey()) ? types.get(entry.getKey()) : defaultDataType;
            AbstractDataType dt = AbstractDataType.newInstance(type);
            dt.setValue(dt.interpret(entry.getValue(), vertex.getId()));
            vertex.addAttribute(entry.getKey(), dt);
        }

        this.vertexes.put(vertex.getId(),vertex);
        return vertex;
    }

    public MyVertex getCreatedVertexByID(Long id){
        return this.vertexes.get(id);
    }

    public void addItemType(String itemName, Class<? extends AbstractDataType> c) {
        types.put(itemName, c);
    }

    public Class<? extends AbstractDataType> getItemType(String itemName){
        return types.get(itemName);
    }
}
