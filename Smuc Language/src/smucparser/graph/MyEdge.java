package smucparser.graph;


import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.jgrapht.graph.DefaultEdge;
import smucparser.datatypes.AbstractDataType;
import smucparser.grammar.ThrowingErrorListener;
import smucparser.grammar.TypeValueAndResult;
import smucparser.grammar.generated.GrammarLexer;
import smucparser.grammar.generated.GrammarParser;
import smucparser.grammar.semantics.EdgeFunctionListener;
import smucparser.grammar.visitor.EdgeFunctionVisitor;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Martin on 09-02-2016.
 */
public class MyEdge<T> extends DefaultEdge {
    private static final Logger log = Logger.getLogger( MyEdge.class.getName() );
    private static TreeMap<String, Pair<String,ParseTree>> commonCapabilities = new TreeMap<>();
    private TreeMap<String, ParseTree> edgeCapabilities = new TreeMap<>();
    private TreeMap<String, AbstractDataType> edgeAttribute = new TreeMap<>();
    public MyEdge(){
    }

    public void addEdgeCapability(String capability){
        if(commonCapabilities.containsKey(capability)){
            Pair<String, ParseTree> cap = commonCapabilities.get(capability);
            edgeCapabilities.put(cap.a, cap.b);
            return;
        }

        GrammarLexer lexer = new GrammarLexer(new ANTLRInputStream(capability));
        GrammarParser parser = new GrammarParser(new CommonTokenStream(lexer));
        parser.removeErrorListeners();
        parser.addErrorListener(ThrowingErrorListener.INSTANCE);

        GrammarParser.Edge_functionContext tree = parser.edge_function();


        ParseTreeWalker walker = new ParseTreeWalker();
        EdgeFunctionListener listener = new EdgeFunctionListener();
        walker.walk(listener,tree);

        //name
        edgeCapabilities.put(listener.functionName,tree);
        commonCapabilities.put(capability, new Pair(listener.functionName,tree));
    }
    public void addIdentityCapability(String capability){
        this.edgeCapabilities.put(capability, null);
    }

    public boolean hasEdgeCapability(String name){
        return this.edgeCapabilities.containsKey(name);
    }


    public MyEdge addAttribute(String key, AbstractDataType value){
        this.edgeAttribute.put(key, value);
        return this;
    }

    public AbstractDataType getAttribute(String key){
        return edgeAttribute.get(key);
    }

    public Object getAttributeValue(String key){
        AbstractDataType item1 = this.getAttribute(key);
        return item1==null ? null : item1.getValue();
    }

    public MyVertex getSource(){
        return (MyVertex) super.getSource();
    }
    public MyVertex getTarget(){
        return (MyVertex) super.getTarget();
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        if(edgeCapabilities.size() > 0 || edgeAttribute.size() > 0){
            for(Map.Entry<String, ParseTree> o : edgeCapabilities.entrySet()){
                if(o.getValue() == null){
                    continue;
                }
                sb.append(o.getValue().getText()).append("\\n");
            }

            for(Map.Entry<String, AbstractDataType> o : edgeAttribute.entrySet()){
                sb.append(o.getKey()).append("=").append(o.getValue().getValue()).append("\\n");
            }
        }

        return sb.toString();
    }

    public AbstractDataType doCapability(AbstractDataType type, GrammarParser.Edge_capabilityContext text, AbstractDataType psiResult, ParseTreeProperty<TypeValueAndResult> types) {
        if(text == null){
            return psiResult;
        }
        // We have the identity capability if it is null
        if(this.edgeCapabilities.get(text.getText()) == null){
            return psiResult;
        }
        EdgeFunctionVisitor visitor = new EdgeFunctionVisitor(type, this.getSource(), this.getTarget(), this.edgeAttribute, psiResult);
        visitor.setTypes(types);

        if(this.edgeCapabilities.containsKey(text.getText())) {
            return (AbstractDataType) visitor.visit(this.edgeCapabilities.get(text.getText()));
        }

        log.log(Level.SEVERE, "{0} has not been defined for edge {1}.", new Object[]{text.getText(), this});
        return psiResult;
    }

    public Map<String,AbstractDataType> getEdgeAttribute(){
        return this.edgeAttribute;
    }

    public Map<String, AbstractDataType> getAttributeTypes(){
        HashMap<String, AbstractDataType> types = new HashMap<>();
        for(Map.Entry<String,AbstractDataType> entry : this.getEdgeAttribute().entrySet()){
            types.put(entry.getKey(),entry.getValue());
        }
        return types;
    }

    public Map<String, ParseTree> getEdgeCapabilities() {
        return this.edgeCapabilities;
    }
    public Map<String, String> getEdgeCapabilitiesAsStringMap() {
        HashMap<String,String> ret = new HashMap<>();
        for(Map.Entry<String,ParseTree> entry: this.edgeCapabilities.entrySet()){
            ret.put(entry.getKey(), entry.getValue().getText());
        }
        return ret;
    }

    public ParseTree getEdgeCapability(String s) {
        return this.edgeCapabilities.get(s);
    }
}
