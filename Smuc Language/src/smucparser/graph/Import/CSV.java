package smucparser.graph.Import;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.antlr.v4.runtime.tree.ParseTree;
import org.jgrapht.graph.DefaultDirectedGraph;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static smucparser.graph.Import.JSON.getAbstractDataType;

/**
 * Created by martin on 07-03-2016.
 */
public class CSV {

    public static String serialize(DefaultDirectedGraph<MyVertex, MyEdge> g) {
        StringWriter sw = new StringWriter();
        CSVWriter writer = new CSVWriter(sw);

        Map<String, AbstractDataType> typeMap = new HashMap<>();
        try {
            typeMap = SMuC.checkGraphItemTypes(g);
        } catch (Exception e) {
            e.printStackTrace();
        }
        writer.writeNext(new String[]{"#Types (item","type)"},false);
        for (Map.Entry<String, AbstractDataType> entry:typeMap.entrySet()){
            writer.writeNext(new String[]{entry.getKey(), SMuC.getTypeName(entry.getValue().getClass())}, false);
        }


        Set<MyVertex> vertexes = g.vertexSet();

        writer.writeNext(new String[]{"#Vertexes (SourceID ("," item)*)"},false);

        for (MyVertex v : vertexes){
            String[] sa = new String[v.getAttributes().size()+v.getFormulaVariableTypes().size()+1];
            int i = 0;
            sa[i++] = String.valueOf(v.getId());

            Map<String,AbstractDataType> items = v.getAttributes();
            for(Map.Entry<String,AbstractDataType> item : items.entrySet()){
                sa[i++] = item.getKey() + ":" + item.getValue().getValue();
            }

            Map<String,AbstractDataType> formulaVariableTypes = v.getFormulaVariableTypes();
            for(Map.Entry<String,AbstractDataType> item : formulaVariableTypes.entrySet()){
                sa[i++] = "fv:" +  item.getKey() + ":" + item.getValue().getValue();
            }
            writer.writeNext(sa,false);
        }

        Set<MyEdge> edges = g.edgeSet();

        writer.writeNext(new String[]{"#Edges (SourceID", "TargetID", "(Capability|Item)*)"},false);

        for(MyEdge e : edges){
            String[] sa = new String[e.getEdgeAttribute().size()+2+e.getEdgeCapabilities().size()];
            int i = 0;
            sa[i++] = String.valueOf(e.getSource().getId());
            sa[i++] = String.valueOf(e.getTarget().getId());

            Map<String,AbstractDataType> items = e.getEdgeAttribute();
            for(Map.Entry<String,AbstractDataType> item : items.entrySet()){
                sa[i++] = item.getKey() + ":" + item.getValue().getValue();
            }

            Map<String, ParseTree> capabilities = e.getEdgeCapabilities();
            for(Map.Entry<String,ParseTree> item : capabilities.entrySet()){
                sa[i++] = item.getValue().getText().replace("<EOF>", "");
            }
            writer.writeNext(sa,false);
        }
        return sw.toString().trim();
    }

    private enum State{
        TYPES, VERTEX, EDGE
    }

    public static DefaultDirectedGraph<MyVertex, MyEdge> deserialize(Class<? extends AbstractDataType> dataType, String input) throws Exception {
        CSVReader reader = new CSVReader(new StringReader(input));
        MyVertexFactory factory = new MyVertexFactory(dataType);
        DefaultDirectedGraph<MyVertex, MyEdge> graph = new DefaultDirectedGraph<>(MyEdge.class);

        State state = State.TYPES;

        String[] value = reader.readNext();
        while (value != null) {
            if(value[0].trim().startsWith("#Edges")){
                value = reader.readNext();
                state = State.EDGE;
                continue;
            }
            if(value[0].trim().startsWith("#Vertexes")){
                value = reader.readNext();
                state = State.VERTEX;
                continue;
            }
            if(value[0].trim().startsWith("#Types")){
                value = reader.readNext();
                state = State.TYPES;
                continue;
            }
            if(value[0].trim().startsWith("#") || (value[0].equals("")&&value.length==1)){
                value = reader.readNext();
                continue;
            }
            if(state == State.TYPES){
                parseType(factory, value);
            }
            if(state == State.VERTEX) {
                createVertex(factory, graph, value);
            }
            if(state == State.EDGE) {
                createEdge(factory, graph, value, dataType);
            }

            value = reader.readNext();
        }
        reader.close();
        return graph;
    }

    private static void parseType(MyVertexFactory factory, String[] value) throws Exception {
        String itemName = value[0].trim();
        Class c = Class.forName(value[1].trim());
        if(AbstractDataType.class.isAssignableFrom(c)){
            factory.addItemType(itemName, c);
        }
        else {
            throw new Exception(String.format("%s is not of assignable from %s", SMuC.getTypeName(AbstractDataType.class), SMuC.getTypeName(c)));
        }
    }

    public static void createEdge(MyVertexFactory factory, DefaultDirectedGraph<MyVertex, MyEdge> graph, String[] value, Class<? extends AbstractDataType> dataType) throws Exception {
        Long source = Long.parseLong(value[0]);
        Long target = Long.parseLong(value[1]);
        MyVertex sourceV = factory.getCreatedVertexByID(source);
        MyVertex targetV = factory.getCreatedVertexByID(target);
        MyEdge edge = graph.addEdge(sourceV, targetV);

        int size = value.length;
        for (int i = 2; i < size; i++) {
            String pattern3 = "(.+):=(.+)";
            Pattern r = Pattern.compile(pattern3);
            Matcher m = r.matcher(value[i]);
            if(m.find()) {
                edge.addEdgeCapability(value[i]);
            }
            else if(!value[i].equals("")){
                String[] d = value[i].split(":");
                if(d.length == 1){
                    throw new Exception("Edge capability was split by mistake, make sure to surround with quotation marks (\").");
                }
                Class<? extends AbstractDataType> dtClass = factory.getItemType(d[0].trim());
                if(dtClass == null){
                    dtClass = dataType;
                }
                AbstractDataType dt = AbstractDataType.newInstance(dtClass);
                dt.setValue(dt.interpret(d[1].trim()));
                edge.addAttribute(d[0].trim(), dt);
            }
        }
    }

    public static void createVertex(MyVertexFactory factory, DefaultDirectedGraph<MyVertex, MyEdge> graph, String[] value) throws Exception {
        Long id = Long.parseLong(value[0]);
        int size = value.length;
        HashMap<String,String> items = new HashMap<>();
        HashMap<String,String> formulaVars = new HashMap<>();
        for (int i = 1; i < size; i++) {
            if(value[i].equals("")){
                continue;
            }
            String[] d = value[i].split(":");

            if(d.length == 3){
                if(d[0].equals("fv")){
                    formulaVars.put(d[1].trim(),d[2].trim());
                    continue;
                }
                else{
                    throw new Exception("Input \""+value[i]+"\" was not recognized.");
                }
            }

            if(d.length!=2){
                throw new Exception("Input \""+value[i]+"\" was not recognized as a key, value pair.");
            }
            items.put(d[0].trim(),d[1].trim());
        }
        MyVertex v = factory.createVertex(id,  items);

        for (Map.Entry<String, String> e:formulaVars.entrySet()){
            v.addFormulaVariableIteration(e.getKey(), 0, getAbstractDataType(e.getValue(), factory.getItemType(e.getKey())));
        }

        graph.addVertex(v);
    }

}
