package smucparser.graph.Import;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.lang3.time.StopWatch;
import org.jgrapht.graph.DefaultDirectedGraph;
import smucparser.*;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.complex.MST.MSTDataType;
import smucparser.datatypes.complex.NeighborPathDataType;
import smucparser.datatypes.complex.PathSetDataType;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.datatypes.simple.BoolDataType;
import smucparser.datatypes.simple.DoubleDataType;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Martin on 24-02-2016.
 */
public class SocialCircles {


    public static DefaultDirectedGraph<MyVertex, MyEdge> getFacebookGraph() throws Exception {
        return getFacebookGraph(new Integer[]{0});
    }

    public static DefaultDirectedGraph<MyVertex, MyEdge> getFacebookGraphFull() throws Exception {
        return getFacebookGraph(new Integer[]{});
    }

    public static DefaultDirectedGraph<MyVertex, MyEdge> getFacebookGraph(Integer[] values) throws Exception {
        HashMap<Integer, HashMap<Integer, String>> featNames = new HashMap<>();
        HashMap<Integer, HashMap<Integer, String>> nodeFeats = new HashMap<>();
        Multimap<Integer, Integer> edges = ArrayListMultimap.create();

        File folder = new File("./example-graphs/facebook/");
        File[] listOfFiles = folder.listFiles();

        if (listOfFiles != null) {
            for (File listOfFile : listOfFiles) {
                if(values.length != 0) {
                    if (listOfFile.getName().endsWith(".featnames") || listOfFile.getName().endsWith(".feat")) {
                        String test = listOfFile.getName().replace(".featnames", "").replace(".feat", "");
                        if (!Arrays.asList(values).contains(Integer.parseInt(test))) {
                            continue;
                        }
                    } else {
                        continue;
                    }
                }

                if (listOfFile.getPath().endsWith(".featnames")) {
                    HashMap<Integer, String> fn = new HashMap<>();
                    readFeatNames(fn, listOfFile.getPath());

                    featNames.put(Integer.parseInt(listOfFile.getName().split("\\.")[0]), fn);
                }
                if (listOfFile.getPath().endsWith(".feat")) {
                    HashMap<Integer, String> nf = new HashMap<>();
                    readFeats(nf, listOfFile.getPath());

                    nodeFeats.put(Integer.parseInt(listOfFile.getName().split("\\.")[0]), nf);
                }
            }
        }
        else {
            throw new Exception("No files were found in ./example-graphs/facebook/");
        }
        readAllEdges(edges, "./example-graphs/facebook/facebook_combined.txt");

        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        MyVertexFactory factory = new MyVertexFactory();

        HashMap<Integer, MyVertex> nodes = new HashMap<>();
        for(Map.Entry<Integer, HashMap<Integer, String>> e : nodeFeats.entrySet()){

            for (Map.Entry<Integer, String> f: e.getValue().entrySet()){
                MyVertex v = factory.createVertex();
                v.setId(Long.valueOf(f.getKey()));

                String feats = f.getValue();
                String[] feats_splitted = feats.split("\\s");


                for (int i = 0; i < feats_splitted.length; i++) {
                    AbstractDataType dt = new BoolDataType();
                    dt.setValue(dt.interpret(feats_splitted[i]));
                    v.addAttribute(featNames.get(e.getKey()).get(i).replace(";", "_").trim(), dt);
                }

                nodes.put(f.getKey(), v);
                g.addVertex(v);
            }
        }


        for(Map.Entry<Integer,Integer> e : edges.entries()){
            if(nodes.containsKey(e.getKey())&& nodes.containsKey(e.getValue())) {
                g.addEdge(nodes.get(e.getKey()), nodes.get(e.getValue()));
            }
        }
        return g;
    }

    private static void readAllEdges(Multimap<Integer, Integer> edges, String fileName1) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName1))) {
            for (String line; (line = br.readLine()) != null; ) {
                Pattern r = Pattern.compile("(\\d+) (\\d+)");
                Matcher m = r.matcher(line);
                if (m.find()) {
                    edges.put(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
                }
            }
        }
    }

    private static void readFeats(HashMap<Integer, String> nodeFeats, String fileName1) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName1))) {
            for (String line; (line = br.readLine()) != null; ) {
                Pattern r = Pattern.compile("(\\d+) (.+)");
                Matcher m = r.matcher(line);
                if (m.find()) {
                    nodeFeats.put(Integer.parseInt(m.group(1)), m.group(2));
                }
            }
        }
    }

    private static void readFeatNames(HashMap<Integer, String> featNames, String fileName) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            for(String line; (line = br.readLine()) != null; ) {
                Pattern r = Pattern.compile("(\\d+)(.+)anonymized feature (\\d+)");
                Matcher m = r.matcher(line);
                if(m.find()){
                    featNames.put(Integer.parseInt(m.group(1)), m.group(2) + m.group(3));
                }
            }
        }
    }


}
