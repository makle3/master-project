package smucparser.graph.Import;

import com.google.gson.*;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by martin on 07-03-2016.
 */
public class JSON {
    public static String serialize(DirectedGraph<MyVertex, MyEdge> g){

        GsonBuilder gb = new GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping();
        gb.registerTypeAdapter(DefaultDirectedGraph.class, new MyGraphAdaptor());

        Gson gson = gb.create();

        return gson.toJson(g);
    }

    public static DefaultDirectedGraph deserialize(Class<? extends AbstractDataType> dataType, String jsonString){


        AbstractDataType dataTypeObject = AbstractDataType.getInstance(dataType);
        return  new GsonBuilder()
                .registerTypeAdapter(DefaultDirectedGraph.class, new DirectedGraphDeserializer(dataTypeObject))
                .create()
                .fromJson(jsonString.replace("<EOF>", ""), DefaultDirectedGraph.class);
    }

    private static class AbstractDataTypeAdaptor implements JsonSerializer<AbstractDataType>, JsonDeserializer<AbstractDataType>{
        @Override
        public JsonElement serialize(AbstractDataType src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject obj = new JsonObject();
            obj.addProperty("value", String.valueOf(src.getValue()));
            obj.addProperty("class", getTypeName(src.getClass()));
            return obj;
        }

        @Override
        public AbstractDataType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            AbstractDataType dataType = getAbstractDataType(
                    json.getAsJsonObject().get("value").getAsString(),
                    json.getAsJsonObject().get("class").getAsString());

            return dataType;
        }

    }

    private static class MyGraphAdaptor implements JsonSerializer {
        @Override
        public JsonElement serialize(Object o, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonArray vertexes = new JsonArray();
            DefaultDirectedGraph<MyVertex, MyEdge> graph = ((DefaultDirectedGraph<MyVertex, MyEdge>) o);

            Gson vertexGson = new GsonBuilder()
                    .registerTypeAdapter(AbstractDataType.class, new AbstractDataTypeAdaptor()).create();

            for(MyVertex v : graph.vertexSet()){
                vertexes.add(vertexGson.toJsonTree(v));
            }

            Gson edgeGson = new GsonBuilder()
                    .registerTypeAdapter(MyEdge.class, new MyEdgeAdaptor(null))
                    .create();

            JsonArray edges = new JsonArray();

            for(MyEdge e : graph.edgeSet()){
                edges.add(edgeGson.toJsonTree(e));
            }

            JsonObject result = new JsonObject();

            result.add("vertexes", vertexes);
            result.add("edges", edges);

            return result;
        }
    }

    private static class MyEdgeAdaptor implements JsonSerializer{
        private final MyVertexFactory factory;

        public MyEdgeAdaptor(MyVertexFactory factory) {
            this.factory = factory;
        }

        @Override
        public JsonElement serialize(Object o, Type type, JsonSerializationContext jsonSerializationContext) {
            MyEdge edge = ((MyEdge) o);
            JsonObject result = new JsonObject();
            result.addProperty("target", edge.getTarget().getId());
            result.addProperty("source", edge.getSource().getId());

            Gson gb = new GsonBuilder()
                    .registerTypeAdapter(AbstractDataType.class, new AbstractDataTypeAdaptor())
                    .disableHtmlEscaping().create();
            result.add("capabilities", gb.toJsonTree(edge.getEdgeCapabilitiesAsStringMap()));
            result.add("item", gb.toJsonTree((Map<String,AbstractDataType>)edge.getEdgeAttribute()));
            return result;
        }
    }

    private static class DirectedGraphDeserializer implements JsonDeserializer {
        private final AbstractDataType dataType;

        public DirectedGraphDeserializer(AbstractDataType dataType) {
            this.dataType = dataType;
        }

        @Override
        public Object deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            DefaultDirectedGraph<MyVertex, MyEdge> graph = new DefaultDirectedGraph<>(MyEdge.class);
            MyVertexFactory factory = new MyVertexFactory(this.dataType.getClass());
            MyVertex[] vertexes = new GsonBuilder()
                    .registerTypeAdapter(MyVertex[].class, new MyVertexAdaptor(factory))
                    .create()
                    .fromJson(((JsonObject) jsonElement).get("vertexes"), MyVertex[].class);


            for (MyVertex vertex: vertexes) {
                graph.addVertex(vertex);
            }


            JsonArray edgeSet = (JsonArray) ((JsonObject) jsonElement).get("edges");
            Iterator<JsonElement> iterator = edgeSet.iterator();

            while(iterator.hasNext()){
                JsonObject element = (JsonObject) iterator.next();
                Long target = element.get("target").getAsLong();
                Long source = element.get("source").getAsLong();
                MyEdge e = graph.addEdge(factory.getCreatedVertexByID(source), factory.getCreatedVertexByID(target));

                JsonObject items = element.get("item").getAsJsonObject();
                for (Map.Entry<String, JsonElement> item : items.entrySet()) {
                    String name = item.getKey();

                    AbstractDataType AbstractDataType = new GsonBuilder()
                            .registerTypeAdapter(AbstractDataType.class, new AbstractDataTypeAdaptor()).create()
                            .fromJson(item.getValue(), AbstractDataType.class);

                    e.addAttribute(name, AbstractDataType);
                }

                JsonObject capabilities = element.get("capabilities").getAsJsonObject();
                for (Map.Entry<String, JsonElement> capability : capabilities.entrySet()) {
                    JsonElement cap = capability.getValue();
                    e.addEdgeCapability(cap.getAsString());
                }
            }

            return graph;
        }


        private class MyVertexAdaptor implements JsonDeserializer {
            private final MyVertexFactory factory;

            public MyVertexAdaptor(MyVertexFactory factory) {
                this.factory = factory;
            }

            @Override
            public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                JsonArray array = json.getAsJsonArray();
                MyVertex vertexes[] = new MyVertex[array.size()];
                int i = 0;
                for (JsonElement obj : array) {

                    JsonObject jsonObj = obj.getAsJsonObject();
                    Long id = jsonObj.get("id").getAsLong();

                    MyVertex vertex = null;
                    try {
                        vertex = factory.createVertex(id,new HashMap<String, String>());
                    } catch (DataTypeException e) {
                        e.printStackTrace();
                    }

                    Set<Map.Entry<String, JsonElement>> items = jsonObj.get("attribute").getAsJsonObject().entrySet();

                    for(Map.Entry<String, JsonElement> item : items){
                        AbstractDataType AbstractDataType = new GsonBuilder()
                                .registerTypeAdapter(AbstractDataType.class, new AbstractDataTypeAdaptor()).create()
                                .fromJson(item.getValue(), AbstractDataType.class);
                        vertex.addAttribute(item.getKey(), AbstractDataType);
                    }



                    JsonObject formulaVars = jsonObj.get("formulaVariables").getAsJsonObject();
                    for (Map.Entry<String, JsonElement> entries : formulaVars.entrySet()) {
                        AbstractDataType test = new GsonBuilder()
                                .registerTypeAdapter(AbstractDataType.class, new AbstractDataTypeAdaptor()).create()
                                .fromJson(entries.getValue(), AbstractDataType.class);
                        vertex.addFormulaVariableIteration(entries.getKey(), 0, test);
                        int a = 0;
                    }

                    //todo add this
                    JsonObject formulaVarsAlias = jsonObj.get("formulaVariableAlias").getAsJsonObject();



                    vertexes[i++] = vertex;
                }
                return vertexes;
            }
        }
    }

    public static AbstractDataType getAbstractDataType(String val, String c) {
        Class<? extends AbstractDataType> theClass = null;
        try {
            theClass = (Class<? extends AbstractDataType>) Class.forName(c);
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
            throw new JsonParseException(String.format("%sis not of type %s", c, getTypeName(AbstractDataType.class)));
        }

        return getAbstractDataType(val, theClass);
    }
    public static AbstractDataType getAbstractDataType(String val, Class<? extends AbstractDataType> c) {
        AbstractDataType ins = null;
        try {
            ins = AbstractDataType.newInstance(c);
        } catch (DataTypeException e) {
            e.printStackTrace();
        }
        ins.setValue(ins.interpret(val.toString()));
        return ins;
    }

    private static String getTypeName(Class clazz){
        return clazz.getName().replaceFirst("class ", "");
    }
}
