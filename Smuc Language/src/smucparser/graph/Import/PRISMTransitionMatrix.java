package smucparser.graph.Import;

import org.jgrapht.graph.DefaultDirectedGraph;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.simple.specialized.PrismDataType;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Martin on 29-02-2016.
 */
public class PRISMTransitionMatrix {
    public static void main(String[] args) throws IOException {
        DefaultDirectedGraph<MyVertex, MyEdge> g = importMatrix("transitionMatrix.txt", "states.txt", PrismDataType.class);
        try {
            SMuC.performQuery(PrismDataType.class, g,"lfp z. +(f)z");
        } catch (DataTypeException e) {
            e.printStackTrace();
        }
    }

    public static DefaultDirectedGraph<MyVertex, MyEdge> importMatrix(String matrixFile, String statesFile, Class<? extends AbstractDataType> dataType) throws IOException {
        List<Integer> edges1 = new ArrayList<>();
        List<Integer> edges2 = new ArrayList<>();
        List<Double> edgesProbability = new ArrayList<>();
        List<HashMap<String,String>> states = new ArrayList<>();
        AbstractDataType datatypeObj = AbstractDataType.getInstance(dataType);

        File file = new File(matrixFile);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String s;
        int i = 0;
        while ((s = br.readLine()) != null)
        {
            String pattern = "^(?:(\\d+)\\s(\\d+)\\s(\\d+(?:.\\d+)?))";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(s);
            if (m.find()) {
                if(m.group(1) != null) {
                    edges1.add(i,Integer.parseInt(m.group(1)));
                    edges2.add(i,Integer.parseInt(m.group(2)));
                    edgesProbability.add(i,Double.parseDouble(m.group(3)));
                }
                i++;
            }
        }
        br.close();

        br = new BufferedReader(new FileReader(statesFile));
        String[] stateNames = new String[0];
        while ((s = br.readLine()) != null)
        {
            String pattern = "(?:(\\d+):)?[(](.*)[)]";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(s);

            if (m.find()) {
                if(m.group(1) == null) {
                    stateNames = m.group(2).split(",");
                }
                else {
                    int node = Integer.parseInt(m.group(1));
                    String[] states1 = m.group(2).split(",");
                    HashMap<String,String> stateMap = new HashMap<>();
                    for (int j = 0; j < states1.length; j++) {
                        stateMap.put(stateNames[j], states1[j]);
                    }
                    states.add(node,stateMap);
                }
            }
        }
        br.close();

        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        MyVertexFactory factory = new MyVertexFactory(dataType);
        HashMap<Integer, MyVertex> nodes = new HashMap<>();
        for (int j : union(edges1,edges2)) {
            MyVertex v = factory.createVertex();

            for(Map.Entry<String, String> e : states.get(j).entrySet()){
                AbstractDataType a = null;
                try {
                    a = AbstractDataType.newInstance(dataType);
                } catch (DataTypeException e1) {
                    e1.printStackTrace();
                }
                a.setValue(datatypeObj.interpret(e.getValue(), v.getId()));
                v.addAttribute(e.getKey(), a);
            }

            nodes.put(j, v);

            AbstractDataType a = null;
            try {
                a = AbstractDataType.newInstance(dataType);
            } catch (DataTypeException e) {
                e.printStackTrace();
            }
            if(j==0){
                a.setValue(datatypeObj.interpret("1.0",v.getId() ));
                v.addAttribute("i", a);
            }
            else {
                a.setValue(datatypeObj.interpret("0.0", v.getId()));
                v.addAttribute("i", a);
            }
            g.addVertex(v);
        }

        for (int j = 0; j < edges1.size(); j++) {
            MyVertex v1 = nodes.get(edges1.get(j));
            MyVertex v2 = nodes.get(edges2.get(j));
            Object p = edgesProbability.get(j);
            MyEdge e = g.addEdge(v1,v2);
            AbstractDataType a = null;
            try {
                a = AbstractDataType.newInstance(dataType);
            } catch (DataTypeException e1) {
                e1.printStackTrace();
            }
            a.setValue(a.interpret(p.toString()));
            e.addAttribute("p",a);
            e.addEdgeCapability("f(x)(s,t):=x*p");
        }
        return g;
    }

    public static <T> List<T> union(List<T> list1, List<T> list2) {
        Set<T> set = new HashSet<T>();

        set.addAll(list1);
        set.addAll(list2);

        return new ArrayList<T>(set);
    }
}
