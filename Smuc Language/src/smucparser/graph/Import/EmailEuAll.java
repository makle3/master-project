package smucparser.graph.Import;

import org.jgrapht.graph.DefaultDirectedGraph;
import smucparser.datatypes.simple.IntDataType;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;
import smucparser.graph.MyVertexFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by Martin on 25-02-2016.
 */
public class EmailEuAll {
    public static void main(String[] args) throws IOException {
        HashSet<Integer> nodeIds = new HashSet<>();
        HashMap<Integer, Integer> edges = new HashMap<>();


        //for (String s : ) {
        //
        //}
        //Files.lines(Paths.get("Email-EuAll.txt")).forEach(s -> {
        //    String pattern = "(\\d+)\\s(\\d+)";
        //    Pattern r = Pattern.compile(pattern);
        //    Matcher m = r.matcher(s);
        //    if(m.find()){
        //        edges.put(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
        //    }
        //});

        for(Map.Entry<Integer, Integer> e : edges.entrySet()){
            nodeIds.add(e.getKey());
            nodeIds.add(e.getValue());
        }

        DefaultDirectedGraph<MyVertex, MyEdge> g = new DefaultDirectedGraph<>(MyEdge.class);
        MyVertexFactory factory = new MyVertexFactory(IntDataType.class);
        HashMap<Integer, MyVertex> nodes = new HashMap<>();
        for(Integer i: nodeIds){
            MyVertex v = factory.createVertex();
            nodes.put(i, v);
            g.addVertex(v);
        }




        for(Map.Entry<Integer,Integer> e : edges.entrySet()){
            g.addEdge(nodes.get(e.getKey()),nodes.get(e.getValue()));
        }

        //SMuC.serialize(g,s);
    }
}
