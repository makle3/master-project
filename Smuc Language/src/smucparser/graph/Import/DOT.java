package smucparser.graph.Import;

import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.EdgeNameProvider;
import org.jgrapht.ext.VertexNameProvider;
import org.jgrapht.graph.DefaultDirectedGraph;
import smucparser.graph.MyVertex;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by martin on 07-03-2016.
 */
public class DOT {
    public static <V,E>  String serialize(DefaultDirectedGraph<V,E> g){
        return serialize(g, true, true);
    }
    public static <V,E>  String serialize(DefaultDirectedGraph<V,E> g, final boolean printItems, final boolean printFormulaVars){
        VertexNameProvider<V> vertexIDProvider = new VertexNameProvider<V>() {
            @Override
            public String getVertexName(V vertex) {
                return ((MyVertex) vertex).getId() + "";
            }
        };
        VertexNameProvider<V> vertexLabelProvider = new VertexNameProvider<V>() {
            @Override
            public String getVertexName(V vertex) {
                return ((MyVertex) vertex).toDot(printItems, printFormulaVars);
            }
        };
        EdgeNameProvider<E> edgeLabelProvider = new EdgeNameProvider<E>() {
            @Override
            public String getEdgeName(E edge) {
                return edge.toString();
            }
        };

        DOTExporter test = new DOTExporter(vertexIDProvider, vertexLabelProvider, edgeLabelProvider);

        StringWriter sb = new StringWriter();
        test.export(sb,g);
        return sb.toString();

    }

    public static <V,E> void printGraph(DefaultDirectedGraph<V,E> g, String filename){
        printGraph(serialize(g, true, true), filename, true);
    }

    public static <V,E> void printGraph(DefaultDirectedGraph<V,E> g, String filename, boolean asDot){
        printGraph(serialize(g, true, true), filename, asDot);
    }
    public static <V,E> void printGraph(DefaultDirectedGraph<V,E> g, String filename, boolean printItems, boolean printFormulaVar){
        printGraph(serialize(g, printItems, printFormulaVar), filename, true);
    }
    public static <V,E> void printGraph(DefaultDirectedGraph<V,E> g, String filename, boolean printItems, boolean printFormulaVar, boolean asDot){
        printGraph(serialize(g, printItems, printFormulaVar), filename, asDot);
    }
    public static void printGraph(String input, String filename){
        printGraph(input,filename,true);
    }

    public static void printGraph(String input, String filename, boolean asDot){
        try(PrintWriter out = new PrintWriter("graph.dot")) {
            out.println(input);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            Runtime rt = Runtime.getRuntime();
            if(asDot){
                rt.exec("dot -Tpdf graph.dot -o " + filename);
            }
            else{
                rt.exec("sfdp -Goverlap=scale -Tpdf graph.dot -o " + filename);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
