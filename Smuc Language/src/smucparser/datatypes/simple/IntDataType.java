package smucparser.datatypes.simple;

import smucparser.datatypes.AbstractComparableDataType;
import smucparser.datatypes.DataTypeDifference;
import smucparser.datatypes.helper.SmucDataType;
import smucparser.datatypes.helper.SmucFunction;

import static com.google.common.math.IntMath.checkedAdd;
import static com.google.common.math.IntMath.checkedMultiply;
import static com.google.common.math.IntMath.checkedSubtract;

/**
 * Created by Martin on 05-02-2016.
 */
@SmucDataType
public class IntDataType extends AbstractComparableDataType<Integer> implements DataTypeDifference<Integer> {
    public IntDataType() {
        super();
    }

    @Override
    public Integer getTop() {
        return Integer.MAX_VALUE;
    }

    @Override
    public Integer getBottom() {
        return 0;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.PLUS)
    public Integer plus(Integer ... values) {
        if(values.length == 0){
            return 0;
        }
        Integer result = null;
        for (Integer val : values) {
            if(result == null){
                result = val;
                continue;
            }
            try {
                //Guava method, throws ArithmeticException on overflow
                result = checkedAdd(result,val);
            }
            catch (ArithmeticException e){
                result = Integer.MAX_VALUE;
            }

        }
        return result;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.STAR)
    public Integer times(Integer ... values) {
        if(values.length == 0){
            return null;
        }
        Integer result = null;
        for (Integer val : values) {
            if(result == null){
                result = val;
                continue;
            }
            try {
                //Guava method, throws ArithmeticException on overflow
                result = checkedMultiply(result,val);
            }
            catch (ArithmeticException e){
                result = Integer.MAX_VALUE;
            }
        }
        return result;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.SLASH)
    public Integer divide(Integer ... values) {
        if(values.length == 0){
            return null;
        }
        Integer result = null;
        for (Integer val : values) {
            if(result == null){
                result = val;
                continue;
            }
            result = result/val;
        }
        return result;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.MINUS)
    public Integer subtract(Integer ... values) {
        if(values.length == 0){
            return 0;
        }
        Integer result = null;
        for (Integer val : values) {
            if(result == null){
                result = val;
                continue;
            }
            //Guava method, throws ArithmeticException on overflow
            result = checkedSubtract(result,val);
        }
        return result;
    }

    @SmucFunction
    public Integer min(Integer ... values){
        if(values.length == 0){
            return this.getTop();
        }
        Integer result = values[0];
        for (Integer val : values) {
            result = Math.min(result, val);
        }
        return result;
    }
    @SmucFunction
    public Integer max(Integer ... values){
        if(values.length == 0){
            return getBottom();
        }
        Integer result = values[0];
        for (Integer val : values) {
            result = Math.max(result, val);
        }
        return result;
    }


    @Override
    public Integer interpret(String input) {
        Integer sup = super.interpret(input);
        if(sup != null){
            return sup;
        }
        return Integer.parseInt(input);
    }

    @Override
    public Integer interpret(String input, Long vertexId) {
        return this.interpret(input);
    }


    @Override
    public Double difference(Integer a, Integer b) {
        a = a==null?0:a;
        b = b==null?0:b;
        if(a == 0.0){
            return b.doubleValue();
        }
        return Math.abs((double)(b - a)) /(double)a;
    }
}
