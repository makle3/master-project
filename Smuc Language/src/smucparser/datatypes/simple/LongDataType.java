package smucparser.datatypes.simple;

import smucparser.datatypes.AbstractComparableDataType;
import smucparser.datatypes.DataTypeDifference;
import smucparser.datatypes.helper.SmucDataType;
import smucparser.datatypes.helper.SmucFunction;

import static com.google.common.math.LongMath.*;

/**
 * Created by Martin on 05-02-2016.
 */
@SmucDataType
public class LongDataType extends AbstractComparableDataType<Long> implements DataTypeDifference<Long> {
    public LongDataType() {
        super();
    }

    @Override
    public Long getTop() {
        return Long.MAX_VALUE;
    }

    @Override
    public Long getBottom() {
        return 0L;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.PLUS)
    public Long plus(Long ... values) {
        if(values.length == 0){
            return 0L;
        }
        Long result = null;
        for (Long val : values) {
            if(result == null){
                result = val;
                continue;
            }
            try {
                //Guava method, throws ArithmeticException on overflow
                result = checkedAdd(result,val);
            }
            catch (ArithmeticException e){
                result = Long.MAX_VALUE;
            }
        }
        return result;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.STAR)
    public Long times(Long ... values) {
        if(values.length == 0){
            return null;
        }
        Long result = null;
        for (Long val : values) {
            if(result == null){
                result = val;
                continue;
            }
            try {
                //Guava method, throws ArithmeticException on overflow
                result = checkedMultiply(result,val);
            }
            catch (ArithmeticException e){
                result = Long.MAX_VALUE;
            }
        }
        return result;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.SLASH)
    public Long divide(Long ... values) {
        if(values.length == 0){
            return null;
        }
        Long result = null;
        for (Long val : values) {
            if(result == null){
                result = val;
                continue;
            }
            result = result/val;
        }
        return result;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.MINUS)
    public Long subtract(Long ... values) {
        if(values.length == 0){
            return 0L;
        }
        Long result = null;
        for (Long val : values) {
            if(result == null){
                result = val;
                continue;
            }
            //Guava method, throws ArithmeticException on overflow
            result = checkedSubtract(result,val);
        }
        return result;
    }

    @SmucFunction
    public Long min(Long ... values){
        if(values.length == 0){
            return this.getTop();
        }
        Long result = values[0];
        for (Long val : values) {
            result = Math.min(result, val);
        }
        return result;
    }
    @SmucFunction
    public Long max(Long ... values){
        if(values.length == 0){
            return getBottom();
        }
        Long result = values[0];
        for (Long val : values) {
            result = Math.max(result, val);
        }
        return result;
    }

    @Override
    public Long interpret(String input) {
        Long sup = super.interpret(input);
        if(sup != null){
            return sup;
        }
        return Long.parseLong(input);
    }

    @Override
    public Long interpret(String input, Long vertexId) {
        return this.interpret(input);
    }


    @Override
    public Double difference(Long a, Long b) {
        a = a==null?0:a;
        b = b==null?0:b;
        if(a == 0){
            return 0.0;
        }
        return ((double) Math.abs(a - b))/(double)a;
    }
}
