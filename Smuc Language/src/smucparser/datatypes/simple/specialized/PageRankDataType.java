package smucparser.datatypes.simple.specialized;

import smucparser.datatypes.helper.SmucDataType;
import smucparser.datatypes.simple.DoubleDataType;

/**
 * Created by Martin on 17-02-2016.
 */
@SmucDataType
public class PageRankDataType extends DoubleDataType {
    public PageRankDataType(){}

    @Override
    public Double getTop() {
        return 1.0;
    }

    @Override
    public Double getBottom() {
        return 0.0;
    }
}
