package smucparser.datatypes.simple.specialized;

import smucparser.datatypes.helper.SmucDataType;
import smucparser.datatypes.simple.DoubleDataType;

/**
 * Created by Martin on 29-02-2016.
 */
@SmucDataType
public class PrismDataType extends DoubleDataType {
    public PrismDataType(){}

    @Override
    public Double getTop() {
        return 1.0;
    }

    @Override
    public Double getBottom() {
        return 0.0;
    }
}
