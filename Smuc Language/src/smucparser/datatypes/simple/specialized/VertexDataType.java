package smucparser.datatypes.simple.specialized;

import smucparser.datatypes.complex.NodeSetDataType;
import smucparser.datatypes.helper.SmucDataType;
import smucparser.datatypes.helper.SmucFunction;
import smucparser.datatypes.simple.IntDataType;
import smucparser.datatypes.simple.LongDataType;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Martin on 05-02-2016.
 */
@SmucDataType
public class VertexDataType extends LongDataType {
    public VertexDataType() {
        super();
    }

    @SmucFunction(returnType = IntDataType.class)
    public Integer asInt(Long ... values){
        checkParameterCount("asInt", values,1);
        return values[0].intValue();
    }

    @SmucFunction(returnType = NodeSetDataType.class)
    public Set asNodeSet(Long ... values){
        checkParameterCount("asNodeSet", values,1);
        HashSet ret = new HashSet<>();
        ret.add(values[0]);
        return ret;
    }

    @Override
    public Long interpret(String input, Long vertexId) {
        if(input.equals("this")){
            return vertexId;
        }
        try {
            return super.interpret(input, vertexId);
        }
        catch (Exception e){
            return vertexId;
        }
    }
}
