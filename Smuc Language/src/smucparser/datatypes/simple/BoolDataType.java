package smucparser.datatypes.simple;

import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.DataTypeDifference;
import smucparser.datatypes.helper.SmucDataType;
import smucparser.datatypes.helper.SmucFunction;

/**
 * Created by Martin on 10-02-2016.
 */
@SmucDataType
public class BoolDataType extends AbstractDataType<Boolean> implements DataTypeDifference<Boolean>{
    public BoolDataType(){}

    @Override
    public Boolean getTop() {
        return true;
    }

    @Override
    public Boolean getBottom() {
        return false;
    }


//


    //
    @SmucFunction(symbol = SmucFunction.Symbols.PLUS)
    public Boolean plus(Boolean ... values) {
        return this.or(values);
    }
    //
    @SmucFunction(symbol = SmucFunction.Symbols.STAR)
    public Boolean times(Boolean ... values) {
        return this.and(values);
    }

    @SmucFunction
    public Boolean or(Boolean ... values){
        if(values.length == 0){
            return null;
        }
        Boolean result = values[0];
        for (Boolean val : values) {
            result = result || val;
        }
        return result;
    }
    @SmucFunction
    public Boolean and(Boolean ... values){
        if(values.length == 0){
            return null;
        }
        Boolean result = values[0];
        for (Boolean val : values) {
            result = result && val;
        }
        return result;
    }
    @SmucFunction
    public Boolean xor(Boolean ... values){
        if(values.length == 0){
            return null;
        }
        Boolean result = values[0];
        for (Boolean val : values) {
            result = result ^ val;
        }
        return result;
    }
    @SmucFunction
    public Boolean not(Boolean ... values){
        if(values.length == 0){
            return null;
        }
        return !values[0];
    }

    @SmucFunction(returnType = IntDataType.class)
    public Integer asInt(Boolean ... values){
        checkParameterCount("asInt", 1, values);
        return values[0] ? 1 : 0;
    }

    @Override
    public Boolean interpret(String input) {
        Boolean sup = super.interpret(input);
        if(sup != null){
            return sup;
        }
        switch (input) {
            case ("1"):
                return true;
            case ("0"):
                return false;
        }
        try{
            double result = Double.parseDouble(input);
            return result>0;
        }
        catch (NumberFormatException ex){
            try{
                return Boolean.parseBoolean(input);
            }
            catch (Exception e){
                return null;
            }
        }
    }

    @Override
    public Boolean interpret(String input, Long vertexId) {
        return this.interpret(input);
    }

    @Override
    public Double difference(Boolean a, Boolean b) {
        return a==b?0:1.0;
    }
}
