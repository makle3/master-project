package smucparser.datatypes.simple;

import org.apache.commons.lang3.StringUtils;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.DataTypeDifference;
import smucparser.datatypes.helper.SmucDataType;
import smucparser.datatypes.helper.SmucFunction;

/**
 * Created by Martin on 25-02-2016.
 */
@SmucDataType
public class StringDataType extends AbstractDataType<String> implements DataTypeDifference<String>{
    public StringDataType(){}

    @Override
    public String getTop() {
        return null;
    }

    @Override
    public String getBottom() {
        return null;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.PLUS)
    public String plus(String ... values) {
        String result = "";
        for(String s : values){
            result += s;
        }
        return result;
    }




    @Override
    public String interpret(String input) {
        String sup = super.interpret(input);
        if(sup != null){
            return sup;
        }
        return input;
    }

    @Override
    public String interpret(String input, Long vertexId) {
        return this.interpret(input);
    }

    @Override
    public Double difference(String a, String b) {
        return (double) StringUtils.getLevenshteinDistance(a, b);
    }
}
