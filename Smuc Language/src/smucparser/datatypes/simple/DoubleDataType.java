package smucparser.datatypes.simple;

import smucparser.datatypes.AbstractComparableDataType;
import smucparser.datatypes.DataTypeDifference;
import smucparser.datatypes.helper.SmucDataType;
import smucparser.datatypes.helper.SmucFunction;

/**
 * Created by Martin on 17-02-2016.
 */
@SmucDataType
public class DoubleDataType extends AbstractComparableDataType<Double> implements DataTypeDifference<Double>{
    public DoubleDataType() {
        super();
    }

    @Override
    public Double getTop() {
        return Double.POSITIVE_INFINITY;
    }

    @Override
    public Double getBottom() {
        return Double.NEGATIVE_INFINITY;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.PLUS)
    public Double plus(Double ... values) {
        Double result = 0d;
        for (Double val : values) {
            result = result+val;
        }
        return result;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.STAR)
    public Double times(Double ... values) {
        if(values.length== 0){
            return null;
        }
        Double result = null;
        for (Double val : values) {
            if(result == null){
                result = val;
                continue;
            }
            result = result*val;
        }
        return result;
    }



    @SmucFunction(symbol = SmucFunction.Symbols.SLASH)
    public Double divide(Double ... values) {
        if(values.length== 0){
            return null;
        }
        Double result = null;
        for (Double val : values) {
            if(result == null){
                result = val;
                continue;
            }
            result = result/val;
        }
        return result;
    }
    @SmucFunction(symbol = SmucFunction.Symbols.MINUS)
    public Double subtract(Double ... values) {
        if(values.length == 0){
            return 0d;
        }
        Double result = null;
        for (Double val : values) {
            if(result == null){
                result = val;
                continue;
            }
            result = result-val;
        }
        return result;
    }

    @SmucFunction
    public Double max(Double ... values){
        if(values.length == 0){
            return this.getBottom();
        }
        Double result = values[0];
        for (Double val : values) {
            result = Math.max(result, val);
        }
        return result;
    }

    @SmucFunction
    public Double min(Double ... values){
        if(values.length == 0){
            return this.getTop();
        }
        Double result = values[0];
        for (Double val : values) {
            result = Math.min(result, val);
        }
        return result;
    }




    @Override
    public Double interpret(String input) {
        Double sup = (Double) super.interpret(input);
        if(sup != null){
            return sup;
        }
        return Double.parseDouble(input);
    }


    @Override
    public Double interpret(String input, Long vertexId) {
        return this.interpret(input);
    }

    @Override
    public Double difference(Double a, Double b) {
        a = a==null?0:a;
        b = b==null?0:b;
        if(a == 0){
            return b;
        }
        return Math.abs(a - b) / a;
    }
}
