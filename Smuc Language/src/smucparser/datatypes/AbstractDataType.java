package smucparser.datatypes;

import com.google.gson.annotations.SerializedName;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import smucparser.SMuC;
import smucparser.datatypes.helper.DataTypeException;
import smucparser.datatypes.helper.DefaultDataType;
import smucparser.datatypes.helper.SmucFunction;
import smucparser.graph.MyVertex;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Martin on 05-02-2016.
 */
public abstract class AbstractDataType<T>{
    private static final Logger log = Logger.getLogger( AbstractDataType.class.getName() );

    private T value;

    //Used in import and export to keep easily serialize class name.
    @SerializedName("class")
    private final String thisClass = SMuC.getTypeName(this.getClass()).toString();

    private DataTypeDifference<T> dataTypeDifference = null;

    private static transient HashMap<Class<? extends AbstractDataType>, AbstractDataType> instances = new HashMap<>();

    private HashMap<String, Method> methods = new HashMap<>();
    private HashSet<String> notMethods = new HashSet<>();
    private static HashMap<Method, Annotation> annotationMap = new HashMap<>();

    public AbstractDataType(){}

    /**
     * Used to define what the top value of a field is.
     * @return the top value of the field.
     */
    public abstract T getTop();

    /**
     * Used to define what the bottom value of a field is.
     * @return the bottom value of the field.
     */
    public abstract T getBottom();


    /**
     * Calls the methods defined in the implementing class by name.
     * @param name the string name of a method to call.
     * @param values A list of values to perform the method on.
     * @return the result of the implemented method.
     * @throws Exception if a method with the given name is not found.
     */
    public Object function(String name, MyVertex vertex, Object ... values) throws ParseCancellationException {
        if(!methods.containsKey(name)){
            if(!checkForFunction(name)){
                throw new ParseCancellationException(String.format("A method of name %s using the %s annotation, was not found for %s",
                        name, SMuC.getTypeName(SmucFunction.class), SMuC.getTypeName(this.getClass())));
            }
        }
        if(values == null){
            return null;
        }
        Object val = null;

        try {
            Method m = methods.get(name);

            Class<?>[] parameterTypes = m.getParameterTypes();
            m.setAccessible(true);

            SmucFunction annotation = (SmucFunction) getMethodAnnotation(SmucFunction.class, this.getClass(), m);
            if(annotation.takesNodeId()){
                // Have to invoke the method with an array of the right type.
                Object[] typedArray = Arrays.copyOf(values, values.length, (Class<? extends Object[]>) parameterTypes[1]);
                val = m.invoke(this, vertex.getId(), (Object) typedArray);
            }
            else {
                Object[] typedArray = Arrays.copyOf(values, values.length, (Class<? extends Object[]>) parameterTypes[0]);
                val = m.invoke(this, (Object) typedArray);
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return (T)val;
    }



    /**
     * Searches for method with name annotated with SmucFunction.
     * @param name The name of the method to find.
     * @return True if the method is found.
     */
    public boolean checkForFunction(String name){
        if(methods.containsKey(name)){
            return true;
        }
        if(notMethods.contains(name)){
            return false;
        }

        boolean result = false;
        for(Method m : this.getClass().getMethods()){
            SmucFunction annotation;
            try {
                annotation = (SmucFunction) getMethodAnnotation(SmucFunction.class, this.getClass(), m);
            } catch (NoSuchMethodException e) {
                continue;
            }
            if(annotation != null){
                if(m.getName().equals(name)){
                    methods.put(name, m);
                    result = true;
                }

                if(name.equals("+") || name.equals("*") || name.equals("/") || name.equals("-")){
                    //SmucFunction annotation = m.getAnnotation(SmucFunction.class);
                    if(     (annotation.symbol() == SmucFunction.Symbols.PLUS  && name.equals("+")) ||
                            (annotation.symbol() == SmucFunction.Symbols.MINUS && name.equals("-")) ||
                            (annotation.symbol() == SmucFunction.Symbols.SLASH && name.equals("/")) ||
                            (annotation.symbol() == SmucFunction.Symbols.STAR  && name.equals("*"))){
                        if(methods.containsKey(name)){
                            throw new ParseCancellationException(
                                    String.format("%s contains multiple methods using the same symbol. %s and %s both use symbol %s.",
                                            this.getClass().toString(),
                                            methods.get(name).getName(),
                                            m.getName(),
                                            name));
                        }

                        methods.put(name, m);
                        methods.put(m.getName(), m);
                        result = true;
                    }
                }
            }
        }
        if(!result){
            notMethods.add(name);
        }
        return result;
    }

    /**
     * Given a method, search for a given annotation by checking for existing methods in the super-class. The annotation
     * of the first method with the given signature is returned. This search is limited to classes up until
     * {@link AbstractDataType}
     * @return The annotation of the given method.
     * @throws NoSuchMethodException If no method was found.
     */
    private Annotation getMethodAnnotation(Class<? extends Annotation> annotation, Class c, Method m1) throws NoSuchMethodException {
        if(annotationMap.containsKey(m1)){
            return annotationMap.get(m1);
        }

        if(!(AbstractDataType.class.isAssignableFrom(c))){
            return null;
        }
        Method m = c.getMethod(m1.getName(), m1.getParameterTypes());
        if(m == null){
            return null;
        }
        if(m.isAnnotationPresent(annotation)){
            Annotation annotation1 = m.getAnnotation(annotation);
            annotationMap.put(m1, annotation1);
            return annotation1;
        }
        return getMethodAnnotation(annotation, c.getSuperclass(), m);
    }

    /**
     * Used to find the return type of a method in case a method returns a different type than the
     * one it is defined in.
     * @param name the name of the method.
     * @return The class of the return type.
     */
    public Class<? extends AbstractDataType> getMethodReturnType(String name){
        if(checkForFunction(name)){
            if(name.equals("+") || name.equals("*") || name.equals("/") || name.equals("-")){
                return this.getClass();
            }
            SmucFunction a = methods.get(name).getAnnotation(SmucFunction.class);
            if(a.returnType().equals(DefaultDataType.class)){
                return this.getClass();
            }
            return a.returnType().equals(AbstractDataType.class) ? this.getClass() : a.returnType();
        }
        return null;
    }

    /**
     * Used to interpret strings in the implementing class. The strings top, bot and bottom
     * @return {@link AbstractDataType#getTop()} if the input is "top" or {@link AbstractDataType#getBottom()} if the
     * input is "bot" or "bottom", otherwise null.
     * @param input The input string to interpret.
     * @param vertexId
     */
    public T interpret(String input, Long vertexId){
        return interpretDefault(input);
    }

    /**
     * Called when edge
     * @param input
     * @return
     */
    public T interpret(String input){
        return interpretDefault(input);
    }

    private T interpretDefault(String input) {
        if(input.equalsIgnoreCase("top")){
            return this.getTop();
        }
        else if(input.equalsIgnoreCase("bottom")||
                input.equalsIgnoreCase("bot")){
            return this.getBottom();
        }
        return null;
    }

    public static AbstractDataType getInstance(Class<? extends AbstractDataType> theClass){
        if(instances.containsKey(theClass)){
            return instances.get(theClass);
        }
        AbstractDataType newInstance = null;
        try {
            newInstance = newInstance(theClass);
        } catch (DataTypeException e) {
            e.printStackTrace();
        }
        instances.put(theClass, newInstance);
        return newInstance;
    }


    protected static void addInstance(AbstractDataType instance){
        instances.put(instance.getClass(), instance);
    }

    /**
     * Used to create an instance of the implementing class of {@link AbstractDataType}. If the values is not
     * null, then the value of the datatype will be set. Calls the constructor of the class which takes 0 parameters.
     * @param theClass The class {@link AbstractDataType} to be instantiated.
     * @return A new instance of the theClass {@link AbstractDataType}.
     */
    public static AbstractDataType newInstance(Class<? extends AbstractDataType> theClass) throws DataTypeException {
        AbstractDataType a = null;
        try {
            String error;
            Constructor[] constructors = theClass.getConstructors();
            if(constructors.length == 0){
                error = String.format("No public constructor found for class %s", SMuC.getTypeName(theClass));
                log.log(Level.SEVERE, error);
                throw new DataTypeException(error);
            }

            for(Constructor c : constructors){
                if (c.getTypeParameters().length == 0) {
                    a = (AbstractDataType) c.newInstance();
                    break;
                }
            }

            if(a==null){
                error = String.format("%s is missing a public constructor without arguments.", SMuC.getTypeName(theClass));
                log.log(Level.SEVERE, error);
                throw new DataTypeException(error);
            }

        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e1) {
            throw new DataTypeException(String.format("An error occurred while creating new instance of datatype %s", SMuC.getTypeName(theClass)), e1);
        }
        return a;
    }

    public AbstractDataType newInstance(){
        try {
            return this.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
    public AbstractDataType newInstance(T value){
        try {
            AbstractDataType dt = this.getClass().newInstance();
            dt.setValue(value);
            return dt;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static AbstractDataType newInstance(Class<? extends AbstractDataType> theClass, Object value) throws DataTypeException {
        AbstractDataType dt = newInstance(theClass);
        dt.setValue(value);
        return dt;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AbstractDataType) {
            return java.util.Objects.equals(this.getValue(), ((AbstractDataType) obj).getValue());
        }
        return false;
    }


    public Double getDifference(T first, T second) throws DataTypeDifferenceNotDefinedException{
        if(this.dataTypeDifference != null) {
            return this.dataTypeDifference.difference(first, second);
        }
        else{
            if(!(this instanceof DataTypeDifference)){
                throw new DataTypeDifferenceNotDefinedException(String.format("A method to calculate differences between objects of %s has not been defined.",this.getClass()));
            }
            return ((DataTypeDifference) this).difference(first, second);
        }
    }

    public void setDataTypeDifference(DataTypeDifference<T> dtd){
        this.dataTypeDifference = dtd;
    }

    public void checkParameterCount(String par, Object ... values) {
        if(values.length != 2 || values[0] == null || values[1] == null){
            throw new ParseCancellationException(String.format("%s in %s must have two parameters, neither of which are null.", par, this.getClass()));
        }
    }

    public void checkParameterCount(String par, int amount, Object ... values) {
        if(values.length != amount){
            throw new ParseCancellationException(String.format("%s in %s must have %s parameters, none of which are null.", par, this.getClass(), amount));
        }
    }

    @Override
    public String toString() {
        return SMuC.getTypeName(this.getClass());
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

}
