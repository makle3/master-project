package smucparser.datatypes;

/**
 * Created by Martin on 25-04-2016.
 */
public class DataTypeDifferenceNotDefinedException extends Exception {
    public DataTypeDifferenceNotDefinedException(String message) {
        super(message);
    }
}
