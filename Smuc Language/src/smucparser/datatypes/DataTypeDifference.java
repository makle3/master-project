package smucparser.datatypes;

/**
 * Created by Martin on 23-04-2016.
 */
public interface DataTypeDifference<T> {
    Double difference(T a, T b);
}
