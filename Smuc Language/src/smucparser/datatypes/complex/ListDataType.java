package smucparser.datatypes.complex;

import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.helper.SmucFunction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Martin on 27-05-2016.
 */
public class ListDataType<T> extends AbstractDataType<List<T>> implements Comparable<ListDataType<T>>{

    @Override
    public List<T> getTop() {
        return new ArrayList<>();
    }

    @Override
    public List<T> getBottom() {
        return new BotList();
    }

    @SmucFunction
    public List<T> addNode(List<T>... values){
        List result = values[0];
        for (List<T> l: values) {
            if(l == getBottom()){
                return getBottom();
            }
            for (T element : l) {
                if (result.contains(element)) {
                    continue;
                }
                result.add(element);
            }
        }
        return result;
    }


    @Override
    public int compareTo(ListDataType<T> o) {
        return this.getValue().size()-o.getValue().size();
    }

    @Override
    public boolean equals(Object obj) {
        return this.getValue().equals (((ListDataType) obj).getValue());
    }

    @Override
    public int hashCode() {
        return this.getValue().hashCode();
    }

    @Override
    public String toString() {
        return getValue().toString();
    }
}
