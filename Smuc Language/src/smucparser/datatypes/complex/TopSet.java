package smucparser.datatypes.complex;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by Martin on 18-05-2016.
 */
public class TopSet extends HashSet {
    @Override
    public String toString() {
        return "TopSet";
    }

    @Override
    public boolean containsAll(Collection c) {
        return true;
    }

    @Override
    public boolean contains(Object o) {
        return true;
    }


}
