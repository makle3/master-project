package smucparser.datatypes.complex;

import org.antlr.v4.runtime.misc.ParseCancellationException;
import smucparser.SMuC;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.DataTypeDifference;
import smucparser.datatypes.helper.SmucFunction;
import smucparser.datatypes.simple.IntDataType;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Martin on 19-04-2016.
 */
public class SetDataType<T> extends AbstractDataType<Set<T>> implements DataTypeDifference<Set<T>> {
    private final Class<? extends Set> setClazz;

    public SetDataType(Class<? extends Set> clazz){
        setClazz = clazz;
    }

    private static final Logger log = Logger.getLogger( SetDataType.class.getName() );




    private static TopSet topSet = new TopSet();

    @Override
    public Set getTop() {
        return topSet;
    }

    @Override
    public Set getBottom(){
        return newClazzInstance();
    }

    Set<T> newClazzInstance() {
        try {
            return setClazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        log.log(Level.SEVERE, String.format("Error occurred when creating instance of %s, creating a HashSet instead.", SMuC.getTypeName(setClazz)));
        return new HashSet();
    }

    @SmucFunction
    public Set union(Set ... values){
        if (values == null){
            return this.getBottom();
        }
        Set temp1 = getBottom();
        for(Set s : values){
            if(s == null){
                continue;
            }
            if(s instanceof TopSet){
                return this.getTop();
            }
            temp1.addAll(s);
        }
        return temp1;
    }

    @SmucFunction
    public Set intersection(Set ... values){
        if (values == null){
            return this.getBottom();
        }
        Set temp1 = null;
        for(Set s : values){
            if (s == null){
                return this.getBottom();
            }
            if(s instanceof TopSet){
                continue;
            }
            if(temp1 == null){
                temp1 = newClazzInstance();
                temp1.addAll(s);
                continue;
            }

            temp1.retainAll(s);
        }
        return temp1;
    }

    @SmucFunction
    public Set complements(Set ... values){
        Set temp1 = newClazzInstance();
        int initialSet = 0;
        for (int i = 0; i < values.length; i++) {
            if(values[i] instanceof TopSet){
                continue;
            }
            initialSet = i;
            break;
        }

        temp1.addAll(values[initialSet]);
        for (int i = initialSet + 1; i < values.length; i++) {
            Set s = values[i];
            if(s instanceof TopSet){
                return newClazzInstance();
            }
            temp1.removeAll(s);
        }
        return temp1;
    }

    @SmucFunction(returnType = IntDataType.class)
    public Integer size(Set ... values){
        if(values == null){
            return 0;
        }
        if(values.length > 1){
            log.log(Level.SEVERE, "More than one set found for Set size method.");
            throw new ParseCancellationException("More than one set found for Set size method.");
        }
        if(values.length == 0){
            return 0;
        }
        return values[0].size();
    }

    @SmucFunction(symbol = SmucFunction.Symbols.PLUS)
    public Set plus(Set[] values) {
        return union(values);
    }

    @SmucFunction(symbol = SmucFunction.Symbols.STAR)
    public Set times(Set ... values) {
        return intersection(values);
    }

    @SmucFunction(symbol = SmucFunction.Symbols.MINUS)
    public Set subtract(Set ... values) {
        return complements(values);
    }

    @Override
    public Double difference(Set<T> a, Set<T> b) {
        if(a == null){
            a = new HashSet<>();
        }
        if(b == null){
            b = new HashSet<>();
        }
        if(a.equals(b)){
            return 0.0;
        }

        // |A\B union B\A| / |A union B|
        Set complements = this.complements(a, b);
        Set complements1 = this.complements(b, a);
        Set diff = this.union(complements, complements1);
        return ((double) diff.size())
                /
                ((double) this.union(a, b).size());
    }
}
