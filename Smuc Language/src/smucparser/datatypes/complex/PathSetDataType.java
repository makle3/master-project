package smucparser.datatypes.complex;

import smucparser.datatypes.helper.SmucDataType;
import smucparser.datatypes.helper.SmucFunction;

import java.util.*;

/**
 * Created by Martin on 15-02-2016.
 */
@SmucDataType
public class PathSetDataType extends SetDataType<Path> {
    public PathSetDataType() {
        super(HashSet.class);
    }

    @SmucFunction
    public Set<Path> addNode(Set<Path>[] values){
        checkParameterCount("addNode", values);

        for (Set pathSet : values) {
            if (pathSet instanceof TopSet) {
                return pathSet;
            }
        }

        if(values[0].size() == 0){
            return values[1];
        }

        Set ret = new HashSet();
        for (Path p : values[0]) {
            Path result = new Path(p);
            result.addNode(values[1].iterator().next().getFirst());
            ret.add(result);
        }

        return ret;
    }

    @SmucFunction(takesNodeId = true)
    public Set<Path> isNodeOne(Long nodeId, Set<Path>[] values){
        checkParameterCount("isNodeOne", 1, values);
        if(nodeId == 1L){
            return new HashSet<>();
        }
        return this.getTop();
    }
    @SmucFunction(takesNodeId = true)
    public Set<Path> isNodeZero(Long nodeId, Set<Path>[] values){
        checkParameterCount("isNodeZero", 1, values);
        if(nodeId == 0L){
            return new HashSet<>();
        }
        return this.getTop();
    }

    @Override
    public Set<Path> interpret(String input, Long vertexId) {
        Set<Path> sup = super.interpret(input);
        if(sup != null){
            return sup;
        }
        if(input.equals("1")) {
            HashSet result1 = new HashSet();
            result1.add(new Path(vertexId));
            return result1;
        }
        else{
            return new HashSet<>();
        }
    }



    @SmucFunction
    public Set filter(Object[] values){
        if(values[0] instanceof TopSet){
            return (Set) values[0];
        }

        HashSet<Path> ret = new HashSet<>();
        for (Path p1 : (HashSet<Path>) values[0] ) {
            for (Path p2 : (HashSet<Path>) values[1]) {
                if (p1.getLast().equals(p2.getLast())) {
                    ret.add(p2);
                }
            }
        }
        return ret;
    }

    @SmucFunction
    public Set<Path> unionNodes(Object[] values){
        HashSet ret = new HashSet();
        ret.add(new Path((Long) values[0]));
        ret.add(new Path((Long) values[1]));

        return ret;
    }



    @SmucFunction
    public Set min(Set<Path>[] values){
        if(values.length == 0){
            return this.getTop();
        }
        if(values.length == 1){
            return values[0];
        }

        Set<Path> combined = new HashSet<>();
        Integer topCount = 0;
        Integer smallest = Integer.MAX_VALUE;
        for (Set<Path> value : values) {
            if (value instanceof TopSet) {
                topCount += 1;
                continue;
            }
            combined.addAll(value);
            if(value.size() == 0){
                smallest = 0;
            }
            else{
                for (Path p : value) {
                    if (p.size() < smallest) {
                        smallest = p.size();
                    }
                }
            }
        }

        if(topCount == values.length){
            return this.getTop();
        }


        HashSet result = new HashSet();
        if(smallest>0) {
            for (Path s : combined) {
                if (Objects.equals(s.size(), smallest)) {
                    result.add(s);
                }
            }
        }
        return result;
    }
}
