package smucparser.datatypes.complex.MST;

import org.antlr.v4.runtime.misc.MurmurHash;
import org.antlr.v4.runtime.misc.ObjectEqualityComparator;
import smucparser.datatypes.complex.SetDataType;
import smucparser.datatypes.complex.TopSet;

import java.util.*;

/**
 * Created by Martin on 07-05-2016.
 */
public class MSTTuple implements Comparable{
    private Long root = null;
    private Edge potentialEdge = new Edge(null,null);

    public void setValue(Integer value) {
        this.value = value;
    }

    private Integer value;
    private final Set<Long> mSTAdjacentNodes;



    //public MSTTuple(Long root, int valueIn) {
    //    this.root = root;
    //    value = valueIn;
    //}

    public MSTTuple(Long root, Integer valueIn, Set<Long> adjacentNodes) {
        this.root = root;
        value = valueIn;
        this.mSTAdjacentNodes = adjacentNodes;
    }

    public MSTTuple(MSTTuple tuple) {
        root = tuple.getRoot();
        this.value = tuple.getValue();

        if (tuple.getmSTAdjacentNodes() instanceof TopSet){
            this.mSTAdjacentNodes = new TopSet();
        }
        else {
            this.mSTAdjacentNodes = new HashSet<>();
            this.mSTAdjacentNodes.addAll(tuple.getmSTAdjacentNodes());
        }

        this.potentialEdge = tuple.potentialEdge;
    }



    @Override
    public int compareTo(Object o) {
        MSTTuple t1 = this;
        MSTTuple t2 = (MSTTuple)o;

        Integer ret = t1.getValue() - t2.getValue();
        if(ret != 0){
            return ret;
        }

        if (t1.root == null && t2.root == null) {
            return 0;
        }
        if (t1.root == null) {
            return 1;
        }
        if (t2.root == null) {
            return -1;
        }
        ret = t1.getRoot().compareTo(t2.getRoot());
        if (ret != 0) {
            return ret;
        }

        ret = t1.potentialEdge.compareTo(t2.potentialEdge);
        if(ret != 0){
            return ret;
        }

        return t1.getValue().compareTo(t2.getValue());
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        else if (!(obj instanceof MSTTuple)) {
            return false;
        }

        MSTTuple other = (MSTTuple) obj;
        return   ObjectEqualityComparator.INSTANCE.equals(getRoot(), other.getRoot()) &&
                 ObjectEqualityComparator.INSTANCE.equals(getValue(), other.getValue()) &&
                 ObjectEqualityComparator.INSTANCE.equals(getPotentialEdge(), other.getPotentialEdge()) &&
                 ObjectEqualityComparator.INSTANCE.equals(getmSTAdjacentNodes(), other.getmSTAdjacentNodes());
    }

    @Override
    public int hashCode() {
        int hash = MurmurHash.initialize();
        hash = MurmurHash.update(hash, getRoot());
        hash = MurmurHash.update(hash, getValue());
        hash = MurmurHash.update(hash, getPotentialEdge().getSourceId());
        hash = MurmurHash.update(hash, getPotentialEdge().getTargetId());
        hash = MurmurHash.update(hash, getmSTAdjacentNodes());
        return MurmurHash.finish(hash, 2);
    }

    @Override
    public String toString() {
        return String.format("(%s, %s, %s, %s)", getValue(), getRoot(), potentialEdge, getmSTAdjacentNodes());
    }

    public Set<Long> getmSTAdjacentNodes() {
        return mSTAdjacentNodes;
    }

    public Edge getPotentialEdge() {
        return potentialEdge;
    }

    public void setPotentialEdge(Long target, Long source) {
        this.potentialEdge = new Edge(target,source);
    }
    public void setPotentialEdge(Edge edge) {
        this.potentialEdge = new Edge(edge.getSourceId(),edge.getTargetId());
    }


    public Long getRoot() {
        return root;
    }

    public void setRoot(long root) {
        this.root = root;
    }
}
