package smucparser.datatypes.complex.MST;

import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.complex.NodeSetDataType;
import smucparser.datatypes.complex.TopSet;
import smucparser.datatypes.helper.SmucFunction;
import smucparser.datatypes.simple.LongDataType;

import java.util.*;

import static smucparser.datatypes.complex.MST.Edge.min;

/**
 * Created by Martin on 06-05-2016.
 */
public class MSTDataType extends AbstractDataType<MSTTuple> {
    @Override
    public MSTTuple getTop() {
        return new MSTTuple(Long.MAX_VALUE, Integer.MAX_VALUE, new TopSet());
    }

    @Override
    public MSTTuple getBottom() {
        return new MSTTuple(null, Integer.MAX_VALUE, new HashSet<Long>());
    }

    @Override
    public MSTTuple interpret(String input, Long vertexId) {
        MSTTuple ret = super.interpret(input, vertexId);
        if (ret != null){
            return ret;
        }
        if(input.equals("1")){
            ret = new MSTTuple(this.getBottom());
            ret.setRoot(vertexId);
        }
        ret.setPotentialEdge(vertexId,null);
        return ret;
    }



    @SmucFunction
    public MSTTuple asMST(Object[] values){
        MSTTuple ret = new MSTTuple(this.getBottom());
        ret.setValue((Integer) values[0]);
        return ret;
    }

    @SmucFunction
    /**
     * Takes two inputs, adding the value of the second to the first.
     */
    public MSTTuple AddEdgeWeight(MSTTuple[] values){
        MSTTuple ret = new MSTTuple(values[0]);
        ret.setValue(Math.min(values[1].getValue(),values[0].getValue()));
        return ret;
    }

    @SmucFunction
    public Set<MSTTuple> asSet(MSTTuple[] values){
        Arrays.sort(values);
        HashSet ret = new HashSet();
        Collections.addAll(ret, values);

        return ret;
    }
    @SmucFunction
    public Set<MSTTuple> asSet2(MSTTuple[] values){
        Arrays.sort(values);
        HashSet ret = new HashSet();
        if(values.length > 0) {
            ret.add(values[0]);
            for (int i = 1; i < values.length; i++) {
                if (!Objects.equals(values[i].getRoot(), values[0].getRoot())) {
                    ret.add(values[i]);
                    break;
                }
            }
        }

        return ret;
    }

    @SmucFunction(takesNodeId = true)
    /**
     * Takes two inputs, the first being the source vertex, and the second being a set of msttuples
     */
    public MSTTuple findSmallest(Long nodeId, Object[] values){
        MSTTuple source = (MSTTuple) values[0];
        Set<MSTTuple> targetSet = (Set<MSTTuple>) values[1];
        TreeSet<MSTTuple> sortedSet = new TreeSet<>();
        sortedSet.addAll(targetSet);

        MSTTuple smallest = new MSTTuple(source);
        // If edgeID has been set, then find the first from the set which does not have the same edgeID
        if(source.getRoot() != Long.MAX_VALUE){
            for (MSTTuple tuple : sortedSet) {
                if (source.getRoot().equals(tuple.getRoot())){
                    continue;
                }
                smallest = tuple;
                break;
            }
        }
        else{
            if(sortedSet.size() == 0){
                return new MSTTuple(source);
            }
            smallest = sortedSet.first();
        }

        MSTTuple ret = new MSTTuple(source);

        if(smallest.getPotentialEdge().getSourceId() != null) {
            ret.setValue(smallest.getValue());
            ret.setPotentialEdge(smallest.getPotentialEdge().getSourceId(), source.getPotentialEdge().getSourceId());
        }
        else {
            ret.setPotentialEdge(null,null);
        }

        return ret;
    }


    @SmucFunction(takesNodeId = true)
    /**
     * Takes two inputs, the first being the source vertex, and the second being a set of msttuples
     */
    public MSTTuple connect(Long nodeId, Object[] values){
        MSTTuple source = (MSTTuple) values[0];

        Set<MSTTuple> targetSet = (Set<MSTTuple>) values[1];
        TreeSet<MSTTuple> sortedSet = new TreeSet<>();
        sortedSet.addAll(targetSet);

        MSTTuple ret = new MSTTuple(source);

        if(source.getRoot() == null){
            for (MSTTuple potential : sortedSet) {
                if (makeConnection(nodeId, source, ret, potential)) {
                    break;
                }
            }
        }
        else{
            for (MSTTuple tuple : sortedSet) {
                if (source.getRoot().equals(tuple.getRoot())){
                    continue;
                }
                if (makeConnection(nodeId, source, ret, tuple)) {
                    break;
                }
            }
        }
        return ret;
    }

    private boolean makeConnection(Long nodeId, MSTTuple source, MSTTuple ret, MSTTuple potential) {
        if (source.getPotentialEdge().equals(potential.getPotentialEdge())) {
            //Only connect if the node is part of potential edge
            if(nodeId.equals(source.getPotentialEdge().getSourceId()) ||
                    nodeId.equals(source.getPotentialEdge().getTargetId() )) {

                Long adjacent;
                if(Objects.equals(nodeId, source.getPotentialEdge().getSourceId())) {
                    adjacent = source.getPotentialEdge().getTargetId();
                }
                else {
                    adjacent = source.getPotentialEdge().getSourceId();
                }
                ret.getmSTAdjacentNodes().add(adjacent);
                return true;
            }
        }
        return false;
    }

    @SmucFunction(takesNodeId = true, symbol = SmucFunction.Symbols.PLUS)
    public MSTTuple combine(Long nodeId, MSTTuple[] values){
        MSTTuple val1 = new MSTTuple(values[0]);
        MSTTuple val2 = new MSTTuple(values[1]);
        val1.setValue(Integer.MAX_VALUE);
        val2.setValue(Integer.MAX_VALUE);

        if(val1.equals(this.getBottom())){
            val2.setPotentialEdge(nodeId, null);
            return val2;
        }
        val1.setPotentialEdge(nodeId, null);
        return val1;
    }

    @SmucFunction(takesNodeId = true)
    /**
     * Takes two inputs, the first being the source vertex, and the second being a set of msttuples
     */
    public MSTTuple shareSmallestPotentialInGroup(Long nodeId, Object[] values){
        MSTTuple source = new MSTTuple((MSTTuple) values[0]);

        if(source.getRoot() == null){
            return source;
        }

        Set<MSTTuple> targetSet = (Set<MSTTuple>) values[1];
        TreeSet<MSTTuple> sortedSet = new TreeSet<>();
        sortedSet.addAll(targetSet);
        for (MSTTuple groupMember : sortedSet) {
            if(groupMember.getPotentialEdge().getTargetId().equals(groupMember.getPotentialEdge().getSourceId())){
                continue;
            }

            //If the selected node contains the id of the current node in adjList, then use the smallest value
            if (groupMember.getmSTAdjacentNodes().contains(nodeId)){
                source.setRoot(Math.min(source.getRoot(), groupMember.getRoot()));

                if(source.getValue() > groupMember.getValue()) {
                    source.setValue(groupMember.getValue());
                    source.setPotentialEdge(groupMember.getPotentialEdge());
                }
            }
        }
        return source;
    }

    @SmucFunction(returnType = NodeSetDataType.class)
    public Set<Long> asNodeSet(Object[] values){
        checkParameterCount("",1,values);
        HashSet<Long> nodeSet = new HashSet<>();
        nodeSet.addAll(((MSTTuple) values[0]).getmSTAdjacentNodes());
        return nodeSet;
    }

    @SmucFunction(returnType = LongDataType.class)
    public Long getRoot(Object[] values){
        checkParameterCount("",1,values);
        MSTTuple value = (MSTTuple) values[0];
        return value.getRoot();
    }

    @SmucFunction
    public Object[] unionNodes(Object[] values){
        return values;
    }

    @SmucFunction(takesNodeId = true)
    public MSTTuple filter(Long nodeId, Object[] values){
        MSTTuple value = (MSTTuple) values[0];
        Long aLong = (Long) ((Object[]) values[1])[0];
        Long bLong = (Long) ((Object[]) values[1])[1];

        HashSet<Long> adjList = new HashSet<>();
        if(value.getmSTAdjacentNodes().contains(aLong)){
            adjList.add(aLong);
        }
        if(value.getmSTAdjacentNodes().contains(bLong)){
            adjList.add(bLong);
        }

        return value;
    }

}
