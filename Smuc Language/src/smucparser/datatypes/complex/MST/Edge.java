package smucparser.datatypes.complex.MST;

import org.antlr.v4.runtime.misc.ObjectEqualityComparator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

/**
 * Created by Martin on 09-05-2016.
 */
public class Edge implements Comparable{
    private final Long[] nodes = new Long[2];

    public Edge(Long sourceId, Long targetId) {
        nodes[0] = sourceId;
        nodes[1] = targetId;
        Arrays.sort(nodes, new Comparator<Long>() {
            @Override
            public int compare(Long o1, Long o2) {
                if (o1 == null && o2 == null) {
                    return 0;
                }
                if (o1 == null) {
                    return 1;
                }
                if (o2 == null) {
                    return -1;
                }
                return o1.compareTo(o2);
            }
        });
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", nodes[0], nodes[1]);
    }

    @Override
    public int compareTo(Object o) {
        Edge t2 = (Edge)o;

        if (getSourceId() == null && getTargetId() == null && t2.getSourceId() == null && t2.getTargetId() == null) {
            return 0;
        }
        if (getSourceId() == null && getTargetId() == null) {
            return 1;
        }
        if (t2.getSourceId() == null && t2.getTargetId() == null) {
            return -1;
        }

        if (Objects.equals(getSourceId(), t2.getSourceId())) {
            if(Objects.equals(getTargetId(), t2.getTargetId())){
                return 0;
            }
            return (int) (getTargetId() - t2.getTargetId());
        }
        return (int) (getSourceId() - t2.getSourceId());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        else if (!(obj instanceof Edge)) {
            return false;
        }

        Edge other = (Edge) obj;
        return     ObjectEqualityComparator.INSTANCE.equals(getSourceId(), other.getSourceId())
                && ObjectEqualityComparator.INSTANCE.equals(getTargetId(), other.getTargetId());
    }

    public Long getSourceId(){
        return nodes[0];
    }
    public Long getTargetId(){
        return nodes[1];
    }
    public static Edge min(Edge a, Edge b){
        return (a.compareTo(b) <= 0) ? a : b;
    }
}