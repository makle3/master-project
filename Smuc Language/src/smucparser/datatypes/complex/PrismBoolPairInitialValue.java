package smucparser.datatypes.complex;

import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.simple.specialized.PrismDataType;
import smucparser.datatypes.helper.SmucFunction;
import smucparser.graph.MyVertex;

/**
 * Created by Martin on 09-04-2016.
 */
public class PrismBoolPairInitialValue extends DoubleBoolPairDataType<PrismDataType> {
    public PrismBoolPairInitialValue() {
        super(PrismDataType.class);
    }

    @Override
    @SmucFunction(returnType = PrismDataType.class)
    public Double left(Pair<Double, Boolean> ... values) {
        return super.left(values);
    }

    @SmucFunction
    public Pair plusI(Pair<Double, Boolean> ... values) throws Exception {
        if(values.length == 0){
            return getBottom();
        }
        if(values.length == 1){
            Pair value = values[0];
            return new Pair(value.a, getTop().b);
        }
        Pair<Double, Boolean> temp = null;
        for (Pair<Double, Boolean> value : values) {
            if(temp == null){
                temp = value;
                continue;
            }
            temp = doPlusI(temp,value);
        }
        return temp;
    }

    private Pair doPlusI(Pair<Double, Boolean> first, Pair<Double, Boolean> second) throws Exception {
        if(first.b == second.b){
            return new Pair(AbstractDataType.getInstance(aClass).function("+", null, first.a,second.a), getTop().b);
        }
        if(first.b){
            return new Pair(first.a, getTop().b);
        }
        if(second.b){
            return new Pair(second.a, getTop().b);
        }
        return null;
    }

    @Override
    public Object function(String name, MyVertex vertex, Object ... values) throws ParseCancellationException {
        Object ret;
        try{
            ret = super.function(name, vertex, values);
        }
        catch (ParseCancellationException ex){
            Double[] l = getListOfDoubles(values);
            ret = new Pair<>((Double) getInstance(aClass).function(name, vertex, l), getTop().b);
        }
        return ret;
    }

    @SmucFunction(symbol = SmucFunction.Symbols.PLUS)
    public Pair plus(Pair<Double, Boolean> ... values) throws Exception {
        Double[] l = getListOfDoubles(values);
        return new Pair(getInstance(aClass).function("+", null, l), getTop().b);
    }

    private Double[] getListOfDoubles(Object[] values) {
        Double[] l = new Double[values.length];
        for (int i = 0; i < values.length; i++) {
            l[i] = ((Pair<Double, Boolean>) values[i]).a;
        }
        return l;
    }

    @Override
    public Pair interpret(String input) {
        Pair sup = super.interpret(input);
        if(sup != null){
            return sup;
        }
        return new Pair(getInstance(aClass).interpret(input), getBottom().b);
        //return null;
    }
    @Override
    public Pair interpret(String input, Long vertexId) {
        return this.interpret(input);
    }
}
