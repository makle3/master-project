package smucparser.datatypes.complex;

import smucparser.datatypes.AbstractDataType;
import smucparser.datatypes.helper.SmucDataType;
import smucparser.datatypes.helper.SmucFunction;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

/**
 * Created by Martin on 17-03-2016.
 */
@SmucDataType
public class NodeSetDataType extends SetDataType<Integer> {
    private static final Logger log = Logger.getLogger( NodeSetDataType.class.getName() );

    public NodeSetDataType(){
        super(new HashSet<Integer>().getClass());
    }

    @SmucFunction
    public Set union(Set ... values){
        Set temp1 = new HashSet<>();
        for(Set s : values){
            temp1.addAll(s);
        }
        return temp1;
    }



    @SmucFunction
    public Set containsOne(Set ... values){
        Set temp1 = new HashSet<>();
        temp1.addAll(values[0]);
        for (int i = 0; i < values.length; i++) {
            if(i == 0){
                continue;
            }
            Set s = values[i];
            for (Object o : s) {
                if (temp1.contains(o)) {
                    return temp1;
                }
            }
        }
        return this.getBottom();
    }



    @SmucFunction(returnType = PathSetDataType.class)
    public Set<Set> asPath(Set ... values){
        PathSetDataType pathType = (PathSetDataType) AbstractDataType.getInstance(PathSetDataType.class);
        HashSet result1 = new HashSet();
        TreeSet result2 = new TreeSet();
        for(Set s : values){
            for(Object o : s) {
                result2.add(o);
                //fixme pathType.setNodeID((Integer) o);
            }
        }
        result1.add(result2);
        return result1;
    }

    @Override
    public Set interpret(String input){
        Set value = super.interpret(input);


        if(value == null){
            String a = input.replace("[","").replace("]","");
            String[] split = a.split(",");
            HashSet set = new HashSet();
            for (String s : split) {
                if(s.trim().equals("")){
                    continue;
                }
                set.add(Long.parseLong(s.trim()));
            }
            return set;
        }


        return value;
    }

    public Set interpret(String input, Long vertexId){
        Set value = super.interpret(input, vertexId);
        if(value != null){
            return value;
        }
        if(input.equals("1")){
            Set ret = new HashSet<>();
            ret.add(vertexId);
            return ret;
        }
        return interpret(input);
    }
}
