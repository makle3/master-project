package smucparser.datatypes.complex;

import smucparser.datatypes.helper.SmucFunction;

import java.util.*;

/**
 * Created by Martin on 27-05-2016.
 */
public class HoareShortestPath extends  HoarePowerDomain<ListDataType<Long>> {
    public HoareShortestPath() {
        super(HashSet.class);
    }

    public Set<ListDataType<Long>> interpret(String input, Long vertexId) {
        Set<ListDataType<Long>> res = super.interpret(input, vertexId);
        if(res == null){
            res = new HashSet<>();
            ListDataType<Long> res2 = new ListDataType<>();
            res2.setValue(new ArrayList<Long>());
            res2.getValue().add(vertexId);
            res.add(res2);
        }
        return res;
    }

//    @Override
//    public Object function(String name, MyVertex vertex, Object[] values) throws ParseCancellationException {
//        return super.function(name, vertex, values);
//    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof HoareShortestPath)){
            return false;
        }
        return this.getValue().equals(((HoareShortestPath) obj).getValue());
    }

    @SmucFunction
    public Set<ListDataType<Long>> addNode(Set<ListDataType<Long>>[] values){
        Set<ListDataType<Long>> result = new HashSet<>();

        for (Set v : values) {
            if(v instanceof TopSet){
                return v;
            }
        }
        if(values[0].size() == 0){
            return values[1];
        }

        
        for (ListDataType<Long> val : values[0]) {
            for (int i = 1; i < values.length; i++) {
                for (ListDataType<Long> val2 : values[i]) {
                    List r = val.addNode((List)val.getValue(), (List)val2.getValue());
                    val2.setValue(r);
                    result.add(val2);
                }
            }
        }
        return result;
    }

    @SmucFunction
    public Set<ListDataType<Long>> min(Set<ListDataType<Long>>[] values){
        return new HashSet<>();
    }

}
