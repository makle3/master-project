package smucparser.datatypes.complex;

import org.antlr.v4.runtime.misc.Pair;
import smucparser.datatypes.helper.SmucDataType;
import smucparser.datatypes.helper.SmucFunction;
import smucparser.datatypes.simple.DoubleDataType;
import smucparser.datatypes.simple.specialized.VertexDataType;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Martin on 27-05-2016.
 */
@SmucDataType
public class NeighborPathDataType extends PairDataType<VertexDataType, Long, DoubleDataType, Double> {
    public NeighborPathDataType() {
        super(VertexDataType.class, DoubleDataType.class);
    }

    @SmucFunction
    public Pair<Long, Double> min(Pair<Long,Double>[] values){
        if(values.length == 0){
            return null;
        }

        Arrays.sort(values, new Comparator<Pair<Long, Double>>() {
            @Override
            public int compare(Pair<Long, Double> o1, Pair<Long, Double> o2) {
                if (o1 == null ^ o2 == null) {
                    return (o1 == null) ? 1 : -1;
                }

                if (o1 == null && o2 == null) {
                    return 0;
                }
                Integer res = o1.b.compareTo(o2.b);
                if(res == 0){
                    res = o1.a.compareTo(o2.a);
                }
                return res;
            }
        });
        //If we don't do this, then we can get graphs which are not connected to the root, which contain loops
        if(values[0].b.equals(this.getTop().b)){
            return this.getTop();
        }

        return values[0];
    }
    @SmucFunction
    public Pair<Long, Double> minValue(Pair<Long,Double>[] values){
        return new Pair<>(values[0].a, Math.min(values[0].b, values[1].b));
    }

    @SmucFunction(takesNodeId = true)
    public Pair<Long, Double>  isNodeOne(Long nodeId, Set<Path>[] values){
        checkParameterCount("isNodeOne", 1, values);
        if(nodeId == 1L){
            return new Pair<>(nodeId, 0.0);
        }
        return new Pair<>(Long.MAX_VALUE, Double.POSITIVE_INFINITY);
    }

    @Override
    public Pair<Long, Double> interpret(String input, Long vertexId) {
        if(input.contains("this")){
            return new Pair<>(vertexId,Double.POSITIVE_INFINITY);
        }
        if(input.equalsIgnoreCase("top")){
            return this.getTop();
        }
        else if(input.equalsIgnoreCase("bottom")||
                input.equalsIgnoreCase("bot")){
            return new Pair<>(vertexId, 0.0);
        }
        return null;
    }

    @SmucFunction
    public Pair<Long, Double> getWeight(Double[] values){
        return new Pair<>(Long.MAX_VALUE, values[0]);
    }

    @SmucFunction(symbol = SmucFunction.Symbols.PLUS)
    public Pair<Long, Double> addWeight(Pair<Long,Double>[] values){
        Long nodeId = Long.MAX_VALUE;
        Double value = 0.0;
        for (Pair<Long, Double> p : values) {
            nodeId = Math.min(nodeId, p.a);
            value += p.b;
        }
        return new Pair<>(nodeId,value);
    }

    @SmucFunction
    public Pair<Long, Double> b(Object[] v){
        return new Pair<>((Long)v[0],Double.POSITIVE_INFINITY);
    }


    @SmucFunction
    public Pair<Long, Double> filter(Pair[] v) throws Exception {
        if(v[0].a == v[1].a || v[0].a == v[2].a){
            return v[0];
        }
        //return new Pair<>((Long)v[1].a, Double.POSITIVE_INFINITY);
        throw new Exception("A node tried to share information about a node which was not a neighbor.");
    }
}
