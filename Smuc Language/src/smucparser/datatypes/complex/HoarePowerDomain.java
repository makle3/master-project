package smucparser.datatypes.complex;

import smucparser.datatypes.AbstractDataType;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Martin on 27-05-2016.
 */
public abstract class HoarePowerDomain<T extends Comparable> extends SetDataType<T> {

    public HoarePowerDomain(Class<? extends Set> clazz) {
        super(clazz);
    }

    @Override
    public Set<T> getValue() {
        return closed(super.getValue());
    }

    public Set<T> closed(Set<T> set){
        if(set == null){
            return null;
        }
        if(set.size() == 0){
            return set;
        }
        TreeSet<T> s = new TreeSet<>(set);
        T first = s.first();

        Set result = super.newClazzInstance();
        result.add(first);
        for (T o : s.tailSet(first,false)) {
            if (o.compareTo(first) == 0) {
                result.add(o);
            }
            else{
                break;
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return this.getValue().equals(((HoareShortestPath) obj).getValue());
    }
}
