package smucparser.datatypes.complex;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Martin on 11-05-2016.
 */
public class Path implements Comparable{
    private List<Long> path = new ArrayList();
    public Path(Path p){
        this.path.addAll(p.path);
    }
    
    public Path(Long vertexId) {
        path.add(vertexId);
    }

    public void addNode(Long nodeId){
        if(path.contains(nodeId)){
            return;
        }
        path.add(nodeId);
    }

    public Integer size(){
        return path.size();
    }


    @Override
    public int compareTo(Object o) {
        return path.size() - ((Path) o).path.size();
    }

    @Override
    public boolean equals(Object obj) {
        return path.equals(((Path) obj).path);
    }

    @Override
    public int hashCode() {
        return path.hashCode();
    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < path.size(); i++) {
            s = (i+1<path.size()?",":"") + path.get(i) + ""  + s;

        }

        return "[" + s + "]";
    }

    public Long getFirst() {
        return (Long) path.get(0);
    }

    public Long getLast(){
        return path.get(path.size()-1);
    }

    public boolean contains(Long id) {
        return path.contains(id);
    }
}
