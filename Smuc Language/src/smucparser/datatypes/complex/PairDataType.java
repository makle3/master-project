package smucparser.datatypes.complex;


import org.antlr.v4.runtime.misc.Pair;
import smucparser.datatypes.AbstractDataType;

/**
 * Created by Martin on 08-04-2016.
 */
public class PairDataType <T1 extends AbstractDataType<V1>, V1, T2 extends AbstractDataType<V2>, V2> extends AbstractDataType<Pair<V1,V2>> {
    protected final Class aClass;
    protected final Class bClass;

    public PairDataType(Class<T1> first, Class<T2> second){
        this.aClass = first;
        this.bClass = second;

        addInstance(this);
    }

    @Override
    public Pair<V1,V2> getTop() {
        return new Pair(AbstractDataType.getInstance(aClass).getTop(),AbstractDataType.getInstance(bClass).getTop());
    }

    @Override
    public Pair getBottom() {
        return new Pair(AbstractDataType.getInstance(aClass).getBottom(),AbstractDataType.getInstance(bClass).getBottom());
    }

}
