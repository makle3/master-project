package smucparser.datatypes.complex;

import org.antlr.v4.runtime.misc.Pair;
import smucparser.datatypes.DataTypeDifference;
import smucparser.datatypes.helper.SmucFunction;
import smucparser.datatypes.simple.BoolDataType;
import smucparser.datatypes.simple.DoubleDataType;

/**
 * Created by Martin on 09-04-2016.
 */
public class DoubleBoolPairDataType<D extends DoubleDataType> extends PairDataType<D, Double, BoolDataType, Boolean> implements DataTypeDifference<Pair<Double,Boolean>> {
    public DoubleBoolPairDataType(Class<D> doubleDataType) {
        super(doubleDataType, BoolDataType.class);
    }


    @SmucFunction(returnType = DoubleDataType.class)
    public Double left(Pair<Double,Boolean> ... values){
        return values[0].a;
    }


    @SmucFunction(returnType = BoolDataType.class)
    public Boolean right(Pair<Double,Boolean> ... values){
        return values[0].b;
    }

    @Override
    public Double difference(Pair<Double, Boolean> a, Pair<Double, Boolean> b) {
        if(a.a == 0){
            return b.a;
        }
        return Math.abs(a.a - b.a);
    }
}
