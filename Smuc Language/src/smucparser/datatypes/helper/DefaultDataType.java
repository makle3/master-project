package smucparser.datatypes.helper;

import smucparser.datatypes.AbstractDataType;

/**
 * Created by Martin on 09-04-2016.
 */
public final class DefaultDataType extends AbstractDataType<Integer>{
    @Override
    public Integer getTop() {
        return null;
    }

    @Override
    public Integer getBottom() {
        return null;
    }
}
