package smucparser.datatypes.helper;

import org.reflections.Reflections;
import smucparser.SMuC;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by Martin on 02-04-2016.
 */
public class writeStuff {
    public static void main(String[] args) {
        Reflections reflections = new Reflections("smucparser.datatypes");
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(SmucDataType.class);

        for (Class<?> controller : annotated) {
            String className = SMuC.getTypeName(controller);
            System.out.println(className);

            List<Method> methods = getMethodsAnnotatedWith(controller, SmucFunction.class);
            Collections.sort(methods, new Comparator<Method>() {
                @Override
                public int compare(Method o1, Method o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
            for(Method m : methods){
                System.out.print(m.getName());
                SmucFunction annotation = m.getAnnotation(SmucFunction.class);

                if(!annotation.returnType().equals(DefaultDataType.class)) {
                    System.out.print("\t\tReturn type: " + SMuC.getTypeName(annotation.returnType()));
                }
                System.out.print("\n");
            }
            System.out.println();




        }

    }

    //http://stackoverflow.com/questions/6593597/java-seek-a-method-with-specific-annotation-and-its-annotation-element
    public static List<Method> getMethodsAnnotatedWith(final Class<?> type, final Class<? extends Annotation> annotation) {
        final List<Method> methods = new ArrayList<Method>();
        Class<?> klass = type;
        while (klass != Object.class) { // need to iterated thought hierarchy in order to retrieve methods from above the current instance
            // iterate though the list of methods declared in the class represented by klass variable, and add those annotated with the specified annotation
            final List<Method> allMethods = new ArrayList<Method>(Arrays.asList(klass.getDeclaredMethods()));
            for (final Method method : allMethods) {
                if (method.isAnnotationPresent(annotation)) {
                    Annotation annotInstance = method.getAnnotation(annotation);
                    // TODO process annotInstance
                    methods.add(method);
                }
            }
            // move to the upper class in the hierarchy in search for more methods
            klass = klass.getSuperclass();
        }
        return methods;
    }
}
