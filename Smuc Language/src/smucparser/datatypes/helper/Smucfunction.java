package smucparser.datatypes.helper;

import smucparser.datatypes.AbstractDataType;

import java.lang.annotation.*;

/**
 * Created by Martin on 16-02-2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface SmucFunction {
    Class<? extends AbstractDataType<?>> returnType() default DefaultDataType.class;

    /**
     * When using SMuC it is possible to use one of the four binary operators:
     * + : PLUS
     * - : MINUS
     * * : STAR
     * / : SLASH
     * There should be at most one method for each of the symbols.
     */
    Symbols symbol() default Symbols.NONE;

    boolean takesNodeId() default false;

    //FunctionType functionType() default FunctionType.EITHER;
//
    //enum FunctionType{
    //    FUNCTION, AGGREGATE, EITHER
    //}

    enum Symbols{
        NONE, PLUS, MINUS, STAR, SLASH
    }
}
