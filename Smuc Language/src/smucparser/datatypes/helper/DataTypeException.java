package smucparser.datatypes.helper;

/**
 * Created by Martin on 29-03-2016.
 */
public class DataTypeException extends Exception {
    public DataTypeException() { super(); }
    public DataTypeException(String message) { super(message); }
    public DataTypeException(String message, Throwable cause) { super(message, cause); }
    public DataTypeException(Throwable cause) { super(cause); }
}
