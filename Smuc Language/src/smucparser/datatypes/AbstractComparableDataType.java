package smucparser.datatypes;

import smucparser.datatypes.helper.SmucFunction;
import smucparser.datatypes.simple.BoolDataType;

/**
 * Created by Martin on 27-04-2016.
 */
public abstract class AbstractComparableDataType<T extends Comparable> extends AbstractDataType<T> {
    @SmucFunction(returnType = BoolDataType.class)
    public Boolean lt(T ... values){
        checkParameterCount("lt", values);
        return values[0].compareTo(values[1]) < 0;
    }
    @SmucFunction(returnType = BoolDataType.class)
    public Boolean lte(T ... values){
        checkParameterCount("lte", values);
        return values[0].compareTo(values[1]) <= 0;
    }

    @SmucFunction(returnType = BoolDataType.class)
    public Boolean gt(T ... values){
        checkParameterCount("gt", values);
        return values[0].compareTo(values[1]) > 0;
    }

    @SmucFunction(returnType = BoolDataType.class)
    public Boolean gte(T ... values){
        checkParameterCount("gte", values);
        return values[0].compareTo(values[1]) >= 0;
    }

    @SmucFunction(returnType = BoolDataType.class)
    public Boolean eq(T ... values){
        int compare = 0;
        for (int i = 0; i < values.length-1; i++) {
            compare+=values[i].compareTo(values[i+1]);
        }
        return compare == 0;
    }

    @SmucFunction(returnType = BoolDataType.class)
    public Boolean neq(T ... values){
        return !eq(values);
    }
}
