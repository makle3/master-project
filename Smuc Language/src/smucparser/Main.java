package smucparser;


import org.apache.commons.cli.*;
import org.jgrapht.graph.DefaultDirectedGraph;
import smucparser.datatypes.AbstractDataType;
import smucparser.graph.Import.*;
import smucparser.graph.MyEdge;
import smucparser.graph.MyVertex;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static final Logger log = Logger.getLogger( Main.class.getName() );

    public static void main(String[] args) {
        String smucQuery = null;
        String outputFileName = null;
        boolean exportJson = false;
        String exportJsonName = "";
        boolean exportcsv = false;
        String exportCSVFileName = "";
        Class dataType = null;
        TargetOption targetOption = TargetOption.EQUALS;
        Long maxIterations = 0L;
        Double epsilon = 0d;

        DefaultDirectedGraph<MyVertex, MyEdge> graph = null;// = new DefaultDirectedGraph<>(MyEdge.class);


        //options.addOption("opt","longOpt",false,"description");
        Option query = new Option("q","query",true,"SMuC query to be performed on a graph.");
        Option pdfOutput = new Option("o",true,"The name of the output pdf file.");
        Option help = new Option("h","help",false,"Print this message.");

        Option classOption = Option.builder().argName("classname")
                .longOpt("datatype")
                .hasArg()
                .desc("The datatype to use with the graph.").build();

        Option prism = Option.builder().numberOfArgs(2)
                .longOpt("prism")
                .hasArgs()
                .argName("Transition matrix> <States")//meh
                .desc("Transition matrix and states exported from Prism model checker as txt.").build();

        Option csvOptionIn = Option.builder().argName("filename")
                .longOpt("importcsv")
                .hasArg()
                .desc("Import a graph from a csv file.").build();

        Option csvOptionOut = Option.builder().argName("filename")
                .longOpt("exportcsv")
                .hasArg()
                .desc("Export the graph to csv file.").build();


        Option jsonOptionIn = Option.builder().argName("filename")
                .longOpt("importjson")
                .hasArg()
                .desc("Import a graph from a json file.").build();

        Option fbImport = Option.builder()
                .argName("full|1-10")
                .longOpt("importFB")
                .hasArg(true)
                .desc("Import facebook graph.").build();


        Option jsonOptionOut = Option.builder().argName("filename")
                .longOpt("exportjson")
                .hasArg()
                .desc("Export the graph to csv file.").build();


        Option target = Option.builder("T")
                .valueSeparator()
                .numberOfArgs(2)
                .desc("Target precision or iterations for fixed points.").build();

        Options options = new Options();
        options.addOption(help);
        options.addOption(query);
        options.addOption(pdfOutput);
        options.addOption(classOption);
        options.addOption(prism);
        options.addOption(csvOptionIn);
        options.addOption(csvOptionOut);
        options.addOption(jsonOptionIn);
        options.addOption(jsonOptionOut);
        options.addOption(target);
        options.addOption(fbImport);


        HelpFormatter formatter = new HelpFormatter();

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse( options, args);

            if(cmd.hasOption("h")){
                formatter.printHelp( "smuc", options, true);
            }
            if(cmd.hasOption("q")){
                smucQuery = cmd.getOptionValue("q");
            }
            if(cmd.hasOption("query")){
                smucQuery = cmd.getOptionValue("query");
            }
            if(cmd.hasOption("o")){
                outputFileName = cmd.getOptionValue("o");
            }
            if(cmd.hasOption("datatype")){
                Class c = Class.forName(cmd.getOptionValue("datatype"));
                try {
                    if (c.newInstance() instanceof AbstractDataType) {
                        dataType = c;
                    } else {
                        throw new ParseException("Classname was of the wrong type.");
                    }
                }
                catch (InstantiationException e){
                    log.log(Level.SEVERE,cmd.getOptionValue("datatype") + " could not be instantiated.");
                    e.printStackTrace();
                }
            }
            if(cmd.hasOption("prism")){
                try {
                    graph = PRISMTransitionMatrix.importMatrix(cmd.getOptionValues("prism")[0],cmd.getOptionValues("prism")[1], dataType);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(cmd.hasOption("exportcsv")){
                exportcsv = true;
                exportCSVFileName = cmd.getOptionValue("exportcsv");
            }
            if(cmd.hasOption("importcsv")){

                try {
                    graph = CSV.deserialize(dataType, readFile(cmd.getOptionValue("importcsv"), Charset.defaultCharset()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(cmd.hasOption("importFB")){
                try {
                    if(cmd.getOptionValue("importFB").toLowerCase().equals("full")){
                        graph = SocialCircles.getFacebookGraphFull();
                    }
                    else{
                        Integer[] a = new Integer[]{0, 107, 348, 414, 686, 698, 1684, 1912, 3437, 3980};
                        int i = Integer.parseInt(cmd.getOptionValue("importFB"));
                        graph = SocialCircles.getFacebookGraph(Arrays.copyOfRange(a, 0, i));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if(cmd.hasOption("exportjson")){
                exportJson = true;
                exportJsonName = cmd.getOptionValue("exportjson");
            }
            if(cmd.hasOption("importjson")){
                try {
                    graph = JSON.deserialize(dataType, readFile(cmd.getOptionValue("importjson"), Charset.defaultCharset()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(cmd.hasOption("T")){
                String[] option = cmd.getOptionValues("T");
                boolean equalsIsSet = false;
                boolean epsilonIsSet = false;
                for (int i = 0; i < option.length; i++) {
                    switch (option[i]){
                        case "eq":
                        case "equals":
                            equalsIsSet = true;
                            targetOption = TargetOption.EQUALS;
                            break;
                        case "it":
                        case "ite":
                        case "iteration":
                        case "iterations":
                            maxIterations = Long.parseLong(option[++i]);
                            break;
                        case "epsilon":
                        case "e":
                        case "eps":
                        case "difference":
                        case "diff":
                            epsilonIsSet = true;
                            targetOption = TargetOption.EPSILON;
                            epsilon = Double.parseDouble(option[++i]);
                            break;
                        default:
                            throw new MyException("Option " + option[i] + " was not recognized as an option for T.");
                    }
                    if(equalsIsSet && epsilonIsSet){
                        throw new MyException("Both equals and epsilon can not be set at the same time.");
                    }
                }
            }

        } catch (ParseException e) {
            System.err.println( "Parsing failed.  Reason: " + e.getMessage() );
            formatter.printHelp( "smuc", options, true);
        } catch (ClassNotFoundException | IllegalAccessException | MyException e) {
            e.printStackTrace();
        }

        if(smucQuery != null && graph != null){
            try {
                SMuC smuc = new SMuC();
                smuc.setDefaultDataType(dataType)
                        .addQuery(smucQuery)
                        .setMaxIterations(maxIterations)
                        .setTargetOption(targetOption)
                        .setEpsilon(epsilon)
                        .run(graph);
            } catch (MyException e) {
                e.printStackTrace();
            }
        }


        if(exportcsv){
            if(!exportCSVFileName.toLowerCase().endsWith(".csv")){
                exportCSVFileName += ".csv";
            }
            String outputString = CSV.serialize(graph);
            writeFile(outputString, exportCSVFileName);
        }


        if(exportJson){
            if(!exportJsonName.toLowerCase().endsWith(".json")){
                exportJsonName += ".json";
            }
            String outputString = JSON.serialize(graph);
            writeFile(outputString, exportJsonName);
        }


        if(outputFileName != null) {
            if(!outputFileName.toLowerCase().endsWith(".pdf")){
                outputFileName += ".pdf";
            }

            DOT.printGraph(graph, outputFileName, false, true, false);
        }
    }



    public static void writeFile(String input, String filename){
        try {
            PrintWriter writer = new PrintWriter(filename, "UTF-8");
            writer.println(input);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
