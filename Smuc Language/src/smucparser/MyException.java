package smucparser;

/**
 * Created by Martin on 23-04-2016.
 */
public class MyException extends Exception{
    public MyException(String s) {
        super(s);
    }
}
